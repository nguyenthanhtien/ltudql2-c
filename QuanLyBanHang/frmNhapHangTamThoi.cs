﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmNhapHangTamThoi : Form
    {
        DACK_ware5Entities1 db = new DACK_ware5Entities1();
        private string mahanghoa;
        public frmNhapHangTamThoi()
        {
            InitializeComponent();
        }
        private void frmNhapHangTamThoi_Load(object sender, EventArgs e)
        {
           
            LoadData();
        }

        private void LoadData()
        {
            var query = (from n in db.HangHoas from m in db.DonViTinhs where n.id_DonVi == m.Id_DV select new { n.id_hanghoa, n.Tenhang, m.TenDonVi, n.SoLuong }).ToList();
            grdDanhSachHangHoa.DataSource = query;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grdDanhSachHangHoa_Click(object sender, EventArgs e)
        {

        }

        private void grdDanhSachHangHoa_MouseClick(object sender, MouseEventArgs e)
        {
           
        }

        private void repositoryItemButtonEdit1_Click(object sender, EventArgs e)
        {
            mahanghoa = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "id_hanghoa").ToString();
            txtTenHang.Text = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "Tenhang").ToString();
            txtMaHangHoa.Text = mahanghoa;
            txtDonVi.Text = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TenDonVi").ToString();
            lueSoLuong.EditValue = 1;
            calcEditDonGia.EditValue = 1;
            var idhh = db.DonViTinhs.FirstOrDefault(a => a.TenDonVi.Equals(txtDonVi.Text));
            txtIDDonVi.Text = Convert.ToString( idhh.Id_DV);
        }
        private void bnLuu_Click(object sender, EventArgs e)
        {
            //var idhh = db.HangHoas.FirstOrDefault(a => a.id_hanghoa.Equals(mahanghoa));
            //idhh.SoLuong = idhh.SoLuong + Convert.ToInt32(lueSoLuong.EditValue);

            NhapHangTamThoi nh = new NhapHangTamThoi();
            nh.MaHang = txtMaHangHoa.Text;
            nh.TenHang = txtTenHang.Text;
            nh.Id_DonVi = Convert.ToInt32(txtIDDonVi.Text);
            nh.SoLuong = Convert.ToInt32(lueSoLuong.EditValue);
            nh.DonGia = Convert.ToInt32(calcEditDonGia.EditValue);
            nh.ThanhTien = Convert.ToInt32(lueSoLuong.EditValue) * Convert.ToInt32(calcEditDonGia.EditValue);
            db.NhapHangTamThois.Add(nh);
            db.SaveChanges();
            MessageBox.Show("Đã thêm.");
        }
    }
}
