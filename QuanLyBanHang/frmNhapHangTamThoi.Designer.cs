﻿namespace QuanLyBanHang
{
    partial class frmNhapHangTamThoi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNhapHangTamThoi));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDonVi = new System.Windows.Forms.TextBox();
            this.txtTenHang = new System.Windows.Forms.TextBox();
            this.txtMaHangHoa = new System.Windows.Forms.TextBox();
            this.calcEditDonGia = new DevExpress.XtraEditors.CalcEdit();
            this.lueSoLuong = new DevExpress.XtraEditors.CalcEdit();
            this.panel2 = new System.Windows.Forms.Panel();
            this.grdDanhSachHangHoa = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.id_hanghoa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Tenhang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TenDonVi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SoLuong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Chọn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.bnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.txtIDDonVi = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditDonGia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSoLuong.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDanhSachHangHoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtIDDonVi);
            this.panel1.Controls.Add(this.txtDonVi);
            this.panel1.Controls.Add(this.txtTenHang);
            this.panel1.Controls.Add(this.txtMaHangHoa);
            this.panel1.Controls.Add(this.calcEditDonGia);
            this.panel1.Controls.Add(this.lueSoLuong);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(735, 105);
            this.panel1.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(322, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Đơn giá";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(519, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Sô lượng";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tên hàng";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Mã hàng";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(322, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Đơn vị";
            // 
            // txtDonVi
            // 
            this.txtDonVi.Location = new System.Drawing.Point(395, 22);
            this.txtDonVi.Name = "txtDonVi";
            this.txtDonVi.Size = new System.Drawing.Size(117, 20);
            this.txtDonVi.TabIndex = 1;
            // 
            // txtTenHang
            // 
            this.txtTenHang.Location = new System.Drawing.Point(78, 59);
            this.txtTenHang.Name = "txtTenHang";
            this.txtTenHang.Size = new System.Drawing.Size(225, 20);
            this.txtTenHang.TabIndex = 1;
            // 
            // txtMaHangHoa
            // 
            this.txtMaHangHoa.Location = new System.Drawing.Point(78, 22);
            this.txtMaHangHoa.Name = "txtMaHangHoa";
            this.txtMaHangHoa.ReadOnly = true;
            this.txtMaHangHoa.Size = new System.Drawing.Size(225, 20);
            this.txtMaHangHoa.TabIndex = 0;
            // 
            // calcEditDonGia
            // 
            this.calcEditDonGia.Location = new System.Drawing.Point(395, 59);
            this.calcEditDonGia.Name = "calcEditDonGia";
            this.calcEditDonGia.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEditDonGia.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.calcEditDonGia.Properties.NullText = "[EditValue is null]";
            this.calcEditDonGia.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.calcEditDonGia.Size = new System.Drawing.Size(117, 20);
            this.calcEditDonGia.TabIndex = 3;
            // 
            // lueSoLuong
            // 
            this.lueSoLuong.Location = new System.Drawing.Point(592, 22);
            this.lueSoLuong.Name = "lueSoLuong";
            this.lueSoLuong.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSoLuong.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.lueSoLuong.Properties.NullText = "[EditValue is null]";
            this.lueSoLuong.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.lueSoLuong.Size = new System.Drawing.Size(117, 20);
            this.lueSoLuong.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.grdDanhSachHangHoa);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 105);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(735, 291);
            this.panel2.TabIndex = 1;
            // 
            // grdDanhSachHangHoa
            // 
            this.grdDanhSachHangHoa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdDanhSachHangHoa.Location = new System.Drawing.Point(0, 0);
            this.grdDanhSachHangHoa.MainView = this.gridView1;
            this.grdDanhSachHangHoa.Name = "grdDanhSachHangHoa";
            this.grdDanhSachHangHoa.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1});
            this.grdDanhSachHangHoa.Size = new System.Drawing.Size(735, 291);
            this.grdDanhSachHangHoa.TabIndex = 0;
            this.grdDanhSachHangHoa.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.grdDanhSachHangHoa.Click += new System.EventHandler(this.grdDanhSachHangHoa_Click);
            this.grdDanhSachHangHoa.MouseClick += new System.Windows.Forms.MouseEventHandler(this.grdDanhSachHangHoa_MouseClick);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.id_hanghoa,
            this.Tenhang,
            this.TenDonVi,
            this.SoLuong,
            this.Chọn});
            this.gridView1.GridControl = this.grdDanhSachHangHoa;
            this.gridView1.Name = "gridView1";
            // 
            // id_hanghoa
            // 
            this.id_hanghoa.Caption = "Mã hàng";
            this.id_hanghoa.FieldName = "id_hanghoa";
            this.id_hanghoa.Name = "id_hanghoa";
            this.id_hanghoa.Visible = true;
            this.id_hanghoa.VisibleIndex = 0;
            // 
            // Tenhang
            // 
            this.Tenhang.Caption = "Tên hàng";
            this.Tenhang.FieldName = "Tenhang";
            this.Tenhang.Name = "Tenhang";
            this.Tenhang.Visible = true;
            this.Tenhang.VisibleIndex = 1;
            // 
            // TenDonVi
            // 
            this.TenDonVi.Caption = "Đơn vị";
            this.TenDonVi.FieldName = "TenDonVi";
            this.TenDonVi.Name = "TenDonVi";
            this.TenDonVi.Visible = true;
            this.TenDonVi.VisibleIndex = 2;
            // 
            // SoLuong
            // 
            this.SoLuong.Caption = "Sô lượng tồn";
            this.SoLuong.FieldName = "SoLuong";
            this.SoLuong.Name = "SoLuong";
            this.SoLuong.Visible = true;
            this.SoLuong.VisibleIndex = 3;
            // 
            // Chọn
            // 
            this.Chọn.Caption = "Chọn";
            this.Chọn.ColumnEdit = this.repositoryItemButtonEdit1;
            this.Chọn.Image = ((System.Drawing.Image)(resources.GetObject("Chọn.Image")));
            this.Chọn.Name = "Chọn";
            this.Chọn.Visible = true;
            this.Chọn.VisibleIndex = 4;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Chọn", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit1.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.Click += new System.EventHandler(this.repositoryItemButtonEdit1_Click);
            // 
            // bnLuu
            // 
            this.bnLuu.ImageUri.Uri = "Edit;Size16x16;Colored";
            this.bnLuu.Location = new System.Drawing.Point(494, 402);
            this.bnLuu.Name = "bnLuu";
            this.bnLuu.Size = new System.Drawing.Size(75, 23);
            this.bnLuu.TabIndex = 2;
            this.bnLuu.Text = "Thêm";
            this.bnLuu.Click += new System.EventHandler(this.bnLuu_Click);
            // 
            // btnDong
            // 
            this.btnDong.ImageUri.Uri = "Cancel;Size16x16;Colored";
            this.btnDong.Location = new System.Drawing.Point(575, 402);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(75, 23);
            this.btnDong.TabIndex = 3;
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // txtIDDonVi
            // 
            this.txtIDDonVi.Location = new System.Drawing.Point(592, 59);
            this.txtIDDonVi.Name = "txtIDDonVi";
            this.txtIDDonVi.ReadOnly = true;
            this.txtIDDonVi.Size = new System.Drawing.Size(117, 20);
            this.txtIDDonVi.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(530, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "ID Đơn vị";
            // 
            // frmNhapHangTamThoi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(735, 435);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.bnLuu);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "frmNhapHangTamThoi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chọn hàng";
            this.Load += new System.EventHandler(this.frmNhapHangTamThoi_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditDonGia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSoLuong.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdDanhSachHangHoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraGrid.GridControl grdDanhSachHangHoa;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton bnLuu;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraGrid.Columns.GridColumn id_hanghoa;
        private DevExpress.XtraGrid.Columns.GridColumn Tenhang;
        private DevExpress.XtraGrid.Columns.GridColumn TenDonVi;
        private DevExpress.XtraGrid.Columns.GridColumn SoLuong;
        private System.Windows.Forms.TextBox txtMaHangHoa;
        private System.Windows.Forms.TextBox txtDonVi;
        private System.Windows.Forms.TextBox txtTenHang;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CalcEdit lueSoLuong;
        private DevExpress.XtraGrid.Columns.GridColumn Chọn;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.CalcEdit calcEditDonGia;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtIDDonVi;
    }
}