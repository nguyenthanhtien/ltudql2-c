﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmdangnhap : Form
    {
        DACK_ware5Entities1 dbe = new DACK_ware5Entities1();
        public frmdangnhap()
        {
            InitializeComponent();
        }

        private void frmdangnhap_Load(object sender, EventArgs e)
        {

        }
        private void XoaTrang()
        {
            txt_Pass.Text = string.Empty;
            txt_DangNhap.Text = string.Empty;
        }

        private void btn_DangNhap_Click(object sender, EventArgs e)
        {
            string userid = txt_DangNhap.Text;
            string datetime = DateTime.Now.ToLongDateString();
            string compname = System.Environment.MachineName;
            NhatKy nk = new NhatKy();
            nk.id_NguoiDung = userid;
            nk.MayTinh = compname;
            nk.Date = DateTime.Parse(datetime);
            dbe.NhatKies.Add(nk);
            dbe.SaveChanges();
            if (txt_DangNhap.Text != string.Empty && txt_Pass.Text != string.Empty)
            {
                var user = dbe.NguoiDungs.FirstOrDefault(a => a.Username.Equals(userid));
                if (user.Username == txt_DangNhap.Text )
                {
                    if (txt_Pass.Text == user.Password)
                    {
                        var form = new Form1();
                        this.Hide();
                        form.manguoidung = txt_DangNhap.Text;
                        form.tennguoidung = txt_Pass.Text;
                        form.ShowDialog();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Password error!");
                        XoaTrang();
                    }
                }
                else
                {
                    MessageBox.Show("UserName error!");
                    XoaTrang();
                }
            }
            else
            {
                MessageBox.Show("Not null!");
                XoaTrang();
            }
            
        }

    }
}