﻿namespace QuanLyBanHang
{
    partial class frmMuaHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMuaHang));
            this.tabPhieuMuaHang = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.layoutControl5 = new DevExpress.XtraLayout.LayoutControl();
            this.txtThanhToan = new System.Windows.Forms.TextBox();
            this.lueChietKhau = new DevExpress.XtraEditors.CalcEdit();
            this.lueVAT = new DevExpress.XtraEditors.CalcEdit();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnNapLaiDanhSach = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemMoiPhieuNhapHang = new DevExpress.XtraEditors.SimpleButton();
            this.grcDanhSachPhieuMuaHang = new DevExpress.XtraGrid.GridControl();
            this.gridViewacb = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.cmbHinhThucThanhToan = new System.Windows.Forms.ComboBox();
            this.dtpNgayThanhToan = new System.Windows.Forms.DateTimePicker();
            this.lueHinhThucThanhToan = new DevExpress.XtraEditors.LookUpEdit();
            this.txtGhiChu = new System.Windows.Forms.TextBox();
            this.txtSoPhieuVietTay = new System.Windows.Forms.TextBox();
            this.lueTenNhanVien = new DevExpress.XtraEditors.LookUpEdit();
            this.txtSoHoaDonVAT = new System.Windows.Forms.TextBox();
            this.txtPhieu = new DevExpress.XtraEditors.TextEdit();
            this.dtpNgayThangXuat = new System.Windows.Forms.DateTimePicker();
            this.txtSdt = new System.Windows.Forms.TextBox();
            this.txtDiaChi = new System.Windows.Forms.TextBox();
            this.lueKhoNhap = new DevExpress.XtraEditors.LookUpEdit();
            this.lueMaNhaCungCap = new DevExpress.XtraEditors.LookUpEdit();
            this.lueTenNhaCungCap = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDongPhieuNhapHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnNapLaiPhieuHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnInPhieuHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuuVaThem = new DevExpress.XtraEditors.SimpleButton();
            this.btntaoMoiPhieuNhapHang = new DevExpress.XtraEditors.SimpleButton();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.grcDanhSachHHTheoChungTu = new DevExpress.XtraGrid.GridControl();
            this.griđánhachhangHoaTheoChungTu = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel4 = new System.Windows.Forms.Panel();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.lueXemTheo = new DevExpress.XtraEditors.LookUpEdit();
            this.btnXoaChungTu = new DevExpress.XtraEditors.SimpleButton();
            this.btnInChungTu = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuatChungTu = new DevExpress.XtraEditors.SimpleButton();
            this.btnDongChungTu = new DevExpress.XtraEditors.SimpleButton();
            this.btnSuaChungTu = new DevExpress.XtraEditors.SimpleButton();
            this.btnTaoMoiChungTu = new DevExpress.XtraEditors.SimpleButton();
            this.btnXemchungTu = new DevExpress.XtraEditors.SimpleButton();
            this.dtpNgayKetThuc = new System.Windows.Forms.DateTimePicker();
            this.dtpNgayBatDau = new System.Windows.Forms.DateTimePicker();
            this.cmbChonLoai = new System.Windows.Forms.ComboBox();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.grcDanhSachTheoHangHoa = new DevExpress.XtraGrid.GridControl();
            this.gridViewHHTheoCT = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel8 = new System.Windows.Forms.Panel();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.btnXoaChungTuTheoCT = new DevExpress.XtraEditors.SimpleButton();
            this.btnXemTheoHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.lueXemDanhSachHangHoaTheo = new DevExpress.XtraEditors.LookUpEdit();
            this.btnDonghangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuatHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnInHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnSuaHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnTaoMoihangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.dtpXemhangHoaKetThuc = new System.Windows.Forms.DateTimePicker();
            this.dtpXemHangHoaTu = new System.Windows.Forms.DateTimePicker();
            this.cmbLuachonXemTheoHangHoa = new System.Windows.Forms.ComboBox();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.navBarControl1 = new DevExpress.XtraNavBar.NavBarControl();
            this.tabThemDanhMuc = new DevExpress.XtraNavBar.NavBarGroup();
            this.LinkHangHoa = new DevExpress.XtraNavBar.NavBarItem();
            this.LinkKhachHang = new DevExpress.XtraNavBar.NavBarItem();
            this.LinkKhoHang = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarControl2 = new DevExpress.XtraNavBar.NavBarControl();
            this.navBarGroup2 = new DevExpress.XtraNavBar.NavBarGroup();
            this.btnTheoChungTu = new DevExpress.XtraNavBar.NavBarItem();
            this.btnTheoHangHoa = new DevExpress.XtraNavBar.NavBarItem();
            this.btnXemPhieuMuaHang = new DevExpress.XtraNavBar.NavBarControl();
            this.navBarGroup1 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItem1 = new DevExpress.XtraNavBar.NavBarItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbTenHienThi = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel7 = new System.Windows.Forms.Panel();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.lookUpEdit8 = new DevExpress.XtraEditors.LookUpEdit();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton12 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton13 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton14 = new DevExpress.XtraEditors.SimpleButton();
            this.dateTimePicker5 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker6 = new System.Windows.Forms.DateTimePicker();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.tabPhieuMuaHang.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).BeginInit();
            this.layoutControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueChietKhau.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueVAT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachPhieuMuaHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewacb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueHinhThucThanhToan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTenNhanVien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhieu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKhoNhap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueMaNhaCungCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTenNhaCungCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            this.panel2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachHHTheoChungTu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.griđánhachhangHoaTheoChungTu)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueXemTheo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachTheoHangHoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHHTheoCT)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueXemDanhSachHangHoaTheo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnXemPhieuMuaHang)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPhieuMuaHang
            // 
            this.tabPhieuMuaHang.Controls.Add(this.tabPage1);
            this.tabPhieuMuaHang.Controls.Add(this.tabPage2);
            this.tabPhieuMuaHang.Controls.Add(this.tabPage3);
            this.tabPhieuMuaHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPhieuMuaHang.Location = new System.Drawing.Point(172, 0);
            this.tabPhieuMuaHang.Name = "tabPhieuMuaHang";
            this.tabPhieuMuaHang.SelectedIndex = 0;
            this.tabPhieuMuaHang.Size = new System.Drawing.Size(914, 523);
            this.tabPhieuMuaHang.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Controls.Add(this.layoutControl1);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(906, 497);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel10);
            this.panel3.Controls.Add(this.btnNapLaiDanhSach);
            this.panel3.Controls.Add(this.btnThemMoiPhieuNhapHang);
            this.panel3.Controls.Add(this.grcDanhSachPhieuMuaHang);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(3, 214);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(900, 280);
            this.panel3.TabIndex = 2;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.layoutControl5);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(0, 241);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(900, 39);
            this.panel10.TabIndex = 3;
            // 
            // layoutControl5
            // 
            this.layoutControl5.Controls.Add(this.txtThanhToan);
            this.layoutControl5.Controls.Add(this.lueChietKhau);
            this.layoutControl5.Controls.Add(this.lueVAT);
            this.layoutControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl5.Location = new System.Drawing.Point(0, 0);
            this.layoutControl5.Name = "layoutControl5";
            this.layoutControl5.Root = this.layoutControlGroup5;
            this.layoutControl5.Size = new System.Drawing.Size(900, 39);
            this.layoutControl5.TabIndex = 0;
            this.layoutControl5.Text = "layoutControl5";
            // 
            // txtThanhToan
            // 
            this.txtThanhToan.ForeColor = System.Drawing.Color.Red;
            this.txtThanhToan.Location = new System.Drawing.Point(636, 12);
            this.txtThanhToan.Name = "txtThanhToan";
            this.txtThanhToan.Size = new System.Drawing.Size(235, 20);
            this.txtThanhToan.TabIndex = 6;
            // 
            // lueChietKhau
            // 
            this.lueChietKhau.Location = new System.Drawing.Point(70, 12);
            this.lueChietKhau.Name = "lueChietKhau";
            this.lueChietKhau.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lueChietKhau.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lueChietKhau.Properties.Appearance.Options.UseFont = true;
            this.lueChietKhau.Properties.Appearance.Options.UseForeColor = true;
            this.lueChietKhau.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueChietKhau.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.lueChietKhau.Properties.NullText = "[EditValue is null]";
            this.lueChietKhau.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.lueChietKhau.Size = new System.Drawing.Size(179, 22);
            this.lueChietKhau.StyleController = this.layoutControl5;
            this.lueChietKhau.TabIndex = 4;
            // 
            // lueVAT
            // 
            this.lueVAT.Location = new System.Drawing.Point(311, 12);
            this.lueVAT.Name = "lueVAT";
            this.lueVAT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lueVAT.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lueVAT.Properties.Appearance.Options.UseFont = true;
            this.lueVAT.Properties.Appearance.Options.UseForeColor = true;
            this.lueVAT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueVAT.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.lueVAT.Properties.NullText = "[EditValue is null]";
            this.lueVAT.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.lueVAT.Size = new System.Drawing.Size(263, 22);
            this.lueVAT.StyleController = this.layoutControl5;
            this.lueVAT.TabIndex = 5;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem47,
            this.emptySpaceItem5,
            this.layoutControlItem48,
            this.layoutControlItem49});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(883, 56);
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.Control = this.lueChietKhau;
            this.layoutControlItem47.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(241, 26);
            this.layoutControlItem47.Text = "CK%";
            this.layoutControlItem47.TextSize = new System.Drawing.Size(55, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 26);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(241, 10);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.Control = this.lueVAT;
            this.layoutControlItem48.Location = new System.Drawing.Point(241, 0);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(325, 36);
            this.layoutControlItem48.Text = "VAT%";
            this.layoutControlItem48.TextSize = new System.Drawing.Size(55, 13);
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.Control = this.txtThanhToan;
            this.layoutControlItem49.Location = new System.Drawing.Point(566, 0);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(297, 36);
            this.layoutControlItem49.Text = "Thanh toán";
            this.layoutControlItem49.TextSize = new System.Drawing.Size(55, 13);
            // 
            // btnNapLaiDanhSach
            // 
            this.btnNapLaiDanhSach.ImageUri.Uri = "Refresh;Size16x16;Colored";
            this.btnNapLaiDanhSach.Location = new System.Drawing.Point(732, 3);
            this.btnNapLaiDanhSach.Name = "btnNapLaiDanhSach";
            this.btnNapLaiDanhSach.Size = new System.Drawing.Size(93, 29);
            this.btnNapLaiDanhSach.TabIndex = 2;
            this.btnNapLaiDanhSach.Text = "Nạp lại";
            this.btnNapLaiDanhSach.Click += new System.EventHandler(this.btnNapLaiDanhSach_Click);
            // 
            // btnThemMoiPhieuNhapHang
            // 
            this.btnThemMoiPhieuNhapHang.ImageUri.Uri = "Edit;Size16x16;Colored";
            this.btnThemMoiPhieuNhapHang.Location = new System.Drawing.Point(610, 3);
            this.btnThemMoiPhieuNhapHang.Name = "btnThemMoiPhieuNhapHang";
            this.btnThemMoiPhieuNhapHang.Size = new System.Drawing.Size(115, 29);
            this.btnThemMoiPhieuNhapHang.TabIndex = 1;
            this.btnThemMoiPhieuNhapHang.Text = "Chọn hàng hóa";
            this.btnThemMoiPhieuNhapHang.Click += new System.EventHandler(this.btnThemMoiPhieuNhapHang_Click);
            // 
            // grcDanhSachPhieuMuaHang
            // 
            this.grcDanhSachPhieuMuaHang.Dock = System.Windows.Forms.DockStyle.Top;
            gridLevelNode2.RelationName = "Level1";
            this.grcDanhSachPhieuMuaHang.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.grcDanhSachPhieuMuaHang.Location = new System.Drawing.Point(0, 0);
            this.grcDanhSachPhieuMuaHang.MainView = this.gridViewacb;
            this.grcDanhSachPhieuMuaHang.Name = "grcDanhSachPhieuMuaHang";
            this.grcDanhSachPhieuMuaHang.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit1,
            this.repositoryItemLookUpEdit2,
            this.repositoryItemLookUpEdit3,
            this.repositoryItemLookUpEdit4,
            this.repositoryItemCalcEdit1,
            this.repositoryItemCalcEdit2});
            this.grcDanhSachPhieuMuaHang.Size = new System.Drawing.Size(900, 241);
            this.grcDanhSachPhieuMuaHang.TabIndex = 0;
            this.grcDanhSachPhieuMuaHang.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewacb});
            this.grcDanhSachPhieuMuaHang.Click += new System.EventHandler(this.grcDanhSachPhieuMuaHang_Click);
            // 
            // gridViewacb
            // 
            this.gridViewacb.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7});
            this.gridViewacb.GridControl = this.grcDanhSachPhieuMuaHang;
            this.gridViewacb.Name = "gridViewacb";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Mã hàng";
            this.gridColumn1.FieldName = "MaHang";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Tên hàng";
            this.gridColumn2.FieldName = "TenHang";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Đơn vị";
            this.gridColumn3.FieldName = "TenDonVi";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Số lượng";
            this.gridColumn4.ColumnEdit = this.repositoryItemCalcEdit2;
            this.gridColumn4.FieldName = "SoLuong";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // repositoryItemCalcEdit2
            // 
            this.repositoryItemCalcEdit2.AutoHeight = false;
            this.repositoryItemCalcEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit2.Name = "repositoryItemCalcEdit2";
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Đơn giá";
            this.gridColumn5.ColumnEdit = this.repositoryItemCalcEdit1;
            this.gridColumn5.FieldName = "DonGia";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Thành tiền";
            this.gridColumn6.FieldName = "ThanhTien";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Ghi chú";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 6;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            // 
            // repositoryItemLookUpEdit2
            // 
            this.repositoryItemLookUpEdit2.AutoHeight = false;
            this.repositoryItemLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit2.Name = "repositoryItemLookUpEdit2";
            // 
            // repositoryItemLookUpEdit3
            // 
            this.repositoryItemLookUpEdit3.AutoHeight = false;
            this.repositoryItemLookUpEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit3.Name = "repositoryItemLookUpEdit3";
            // 
            // repositoryItemLookUpEdit4
            // 
            this.repositoryItemLookUpEdit4.AutoHeight = false;
            this.repositoryItemLookUpEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit4.Name = "repositoryItemLookUpEdit4";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.cmbHinhThucThanhToan);
            this.layoutControl1.Controls.Add(this.dtpNgayThanhToan);
            this.layoutControl1.Controls.Add(this.lueHinhThucThanhToan);
            this.layoutControl1.Controls.Add(this.txtGhiChu);
            this.layoutControl1.Controls.Add(this.txtSoPhieuVietTay);
            this.layoutControl1.Controls.Add(this.lueTenNhanVien);
            this.layoutControl1.Controls.Add(this.txtSoHoaDonVAT);
            this.layoutControl1.Controls.Add(this.txtPhieu);
            this.layoutControl1.Controls.Add(this.dtpNgayThangXuat);
            this.layoutControl1.Controls.Add(this.txtSdt);
            this.layoutControl1.Controls.Add(this.txtDiaChi);
            this.layoutControl1.Controls.Add(this.lueKhoNhap);
            this.layoutControl1.Controls.Add(this.lueMaNhaCungCap);
            this.layoutControl1.Controls.Add(this.lueTenNhaCungCap);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(3, 37);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(900, 177);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // cmbHinhThucThanhToan
            // 
            this.cmbHinhThucThanhToan.FormattingEnabled = true;
            this.cmbHinhThucThanhToan.Items.AddRange(new object[] {
            "Công nợ",
            "Thanh toán ngay"});
            this.cmbHinhThucThanhToan.Location = new System.Drawing.Point(117, 108);
            this.cmbHinhThucThanhToan.Name = "cmbHinhThucThanhToan";
            this.cmbHinhThucThanhToan.Size = new System.Drawing.Size(506, 21);
            this.cmbHinhThucThanhToan.TabIndex = 12;
            this.cmbHinhThucThanhToan.Text = "Công nợ";
            // 
            // dtpNgayThanhToan
            // 
            this.dtpNgayThanhToan.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpNgayThanhToan.Location = new System.Drawing.Point(732, 108);
            this.dtpNgayThanhToan.Name = "dtpNgayThanhToan";
            this.dtpNgayThanhToan.Size = new System.Drawing.Size(156, 20);
            this.dtpNgayThanhToan.TabIndex = 13;
            // 
            // lueHinhThucThanhToan
            // 
            this.lueHinhThucThanhToan.Location = new System.Drawing.Point(117, 133);
            this.lueHinhThucThanhToan.Name = "lueHinhThucThanhToan";
            this.lueHinhThucThanhToan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueHinhThucThanhToan.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name12", "Name12", 20, DevExpress.Utils.FormatType.None, "dsssssd", true, DevExpress.Utils.HorzAlignment.Default)});
            this.lueHinhThucThanhToan.Size = new System.Drawing.Size(506, 20);
            this.lueHinhThucThanhToan.StyleController = this.layoutControl1;
            this.lueHinhThucThanhToan.TabIndex = 14;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(117, 60);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(183, 20);
            this.txtGhiChu.TabIndex = 7;
            // 
            // txtSoPhieuVietTay
            // 
            this.txtSoPhieuVietTay.Location = new System.Drawing.Point(409, 84);
            this.txtSoPhieuVietTay.Name = "txtSoPhieuVietTay";
            this.txtSoPhieuVietTay.Size = new System.Drawing.Size(214, 20);
            this.txtSoPhieuVietTay.TabIndex = 10;
            // 
            // lueTenNhanVien
            // 
            this.lueTenNhanVien.Location = new System.Drawing.Point(732, 60);
            this.lueTenNhanVien.Name = "lueTenNhanVien";
            this.lueTenNhanVien.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTenNhanVien.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenNV", "Họ tên"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("id_NhanVien", "Mã")});
            this.lueTenNhanVien.Size = new System.Drawing.Size(156, 20);
            this.lueTenNhanVien.StyleController = this.layoutControl1;
            this.lueTenNhanVien.TabIndex = 9;
            // 
            // txtSoHoaDonVAT
            // 
            this.txtSoHoaDonVAT.Location = new System.Drawing.Point(409, 60);
            this.txtSoHoaDonVAT.Name = "txtSoHoaDonVAT";
            this.txtSoHoaDonVAT.Size = new System.Drawing.Size(214, 20);
            this.txtSoHoaDonVAT.TabIndex = 8;
            // 
            // txtPhieu
            // 
            this.txtPhieu.Location = new System.Drawing.Point(732, 84);
            this.txtPhieu.Name = "txtPhieu";
            this.txtPhieu.Size = new System.Drawing.Size(156, 20);
            this.txtPhieu.StyleController = this.layoutControl1;
            this.txtPhieu.TabIndex = 11;
            // 
            // dtpNgayThangXuat
            // 
            this.dtpNgayThangXuat.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpNgayThangXuat.Location = new System.Drawing.Point(732, 36);
            this.dtpNgayThangXuat.Name = "dtpNgayThangXuat";
            this.dtpNgayThangXuat.Size = new System.Drawing.Size(156, 20);
            this.dtpNgayThangXuat.TabIndex = 6;
            // 
            // txtSdt
            // 
            this.txtSdt.Location = new System.Drawing.Point(409, 36);
            this.txtSdt.Name = "txtSdt";
            this.txtSdt.Size = new System.Drawing.Size(214, 20);
            this.txtSdt.TabIndex = 5;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(117, 36);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(183, 20);
            this.txtDiaChi.TabIndex = 4;
            // 
            // lueKhoNhap
            // 
            this.lueKhoNhap.Location = new System.Drawing.Point(732, 12);
            this.lueKhoNhap.Name = "lueKhoNhap";
            this.lueKhoNhap.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueKhoNhap.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenKho", "Kho"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id_Kho", "Mã")});
            this.lueKhoNhap.Size = new System.Drawing.Size(156, 20);
            this.lueKhoNhap.StyleController = this.layoutControl1;
            this.lueKhoNhap.TabIndex = 3;
            // 
            // lueMaNhaCungCap
            // 
            this.lueMaNhaCungCap.Location = new System.Drawing.Point(409, 12);
            this.lueMaNhaCungCap.Name = "lueMaNhaCungCap";
            this.lueMaNhaCungCap.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueMaNhaCungCap.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenNCC", "Nhà cung cấp"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("id_NCC", "Mã")});
            this.lueMaNhaCungCap.Size = new System.Drawing.Size(214, 20);
            this.lueMaNhaCungCap.StyleController = this.layoutControl1;
            this.lueMaNhaCungCap.TabIndex = 2;
            // 
            // lueTenNhaCungCap
            // 
            this.lueTenNhaCungCap.Location = new System.Drawing.Point(117, 12);
            this.lueTenNhaCungCap.Name = "lueTenNhaCungCap";
            this.lueTenNhaCungCap.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTenNhaCungCap.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenNCC", "Nhà cung cấp"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("id_NCC", "Mã")});
            this.lueTenNhaCungCap.Size = new System.Drawing.Size(183, 20);
            this.lueTenNhaCungCap.StyleController = this.layoutControl1;
            this.lueTenNhaCungCap.TabIndex = 0;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem7,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem12});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(900, 177);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lueTenNhaCungCap;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(292, 24);
            this.layoutControlItem1.Text = "Tên nhà cung cấp";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 72);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(292, 24);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lueMaNhaCungCap;
            this.layoutControlItem2.Location = new System.Drawing.Point(292, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(323, 24);
            this.layoutControlItem2.Text = "Mã nhà cung cấp";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.lueKhoNhap;
            this.layoutControlItem3.Location = new System.Drawing.Point(615, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(265, 24);
            this.layoutControlItem3.Text = "Kho nhập";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtDiaChi;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(292, 24);
            this.layoutControlItem4.Text = "Địa chỉ";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtSdt;
            this.layoutControlItem5.Location = new System.Drawing.Point(292, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(323, 24);
            this.layoutControlItem5.Text = "Điện thoại";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.dtpNgayThangXuat;
            this.layoutControlItem6.Location = new System.Drawing.Point(615, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(265, 24);
            this.layoutControlItem6.Text = "Ngày";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtSoHoaDonVAT;
            this.layoutControlItem8.Location = new System.Drawing.Point(292, 48);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(323, 24);
            this.layoutControlItem8.Text = "Số hóa đơn VAT";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.lueTenNhanVien;
            this.layoutControlItem9.Location = new System.Drawing.Point(615, 48);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(265, 24);
            this.layoutControlItem9.Text = "Nhân viên";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtPhieu;
            this.layoutControlItem7.Location = new System.Drawing.Point(615, 72);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(265, 24);
            this.layoutControlItem7.Text = "Phiếu";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txtSoPhieuVietTay;
            this.layoutControlItem10.Location = new System.Drawing.Point(292, 72);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(323, 24);
            this.layoutControlItem10.Text = "Số phiếu viết tay";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.txtGhiChu;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(292, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(292, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(292, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "Ghi chú";
            this.layoutControlItem11.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.lueHinhThucThanhToan;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 121);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(615, 36);
            this.layoutControlItem13.Text = "Hình thức thanh toán";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.dtpNgayThanhToan;
            this.layoutControlItem14.Location = new System.Drawing.Point(615, 96);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(265, 61);
            this.layoutControlItem14.Text = "Hạn thanh toán";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.cmbHinhThucThanhToan;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(615, 25);
            this.layoutControlItem12.Text = "Đ.Khoản, T Toán";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(102, 13);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnDongPhieuNhapHang);
            this.panel2.Controls.Add(this.btnNapLaiPhieuHang);
            this.panel2.Controls.Add(this.btnInPhieuHang);
            this.panel2.Controls.Add(this.btnLuuVaThem);
            this.panel2.Controls.Add(this.btntaoMoiPhieuNhapHang);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(900, 34);
            this.panel2.TabIndex = 0;
            // 
            // btnDongPhieuNhapHang
            // 
            this.btnDongPhieuNhapHang.ImageUri.Uri = "Cancel;Size16x16;Colored";
            this.btnDongPhieuNhapHang.Location = new System.Drawing.Point(327, 5);
            this.btnDongPhieuNhapHang.Name = "btnDongPhieuNhapHang";
            this.btnDongPhieuNhapHang.Size = new System.Drawing.Size(75, 23);
            this.btnDongPhieuNhapHang.TabIndex = 4;
            this.btnDongPhieuNhapHang.Text = "Đóng";
            this.btnDongPhieuNhapHang.Click += new System.EventHandler(this.btnDongPhieuNhapHang_Click);
            // 
            // btnNapLaiPhieuHang
            // 
            this.btnNapLaiPhieuHang.ImageUri.Uri = "Refresh;Size16x16;Colored";
            this.btnNapLaiPhieuHang.Location = new System.Drawing.Point(246, 5);
            this.btnNapLaiPhieuHang.Name = "btnNapLaiPhieuHang";
            this.btnNapLaiPhieuHang.Size = new System.Drawing.Size(75, 23);
            this.btnNapLaiPhieuHang.TabIndex = 3;
            this.btnNapLaiPhieuHang.Text = "Nạp lại";
            this.btnNapLaiPhieuHang.Click += new System.EventHandler(this.btnNapLaiPhieuHang_Click);
            // 
            // btnInPhieuHang
            // 
            this.btnInPhieuHang.ImageUri.Uri = "Print;Size16x16;Colored";
            this.btnInPhieuHang.Location = new System.Drawing.Point(165, 3);
            this.btnInPhieuHang.Name = "btnInPhieuHang";
            this.btnInPhieuHang.Size = new System.Drawing.Size(75, 23);
            this.btnInPhieuHang.TabIndex = 2;
            this.btnInPhieuHang.Text = "In";
            this.btnInPhieuHang.Click += new System.EventHandler(this.btnInPhieuHang_Click);
            // 
            // btnLuuVaThem
            // 
            this.btnLuuVaThem.ImageUri.Uri = "SaveAll;Size16x16;Colored";
            this.btnLuuVaThem.Location = new System.Drawing.Point(84, 3);
            this.btnLuuVaThem.Name = "btnLuuVaThem";
            this.btnLuuVaThem.Size = new System.Drawing.Size(75, 23);
            this.btnLuuVaThem.TabIndex = 1;
            this.btnLuuVaThem.Text = "Lưu & thêm";
            this.btnLuuVaThem.Click += new System.EventHandler(this.btnLuuVaThem_Click);
            // 
            // btntaoMoiPhieuNhapHang
            // 
            this.btntaoMoiPhieuNhapHang.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btntaoMoiPhieuNhapHang.ImageUri.Uri = "New;Size16x16;Colored";
            this.btntaoMoiPhieuNhapHang.Location = new System.Drawing.Point(3, 3);
            this.btntaoMoiPhieuNhapHang.Name = "btntaoMoiPhieuNhapHang";
            this.btntaoMoiPhieuNhapHang.Size = new System.Drawing.Size(75, 23);
            this.btntaoMoiPhieuNhapHang.TabIndex = 0;
            this.btntaoMoiPhieuNhapHang.Text = "Tạo mới";
            this.btntaoMoiPhieuNhapHang.Click += new System.EventHandler(this.btntaoMoiPhieuNhapHang_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel5);
            this.tabPage2.Controls.Add(this.panel4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(906, 497);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.grcDanhSachHHTheoChungTu);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 80);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(900, 414);
            this.panel5.TabIndex = 1;
            // 
            // grcDanhSachHHTheoChungTu
            // 
            this.grcDanhSachHHTheoChungTu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcDanhSachHHTheoChungTu.Location = new System.Drawing.Point(0, 0);
            this.grcDanhSachHHTheoChungTu.MainView = this.griđánhachhangHoaTheoChungTu;
            this.grcDanhSachHHTheoChungTu.Name = "grcDanhSachHHTheoChungTu";
            this.grcDanhSachHHTheoChungTu.Size = new System.Drawing.Size(900, 414);
            this.grcDanhSachHHTheoChungTu.TabIndex = 0;
            this.grcDanhSachHHTheoChungTu.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.griđánhachhangHoaTheoChungTu});
            // 
            // griđánhachhangHoaTheoChungTu
            // 
            this.griđánhachhangHoaTheoChungTu.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn9,
            this.gridColumn17,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26});
            this.griđánhachhangHoaTheoChungTu.GridControl = this.grcDanhSachHHTheoChungTu;
            this.griđánhachhangHoaTheoChungTu.GroupCount = 2;
            this.griđánhachhangHoaTheoChungTu.Name = "griđánhachhangHoaTheoChungTu";
            this.griđánhachhangHoaTheoChungTu.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn9, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn17, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Kho hàng";
            this.gridColumn9.FieldName = "TenKho";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 0;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Chứng từ";
            this.gridColumn17.FieldName = "id_ChungTuNhap";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 0;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Mã hàng";
            this.gridColumn20.FieldName = "id_HangHoa";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 0;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Tên hàng";
            this.gridColumn21.FieldName = "Tenhang";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 1;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Kho hàng";
            this.gridColumn22.FieldName = "TenKho";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 2;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "ĐVT";
            this.gridColumn23.FieldName = "TenDonVi";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 3;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Đơn giá";
            this.gridColumn24.FieldName = "DonGia";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 4;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Số lượng";
            this.gridColumn25.FieldName = "SoLuong";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 5;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Thành tiền";
            this.gridColumn26.FieldName = "ThanhToan";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 6;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.layoutControl2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(900, 77);
            this.panel4.TabIndex = 0;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.lueXemTheo);
            this.layoutControl2.Controls.Add(this.btnXoaChungTu);
            this.layoutControl2.Controls.Add(this.btnInChungTu);
            this.layoutControl2.Controls.Add(this.btnXuatChungTu);
            this.layoutControl2.Controls.Add(this.btnDongChungTu);
            this.layoutControl2.Controls.Add(this.btnSuaChungTu);
            this.layoutControl2.Controls.Add(this.btnTaoMoiChungTu);
            this.layoutControl2.Controls.Add(this.btnXemchungTu);
            this.layoutControl2.Controls.Add(this.dtpNgayKetThuc);
            this.layoutControl2.Controls.Add(this.dtpNgayBatDau);
            this.layoutControl2.Controls.Add(this.cmbChonLoai);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(900, 77);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // lueXemTheo
            // 
            this.lueXemTheo.Location = new System.Drawing.Point(60, 47);
            this.lueXemTheo.Name = "lueXemTheo";
            this.lueXemTheo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueXemTheo.Size = new System.Drawing.Size(427, 20);
            this.lueXemTheo.StyleController = this.layoutControl2;
            this.lueXemTheo.TabIndex = 14;
            // 
            // btnXoaChungTu
            // 
            this.btnXoaChungTu.ImageUri.Uri = "Close;Size16x16;Colored";
            this.btnXoaChungTu.Location = new System.Drawing.Point(585, 12);
            this.btnXoaChungTu.Name = "btnXoaChungTu";
            this.btnXoaChungTu.Size = new System.Drawing.Size(64, 22);
            this.btnXoaChungTu.StyleController = this.layoutControl2;
            this.btnXoaChungTu.TabIndex = 13;
            this.btnXoaChungTu.Text = "Xóa";
            this.btnXoaChungTu.Click += new System.EventHandler(this.btnXoaChungTu_Click);
            // 
            // btnInChungTu
            // 
            this.btnInChungTu.ImageUri.Uri = "Print;Size16x16;Colored";
            this.btnInChungTu.Location = new System.Drawing.Point(653, 12);
            this.btnInChungTu.Name = "btnInChungTu";
            this.btnInChungTu.Size = new System.Drawing.Size(70, 22);
            this.btnInChungTu.StyleController = this.layoutControl2;
            this.btnInChungTu.TabIndex = 12;
            this.btnInChungTu.Text = "In";
            // 
            // btnXuatChungTu
            // 
            this.btnXuatChungTu.ImageUri.Uri = "AlignHorizontalCenter;Size16x16;Colored";
            this.btnXuatChungTu.Location = new System.Drawing.Point(727, 12);
            this.btnXuatChungTu.Name = "btnXuatChungTu";
            this.btnXuatChungTu.Size = new System.Drawing.Size(70, 22);
            this.btnXuatChungTu.StyleController = this.layoutControl2;
            this.btnXuatChungTu.TabIndex = 11;
            this.btnXuatChungTu.Text = "Xuất";
            this.btnXuatChungTu.Click += new System.EventHandler(this.btnXuatChungTu_Click);
            // 
            // btnDongChungTu
            // 
            this.btnDongChungTu.ImageUri.Uri = "Cancel;Size16x16;Colored";
            this.btnDongChungTu.Location = new System.Drawing.Point(801, 12);
            this.btnDongChungTu.Name = "btnDongChungTu";
            this.btnDongChungTu.Size = new System.Drawing.Size(87, 22);
            this.btnDongChungTu.StyleController = this.layoutControl2;
            this.btnDongChungTu.TabIndex = 10;
            this.btnDongChungTu.Text = "Đóng";
            // 
            // btnSuaChungTu
            // 
            this.btnSuaChungTu.ImageUri.Uri = "Replace;Size16x16;Colored";
            this.btnSuaChungTu.Location = new System.Drawing.Point(491, 12);
            this.btnSuaChungTu.Name = "btnSuaChungTu";
            this.btnSuaChungTu.Size = new System.Drawing.Size(90, 22);
            this.btnSuaChungTu.StyleController = this.layoutControl2;
            this.btnSuaChungTu.TabIndex = 9;
            this.btnSuaChungTu.Text = "Sửa chữa";
            this.btnSuaChungTu.Click += new System.EventHandler(this.btnSuaChungTu_Click);
            // 
            // btnTaoMoiChungTu
            // 
            this.btnTaoMoiChungTu.ImageUri.Uri = "New;Size16x16;Colored";
            this.btnTaoMoiChungTu.Location = new System.Drawing.Point(408, 12);
            this.btnTaoMoiChungTu.Name = "btnTaoMoiChungTu";
            this.btnTaoMoiChungTu.Size = new System.Drawing.Size(79, 22);
            this.btnTaoMoiChungTu.StyleController = this.layoutControl2;
            this.btnTaoMoiChungTu.TabIndex = 8;
            this.btnTaoMoiChungTu.Text = "Tạo mới";
            // 
            // btnXemchungTu
            // 
            this.btnXemchungTu.ImageUri.Uri = "Find;Size16x16;Colored";
            this.btnXemchungTu.Location = new System.Drawing.Point(330, 12);
            this.btnXemchungTu.Name = "btnXemchungTu";
            this.btnXemchungTu.Size = new System.Drawing.Size(74, 22);
            this.btnXemchungTu.StyleController = this.layoutControl2;
            this.btnXemchungTu.TabIndex = 7;
            this.btnXemchungTu.Text = "Xem";
            // 
            // dtpNgayKetThuc
            // 
            this.dtpNgayKetThuc.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpNgayKetThuc.Location = new System.Drawing.Point(258, 12);
            this.dtpNgayKetThuc.Name = "dtpNgayKetThuc";
            this.dtpNgayKetThuc.Size = new System.Drawing.Size(68, 20);
            this.dtpNgayKetThuc.TabIndex = 6;
            // 
            // dtpNgayBatDau
            // 
            this.dtpNgayBatDau.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpNgayBatDau.Location = new System.Drawing.Point(142, 12);
            this.dtpNgayBatDau.Name = "dtpNgayBatDau";
            this.dtpNgayBatDau.Size = new System.Drawing.Size(64, 20);
            this.dtpNgayBatDau.TabIndex = 5;
            // 
            // cmbChonLoai
            // 
            this.cmbChonLoai.FormattingEnabled = true;
            this.cmbChonLoai.Items.AddRange(new object[] {
            "Quý 1",
            "Quý 2",
            "Quý 3",
            "Quý 4",
            "Hôm nay",
            "Tuần này",
            "Tháng này",
            "Quý này",
            "Tháng 1",
            "Tháng 2",
            "Tháng 3",
            "Tháng 4",
            "Tháng 5",
            "Tháng 6",
            "Tháng 7",
            "Tháng 8",
            "Tháng 9",
            "Tháng 10",
            "Tháng 11",
            "Tháng 12"});
            this.cmbChonLoai.Location = new System.Drawing.Point(60, 12);
            this.cmbChonLoai.Name = "cmbChonLoai";
            this.cmbChonLoai.Size = new System.Drawing.Size(30, 21);
            this.cmbChonLoai.TabIndex = 4;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem15,
            this.emptySpaceItem2,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem24,
            this.layoutControlItem25});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(900, 79);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.cmbChonLoai;
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(82, 25);
            this.layoutControlItem15.Text = "Chọn";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(45, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 25);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(198, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.dtpNgayBatDau;
            this.layoutControlItem16.Location = new System.Drawing.Point(82, 0);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(116, 25);
            this.layoutControlItem16.Text = "Từ";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(45, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.dtpNgayKetThuc;
            this.layoutControlItem17.Location = new System.Drawing.Point(198, 0);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(120, 35);
            this.layoutControlItem17.Text = "Đến";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(45, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.btnXemchungTu;
            this.layoutControlItem18.Location = new System.Drawing.Point(318, 0);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(78, 35);
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.btnTaoMoiChungTu;
            this.layoutControlItem19.Location = new System.Drawing.Point(396, 0);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(83, 35);
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.btnSuaChungTu;
            this.layoutControlItem20.Location = new System.Drawing.Point(479, 0);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(94, 59);
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextVisible = false;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.btnDongChungTu;
            this.layoutControlItem21.Location = new System.Drawing.Point(789, 0);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(91, 59);
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextVisible = false;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.btnXuatChungTu;
            this.layoutControlItem22.Location = new System.Drawing.Point(715, 0);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(74, 59);
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextVisible = false;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.btnInChungTu;
            this.layoutControlItem23.Location = new System.Drawing.Point(641, 0);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(74, 59);
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.btnXoaChungTu;
            this.layoutControlItem24.Location = new System.Drawing.Point(573, 0);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(68, 59);
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextVisible = false;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.lueXemTheo;
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 35);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(479, 24);
            this.layoutControlItem25.Text = "Xem theo";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(45, 13);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel9);
            this.tabPage3.Controls.Add(this.panel8);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(906, 497);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.grcDanhSachTheoHangHoa);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(3, 73);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(900, 421);
            this.panel9.TabIndex = 1;
            // 
            // grcDanhSachTheoHangHoa
            // 
            this.grcDanhSachTheoHangHoa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcDanhSachTheoHangHoa.Location = new System.Drawing.Point(0, 0);
            this.grcDanhSachTheoHangHoa.MainView = this.gridViewHHTheoCT;
            this.grcDanhSachTheoHangHoa.Name = "grcDanhSachTheoHangHoa";
            this.grcDanhSachTheoHangHoa.Size = new System.Drawing.Size(900, 421);
            this.grcDanhSachTheoHangHoa.TabIndex = 0;
            this.grcDanhSachTheoHangHoa.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewHHTheoCT});
            // 
            // gridViewHHTheoCT
            // 
            this.gridViewHHTheoCT.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn19,
            this.gridColumn18,
            this.gridColumn16,
            this.gridColumn15,
            this.gridColumn14,
            this.gridColumn13,
            this.gridColumn12,
            this.gridColumn8,
            this.gridColumn11});
            this.gridViewHHTheoCT.GridControl = this.grcDanhSachTheoHangHoa;
            this.gridViewHHTheoCT.Name = "gridViewHHTheoCT";
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Chứng từ";
            this.gridColumn10.FieldName = "id_ChungTuNhap";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 0;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Nhà cung cấp";
            this.gridColumn19.FieldName = "id_ChungTuNhap";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 1;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Ngày";
            this.gridColumn18.FieldName = "NgayNhap";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 2;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Mã hàng";
            this.gridColumn16.FieldName = "id_HangHoa";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 3;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Tên hàng";
            this.gridColumn15.FieldName = "Tenhang";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 4;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Đơn vị";
            this.gridColumn14.FieldName = "TenDonVi";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 5;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Kho hàng";
            this.gridColumn13.FieldName = "TenKho";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 6;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Đơn giá";
            this.gridColumn12.FieldName = "DonGia";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 7;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Số lượng";
            this.gridColumn8.FieldName = "SoLuong";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 8;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Thành tiền";
            this.gridColumn11.FieldName = "ThanhToan";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 9;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.layoutControl4);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(900, 70);
            this.panel8.TabIndex = 0;
            // 
            // layoutControl4
            // 
            this.layoutControl4.Controls.Add(this.btnXoaChungTuTheoCT);
            this.layoutControl4.Controls.Add(this.btnXemTheoHangHoa);
            this.layoutControl4.Controls.Add(this.lueXemDanhSachHangHoaTheo);
            this.layoutControl4.Controls.Add(this.btnDonghangHoa);
            this.layoutControl4.Controls.Add(this.btnXuatHangHoa);
            this.layoutControl4.Controls.Add(this.btnInHangHoa);
            this.layoutControl4.Controls.Add(this.btnSuaHangHoa);
            this.layoutControl4.Controls.Add(this.btnTaoMoihangHoa);
            this.layoutControl4.Controls.Add(this.dtpXemhangHoaKetThuc);
            this.layoutControl4.Controls.Add(this.dtpXemHangHoaTu);
            this.layoutControl4.Controls.Add(this.cmbLuachonXemTheoHangHoa);
            this.layoutControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl4.Location = new System.Drawing.Point(0, 0);
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.Root = this.layoutControlGroup4;
            this.layoutControl4.Size = new System.Drawing.Size(900, 70);
            this.layoutControl4.TabIndex = 0;
            this.layoutControl4.Text = "layoutControl4";
            // 
            // btnXoaChungTuTheoCT
            // 
            this.btnXoaChungTuTheoCT.ImageUri.Uri = "Close;Size16x16;Colored";
            this.btnXoaChungTuTheoCT.Location = new System.Drawing.Point(658, 12);
            this.btnXoaChungTuTheoCT.Name = "btnXoaChungTuTheoCT";
            this.btnXoaChungTuTheoCT.Size = new System.Drawing.Size(76, 22);
            this.btnXoaChungTuTheoCT.StyleController = this.layoutControl4;
            this.btnXoaChungTuTheoCT.TabIndex = 15;
            this.btnXoaChungTuTheoCT.Text = "Xóa";
            this.btnXoaChungTuTheoCT.Click += new System.EventHandler(this.btnXoaChungTuTheoCT_Click);
            // 
            // btnXemTheoHangHoa
            // 
            this.btnXemTheoHangHoa.ImageUri.Uri = "Zoom;Size16x16;Colored";
            this.btnXemTheoHangHoa.Location = new System.Drawing.Point(385, 12);
            this.btnXemTheoHangHoa.Name = "btnXemTheoHangHoa";
            this.btnXemTheoHangHoa.Size = new System.Drawing.Size(93, 22);
            this.btnXemTheoHangHoa.StyleController = this.layoutControl4;
            this.btnXemTheoHangHoa.TabIndex = 14;
            this.btnXemTheoHangHoa.Text = "Xem";
            this.btnXemTheoHangHoa.Click += new System.EventHandler(this.btnXemTheoHangHoa_Click);
            // 
            // lueXemDanhSachHangHoaTheo
            // 
            this.lueXemDanhSachHangHoaTheo.Location = new System.Drawing.Point(60, 38);
            this.lueXemDanhSachHangHoaTheo.Name = "lueXemDanhSachHangHoaTheo";
            this.lueXemDanhSachHangHoaTheo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueXemDanhSachHangHoaTheo.Size = new System.Drawing.Size(389, 20);
            this.lueXemDanhSachHangHoaTheo.StyleController = this.layoutControl4;
            this.lueXemDanhSachHangHoaTheo.TabIndex = 13;
            // 
            // btnDonghangHoa
            // 
            this.btnDonghangHoa.ImageUri.Uri = "Close;Size16x16;Colored";
            this.btnDonghangHoa.Location = new System.Drawing.Point(834, 12);
            this.btnDonghangHoa.Name = "btnDonghangHoa";
            this.btnDonghangHoa.Size = new System.Drawing.Size(54, 22);
            this.btnDonghangHoa.StyleController = this.layoutControl4;
            this.btnDonghangHoa.TabIndex = 12;
            this.btnDonghangHoa.Text = "Đóng";
            this.btnDonghangHoa.Click += new System.EventHandler(this.btnDonghangHoa_Click);
            // 
            // btnXuatHangHoa
            // 
            this.btnXuatHangHoa.ImageUri.Uri = "SendBehindText;Size16x16;Colored";
            this.btnXuatHangHoa.Location = new System.Drawing.Point(780, 12);
            this.btnXuatHangHoa.Name = "btnXuatHangHoa";
            this.btnXuatHangHoa.Size = new System.Drawing.Size(50, 22);
            this.btnXuatHangHoa.StyleController = this.layoutControl4;
            this.btnXuatHangHoa.TabIndex = 11;
            this.btnXuatHangHoa.Text = "Xuất";
            this.btnXuatHangHoa.Click += new System.EventHandler(this.btnXuatHangHoa_Click);
            // 
            // btnInHangHoa
            // 
            this.btnInHangHoa.ImageUri.Uri = "Print;Size16x16;Colored";
            this.btnInHangHoa.Location = new System.Drawing.Point(738, 12);
            this.btnInHangHoa.Name = "btnInHangHoa";
            this.btnInHangHoa.Size = new System.Drawing.Size(38, 22);
            this.btnInHangHoa.StyleController = this.layoutControl4;
            this.btnInHangHoa.TabIndex = 10;
            this.btnInHangHoa.Text = "In";
            // 
            // btnSuaHangHoa
            // 
            this.btnSuaHangHoa.ImageUri.Uri = "Replace;Size16x16;Colored";
            this.btnSuaHangHoa.Location = new System.Drawing.Point(580, 12);
            this.btnSuaHangHoa.Name = "btnSuaHangHoa";
            this.btnSuaHangHoa.Size = new System.Drawing.Size(74, 22);
            this.btnSuaHangHoa.StyleController = this.layoutControl4;
            this.btnSuaHangHoa.TabIndex = 9;
            this.btnSuaHangHoa.Text = "Sửa chữa";
            this.btnSuaHangHoa.Click += new System.EventHandler(this.simpleButton17_Click);
            // 
            // btnTaoMoihangHoa
            // 
            this.btnTaoMoihangHoa.ImageUri.Uri = "Edit;Size16x16;Colored";
            this.btnTaoMoihangHoa.Location = new System.Drawing.Point(482, 12);
            this.btnTaoMoihangHoa.Name = "btnTaoMoihangHoa";
            this.btnTaoMoihangHoa.Size = new System.Drawing.Size(94, 22);
            this.btnTaoMoihangHoa.StyleController = this.layoutControl4;
            this.btnTaoMoihangHoa.TabIndex = 8;
            this.btnTaoMoihangHoa.Text = "Tạo mới";
            this.btnTaoMoihangHoa.Click += new System.EventHandler(this.btnTaoMoihangHoa_Click);
            // 
            // dtpXemhangHoaKetThuc
            // 
            this.dtpXemhangHoaKetThuc.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpXemhangHoaKetThuc.Location = new System.Drawing.Point(301, 12);
            this.dtpXemhangHoaKetThuc.Name = "dtpXemhangHoaKetThuc";
            this.dtpXemhangHoaKetThuc.Size = new System.Drawing.Size(80, 20);
            this.dtpXemhangHoaKetThuc.TabIndex = 6;
            // 
            // dtpXemHangHoaTu
            // 
            this.dtpXemHangHoaTu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpXemHangHoaTu.Location = new System.Drawing.Point(163, 12);
            this.dtpXemHangHoaTu.Name = "dtpXemHangHoaTu";
            this.dtpXemHangHoaTu.Size = new System.Drawing.Size(86, 20);
            this.dtpXemHangHoaTu.TabIndex = 5;
            // 
            // cmbLuachonXemTheoHangHoa
            // 
            this.cmbLuachonXemTheoHangHoa.FormattingEnabled = true;
            this.cmbLuachonXemTheoHangHoa.Location = new System.Drawing.Point(60, 12);
            this.cmbLuachonXemTheoHangHoa.Name = "cmbLuachonXemTheoHangHoa";
            this.cmbLuachonXemTheoHangHoa.Size = new System.Drawing.Size(51, 21);
            this.cmbLuachonXemTheoHangHoa.TabIndex = 4;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem37,
            this.emptySpaceItem4,
            this.layoutControlItem38,
            this.layoutControlItem39,
            this.layoutControlItem41,
            this.layoutControlItem42,
            this.layoutControlItem43,
            this.layoutControlItem44,
            this.layoutControlItem45,
            this.layoutControlItem46,
            this.layoutControlItem40,
            this.layoutControlItem50});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(900, 70);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.cmbLuachonXemTheoHangHoa;
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(103, 26);
            this.layoutControlItem37.Text = "Lựa chọn";
            this.layoutControlItem37.TextSize = new System.Drawing.Size(45, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(441, 26);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(439, 24);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.dtpXemHangHoaTu;
            this.layoutControlItem38.Location = new System.Drawing.Point(103, 0);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(138, 26);
            this.layoutControlItem38.Text = "Từ";
            this.layoutControlItem38.TextSize = new System.Drawing.Size(45, 13);
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.Control = this.dtpXemhangHoaKetThuc;
            this.layoutControlItem39.Location = new System.Drawing.Point(241, 0);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(132, 26);
            this.layoutControlItem39.Text = "Đến";
            this.layoutControlItem39.TextSize = new System.Drawing.Size(45, 13);
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.Control = this.btnTaoMoihangHoa;
            this.layoutControlItem41.Location = new System.Drawing.Point(470, 0);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(98, 26);
            this.layoutControlItem41.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem41.TextVisible = false;
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.Control = this.btnSuaHangHoa;
            this.layoutControlItem42.Location = new System.Drawing.Point(568, 0);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(78, 26);
            this.layoutControlItem42.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem42.TextVisible = false;
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.Control = this.btnInHangHoa;
            this.layoutControlItem43.Location = new System.Drawing.Point(726, 0);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(42, 26);
            this.layoutControlItem43.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem43.TextVisible = false;
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.Control = this.btnXuatHangHoa;
            this.layoutControlItem44.Location = new System.Drawing.Point(768, 0);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(54, 26);
            this.layoutControlItem44.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem44.TextVisible = false;
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.Control = this.btnDonghangHoa;
            this.layoutControlItem45.Location = new System.Drawing.Point(822, 0);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(58, 26);
            this.layoutControlItem45.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem45.TextVisible = false;
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.Control = this.lueXemDanhSachHangHoaTheo;
            this.layoutControlItem46.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(441, 24);
            this.layoutControlItem46.Text = "Xem theo";
            this.layoutControlItem46.TextSize = new System.Drawing.Size(45, 13);
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.Control = this.btnXemTheoHangHoa;
            this.layoutControlItem40.Location = new System.Drawing.Point(373, 0);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(97, 26);
            this.layoutControlItem40.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem40.TextVisible = false;
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.Control = this.btnXoaChungTuTheoCT;
            this.layoutControlItem50.Location = new System.Drawing.Point(646, 0);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(80, 26);
            this.layoutControlItem50.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem50.TextVisible = false;
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane",
            "DevExpress.XtraBars.TabFormControl"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("3430b2f2-71c1-4c9f-845a-8703697fb5f4");
            this.dockPanel1.Location = new System.Drawing.Point(0, 0);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.OriginalSize = new System.Drawing.Size(172, 200);
            this.dockPanel1.Size = new System.Drawing.Size(172, 523);
            this.dockPanel1.Text = "Chức năng";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.navBarControl1);
            this.dockPanel1_Container.Controls.Add(this.navBarControl2);
            this.dockPanel1_Container.Controls.Add(this.btnXemPhieuMuaHang);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(163, 496);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // navBarControl1
            // 
            this.navBarControl1.ActiveGroup = this.tabThemDanhMuc;
            this.navBarControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navBarControl1.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.tabThemDanhMuc});
            this.navBarControl1.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.LinkHangHoa,
            this.LinkKhachHang,
            this.LinkKhoHang});
            this.navBarControl1.Location = new System.Drawing.Point(0, 285);
            this.navBarControl1.Name = "navBarControl1";
            this.navBarControl1.OptionsNavPane.ExpandedWidth = 163;
            this.navBarControl1.Size = new System.Drawing.Size(163, 211);
            this.navBarControl1.TabIndex = 2;
            this.navBarControl1.Text = "Thêm danh mục";
            // 
            // tabThemDanhMuc
            // 
            this.tabThemDanhMuc.Caption = "Thêm danh mục";
            this.tabThemDanhMuc.Expanded = true;
            this.tabThemDanhMuc.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText;
            this.tabThemDanhMuc.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.LinkHangHoa),
            new DevExpress.XtraNavBar.NavBarItemLink(this.LinkKhachHang),
            new DevExpress.XtraNavBar.NavBarItemLink(this.LinkKhoHang)});
            this.tabThemDanhMuc.Name = "tabThemDanhMuc";
            this.tabThemDanhMuc.Visible = false;
            // 
            // LinkHangHoa
            // 
            this.LinkHangHoa.Caption = "Hàng hóa";
            this.LinkHangHoa.LargeImage = ((System.Drawing.Image)(resources.GetObject("LinkHangHoa.LargeImage")));
            this.LinkHangHoa.Name = "LinkHangHoa";
            this.LinkHangHoa.SmallImage = ((System.Drawing.Image)(resources.GetObject("LinkHangHoa.SmallImage")));
            this.LinkHangHoa.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.LinkHangHoa_LinkClicked);
            // 
            // LinkKhachHang
            // 
            this.LinkKhachHang.Caption = "Khách hàng";
            this.LinkKhachHang.LargeImage = ((System.Drawing.Image)(resources.GetObject("LinkKhachHang.LargeImage")));
            this.LinkKhachHang.Name = "LinkKhachHang";
            this.LinkKhachHang.SmallImage = ((System.Drawing.Image)(resources.GetObject("LinkKhachHang.SmallImage")));
            this.LinkKhachHang.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.LinkKhachHang_LinkClicked);
            // 
            // LinkKhoHang
            // 
            this.LinkKhoHang.Caption = "Kho hàng";
            this.LinkKhoHang.LargeImage = ((System.Drawing.Image)(resources.GetObject("LinkKhoHang.LargeImage")));
            this.LinkKhoHang.Name = "LinkKhoHang";
            this.LinkKhoHang.SmallImage = ((System.Drawing.Image)(resources.GetObject("LinkKhoHang.SmallImage")));
            this.LinkKhoHang.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.LinkKhoHang_LinkClicked);
            // 
            // navBarControl2
            // 
            this.navBarControl2.ActiveGroup = this.navBarGroup2;
            this.navBarControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.navBarControl2.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarGroup2});
            this.navBarControl2.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.btnTheoChungTu,
            this.btnTheoHangHoa});
            this.navBarControl2.Location = new System.Drawing.Point(0, 125);
            this.navBarControl2.Name = "navBarControl2";
            this.navBarControl2.OptionsNavPane.ExpandedWidth = 163;
            this.navBarControl2.Size = new System.Drawing.Size(163, 160);
            this.navBarControl2.TabIndex = 1;
            this.navBarControl2.Text = "navBarControl2";
            // 
            // navBarGroup2
            // 
            this.navBarGroup2.Caption = "Bảng kê";
            this.navBarGroup2.Expanded = true;
            this.navBarGroup2.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText;
            this.navBarGroup2.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnTheoChungTu),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnTheoHangHoa)});
            this.navBarGroup2.Name = "navBarGroup2";
            // 
            // btnTheoChungTu
            // 
            this.btnTheoChungTu.Caption = "Theo chứng từ";
            this.btnTheoChungTu.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnTheoChungTu.LargeImage")));
            this.btnTheoChungTu.Name = "btnTheoChungTu";
            this.btnTheoChungTu.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnTheoChungTu.SmallImage")));
            this.btnTheoChungTu.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnTheoChungTu_LinkClicked);
            // 
            // btnTheoHangHoa
            // 
            this.btnTheoHangHoa.Caption = "Theo hàng hóa";
            this.btnTheoHangHoa.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnTheoHangHoa.LargeImage")));
            this.btnTheoHangHoa.Name = "btnTheoHangHoa";
            this.btnTheoHangHoa.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnTheoHangHoa.SmallImage")));
            this.btnTheoHangHoa.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnTheoHangHoa_LinkClicked);
            // 
            // btnXemPhieuMuaHang
            // 
            this.btnXemPhieuMuaHang.ActiveGroup = this.navBarGroup1;
            this.btnXemPhieuMuaHang.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXemPhieuMuaHang.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarGroup1});
            this.btnXemPhieuMuaHang.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.navBarItem1});
            this.btnXemPhieuMuaHang.Location = new System.Drawing.Point(0, 0);
            this.btnXemPhieuMuaHang.Name = "btnXemPhieuMuaHang";
            this.btnXemPhieuMuaHang.OptionsNavPane.ExpandedWidth = 163;
            this.btnXemPhieuMuaHang.Size = new System.Drawing.Size(163, 125);
            this.btnXemPhieuMuaHang.TabIndex = 0;
            this.btnXemPhieuMuaHang.Text = "navBarControl1";
            // 
            // navBarGroup1
            // 
            this.navBarGroup1.Caption = "Nhập hàng";
            this.navBarGroup1.Expanded = true;
            this.navBarGroup1.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText;
            this.navBarGroup1.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem1)});
            this.navBarGroup1.Name = "navBarGroup1";
            // 
            // navBarItem1
            // 
            this.navBarItem1.Caption = "Phiếu nhập hàng";
            this.navBarItem1.LargeImage = ((System.Drawing.Image)(resources.GetObject("navBarItem1.LargeImage")));
            this.navBarItem1.Name = "navBarItem1";
            this.navBarItem1.SmallImage = ((System.Drawing.Image)(resources.GetObject("navBarItem1.SmallImage")));
            this.navBarItem1.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem1_LinkClicked);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel1.Controls.Add(this.lbTenHienThi);
            this.panel1.Location = new System.Drawing.Point(176, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(891, 24);
            this.panel1.TabIndex = 3;
            // 
            // lbTenHienThi
            // 
            this.lbTenHienThi.AutoSize = true;
            this.lbTenHienThi.Location = new System.Drawing.Point(3, 6);
            this.lbTenHienThi.Name = "lbTenHienThi";
            this.lbTenHienThi.Size = new System.Drawing.Size(0, 13);
            this.lbTenHienThi.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.gridControl3);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(200, 100);
            this.panel6.TabIndex = 1;
            // 
            // gridControl3
            // 
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(200, 100);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            // 
            // panel7
            // 
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(200, 100);
            this.panel7.TabIndex = 0;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.lookUpEdit8);
            this.layoutControl3.Controls.Add(this.simpleButton8);
            this.layoutControl3.Controls.Add(this.simpleButton9);
            this.layoutControl3.Controls.Add(this.simpleButton10);
            this.layoutControl3.Controls.Add(this.simpleButton11);
            this.layoutControl3.Controls.Add(this.simpleButton12);
            this.layoutControl3.Controls.Add(this.simpleButton13);
            this.layoutControl3.Controls.Add(this.simpleButton14);
            this.layoutControl3.Controls.Add(this.dateTimePicker5);
            this.layoutControl3.Controls.Add(this.dateTimePicker6);
            this.layoutControl3.Controls.Add(this.comboBox2);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 0);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(857, 77);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl2";
            // 
            // lookUpEdit8
            // 
            this.lookUpEdit8.Location = new System.Drawing.Point(60, 47);
            this.lookUpEdit8.Name = "lookUpEdit8";
            this.lookUpEdit8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit8.Size = new System.Drawing.Size(391, 20);
            this.lookUpEdit8.StyleController = this.layoutControl3;
            this.lookUpEdit8.TabIndex = 14;
            // 
            // simpleButton8
            // 
            this.simpleButton8.ImageUri.Uri = "Close;Size16x16;Colored";
            this.simpleButton8.Location = new System.Drawing.Point(543, 12);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(60, 22);
            this.simpleButton8.StyleController = this.layoutControl3;
            this.simpleButton8.TabIndex = 13;
            this.simpleButton8.Text = "Xóa";
            // 
            // simpleButton9
            // 
            this.simpleButton9.ImageUri.Uri = "Print;Size16x16;Colored";
            this.simpleButton9.Location = new System.Drawing.Point(607, 12);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(66, 22);
            this.simpleButton9.StyleController = this.layoutControl3;
            this.simpleButton9.TabIndex = 12;
            this.simpleButton9.Text = "In";
            // 
            // simpleButton10
            // 
            this.simpleButton10.ImageUri.Uri = "AlignHorizontalCenter;Size16x16;Colored";
            this.simpleButton10.Location = new System.Drawing.Point(677, 12);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(66, 22);
            this.simpleButton10.StyleController = this.layoutControl3;
            this.simpleButton10.TabIndex = 11;
            this.simpleButton10.Text = "Xuất";
            // 
            // simpleButton11
            // 
            this.simpleButton11.ImageUri.Uri = "Cancel;Size16x16;Colored";
            this.simpleButton11.Location = new System.Drawing.Point(747, 12);
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.Size = new System.Drawing.Size(81, 22);
            this.simpleButton11.StyleController = this.layoutControl3;
            this.simpleButton11.TabIndex = 10;
            this.simpleButton11.Text = "Đóng";
            // 
            // simpleButton12
            // 
            this.simpleButton12.ImageUri.Uri = "Replace;Size16x16;Colored";
            this.simpleButton12.Location = new System.Drawing.Point(455, 12);
            this.simpleButton12.Name = "simpleButton12";
            this.simpleButton12.Size = new System.Drawing.Size(84, 22);
            this.simpleButton12.StyleController = this.layoutControl3;
            this.simpleButton12.TabIndex = 9;
            this.simpleButton12.Text = "Sửa chữa";
            // 
            // simpleButton13
            // 
            this.simpleButton13.ImageUri.Uri = "New;Size16x16;Colored";
            this.simpleButton13.Location = new System.Drawing.Point(377, 12);
            this.simpleButton13.Name = "simpleButton13";
            this.simpleButton13.Size = new System.Drawing.Size(74, 22);
            this.simpleButton13.StyleController = this.layoutControl3;
            this.simpleButton13.TabIndex = 8;
            this.simpleButton13.Text = "Tạo mới";
            // 
            // simpleButton14
            // 
            this.simpleButton14.ImageUri.Uri = "Find;Size16x16;Colored";
            this.simpleButton14.Location = new System.Drawing.Point(304, 12);
            this.simpleButton14.Name = "simpleButton14";
            this.simpleButton14.Size = new System.Drawing.Size(69, 22);
            this.simpleButton14.StyleController = this.layoutControl3;
            this.simpleButton14.TabIndex = 7;
            this.simpleButton14.Text = "Xem";
            // 
            // dateTimePicker5
            // 
            this.dateTimePicker5.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker5.Location = new System.Drawing.Point(240, 12);
            this.dateTimePicker5.Name = "dateTimePicker5";
            this.dateTimePicker5.Size = new System.Drawing.Size(60, 20);
            this.dateTimePicker5.TabIndex = 6;
            // 
            // dateTimePicker6
            // 
            this.dateTimePicker6.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker6.Location = new System.Drawing.Point(135, 12);
            this.dateTimePicker6.Name = "dateTimePicker6";
            this.dateTimePicker6.Size = new System.Drawing.Size(53, 20);
            this.dateTimePicker6.TabIndex = 5;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(60, 12);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(23, 21);
            this.comboBox2.TabIndex = 4;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem26,
            this.emptySpaceItem3,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem31,
            this.layoutControlItem32,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.layoutControlItem35,
            this.layoutControlItem36});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup2";
            this.layoutControlGroup3.Size = new System.Drawing.Size(840, 79);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.comboBox2;
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem26.Name = "layoutControlItem15";
            this.layoutControlItem26.Size = new System.Drawing.Size(75, 25);
            this.layoutControlItem26.Text = "Chọn";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(45, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 25);
            this.emptySpaceItem3.Name = "emptySpaceItem2";
            this.emptySpaceItem3.Size = new System.Drawing.Size(180, 10);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.dateTimePicker6;
            this.layoutControlItem27.Location = new System.Drawing.Point(75, 0);
            this.layoutControlItem27.Name = "layoutControlItem16";
            this.layoutControlItem27.Size = new System.Drawing.Size(105, 25);
            this.layoutControlItem27.Text = "Từ";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(45, 13);
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.dateTimePicker5;
            this.layoutControlItem28.Location = new System.Drawing.Point(180, 0);
            this.layoutControlItem28.Name = "layoutControlItem17";
            this.layoutControlItem28.Size = new System.Drawing.Size(112, 35);
            this.layoutControlItem28.Text = "Đến";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(45, 13);
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.simpleButton14;
            this.layoutControlItem29.Location = new System.Drawing.Point(292, 0);
            this.layoutControlItem29.Name = "layoutControlItem18";
            this.layoutControlItem29.Size = new System.Drawing.Size(73, 35);
            this.layoutControlItem29.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem29.TextVisible = false;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.simpleButton13;
            this.layoutControlItem30.Location = new System.Drawing.Point(365, 0);
            this.layoutControlItem30.Name = "layoutControlItem19";
            this.layoutControlItem30.Size = new System.Drawing.Size(78, 35);
            this.layoutControlItem30.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem30.TextVisible = false;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.simpleButton12;
            this.layoutControlItem31.Location = new System.Drawing.Point(443, 0);
            this.layoutControlItem31.Name = "layoutControlItem20";
            this.layoutControlItem31.Size = new System.Drawing.Size(88, 59);
            this.layoutControlItem31.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem31.TextVisible = false;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.simpleButton11;
            this.layoutControlItem32.Location = new System.Drawing.Point(735, 0);
            this.layoutControlItem32.Name = "layoutControlItem21";
            this.layoutControlItem32.Size = new System.Drawing.Size(85, 59);
            this.layoutControlItem32.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem32.TextVisible = false;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.simpleButton10;
            this.layoutControlItem33.Location = new System.Drawing.Point(665, 0);
            this.layoutControlItem33.Name = "layoutControlItem22";
            this.layoutControlItem33.Size = new System.Drawing.Size(70, 59);
            this.layoutControlItem33.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem33.TextVisible = false;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.simpleButton9;
            this.layoutControlItem34.Location = new System.Drawing.Point(595, 0);
            this.layoutControlItem34.Name = "layoutControlItem23";
            this.layoutControlItem34.Size = new System.Drawing.Size(70, 59);
            this.layoutControlItem34.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem34.TextVisible = false;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.simpleButton8;
            this.layoutControlItem35.Location = new System.Drawing.Point(531, 0);
            this.layoutControlItem35.Name = "layoutControlItem24";
            this.layoutControlItem35.Size = new System.Drawing.Size(64, 59);
            this.layoutControlItem35.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem35.TextVisible = false;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.lookUpEdit8;
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 35);
            this.layoutControlItem36.Name = "layoutControlItem25";
            this.layoutControlItem36.Size = new System.Drawing.Size(443, 24);
            this.layoutControlItem36.Text = "Xem theo";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(45, 13);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.panel6);
            this.tabPage4.Location = new System.Drawing.Point(0, 0);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(200, 100);
            this.tabPage4.TabIndex = 0;
            // 
            // frmMuaHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1086, 523);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabPhieuMuaHang);
            this.Controls.Add(this.dockPanel1);
            this.Name = "frmMuaHang";
            this.Text = "Nhập hàng hóa";
            this.Load += new System.EventHandler(this.frmMuaHang_Load);
            this.tabPhieuMuaHang.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).EndInit();
            this.layoutControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lueChietKhau.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueVAT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachPhieuMuaHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewacb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lueHinhThucThanhToan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTenNhanVien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhieu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKhoNhap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueMaNhaCungCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTenNhaCungCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            this.panel2.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachHHTheoChungTu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.griđánhachhangHoaTheoChungTu)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lueXemTheo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachTheoHangHoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHHTheoCT)).EndInit();
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lueXemDanhSachHangHoaTheo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnXemPhieuMuaHang)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabPhieuMuaHang;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraNavBar.NavBarControl btnXemPhieuMuaHang;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup1;
        private DevExpress.XtraNavBar.NavBarItem navBarItem1;
        private DevExpress.XtraNavBar.NavBarControl navBarControl2;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup2;
        private DevExpress.XtraNavBar.NavBarItem btnTheoChungTu;
        private DevExpress.XtraNavBar.NavBarItem btnTheoHangHoa;
        private DevExpress.XtraNavBar.NavBarControl navBarControl1;
        private DevExpress.XtraNavBar.NavBarGroup tabThemDanhMuc;
        private DevExpress.XtraNavBar.NavBarItem LinkHangHoa;
        private DevExpress.XtraNavBar.NavBarItem LinkKhachHang;
        private DevExpress.XtraNavBar.NavBarItem LinkKhoHang;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbTenHienThi;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.LookUpEdit lueHinhThucThanhToan;
        private System.Windows.Forms.TextBox txtGhiChu;
        private System.Windows.Forms.TextBox txtSoPhieuVietTay;
        private DevExpress.XtraEditors.LookUpEdit lueTenNhanVien;
        private System.Windows.Forms.TextBox txtSoHoaDonVAT;
        private DevExpress.XtraEditors.TextEdit txtPhieu;
        private System.Windows.Forms.DateTimePicker dtpNgayThangXuat;
        private System.Windows.Forms.TextBox txtSdt;
        private System.Windows.Forms.TextBox txtDiaChi;
        private DevExpress.XtraEditors.LookUpEdit lueKhoNhap;
        private DevExpress.XtraEditors.LookUpEdit lueMaNhaCungCap;
        private DevExpress.XtraEditors.LookUpEdit lueTenNhaCungCap;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.SimpleButton btnDongPhieuNhapHang;
        private DevExpress.XtraEditors.SimpleButton btnNapLaiPhieuHang;
        private DevExpress.XtraEditors.SimpleButton btnInPhieuHang;
        private DevExpress.XtraEditors.SimpleButton btnLuuVaThem;
        private DevExpress.XtraEditors.SimpleButton btntaoMoiPhieuNhapHang;
        private System.Windows.Forms.DateTimePicker dtpNgayThanhToan;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraGrid.GridControl grcDanhSachPhieuMuaHang;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewacb;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.SimpleButton btnXoaChungTu;
        private DevExpress.XtraEditors.SimpleButton btnInChungTu;
        private DevExpress.XtraEditors.SimpleButton btnXuatChungTu;
        private DevExpress.XtraEditors.SimpleButton btnDongChungTu;
        private DevExpress.XtraEditors.SimpleButton btnSuaChungTu;
        private DevExpress.XtraEditors.SimpleButton btnTaoMoiChungTu;
        private DevExpress.XtraEditors.SimpleButton btnXemchungTu;
        private System.Windows.Forms.DateTimePicker dtpNgayKetThuc;
        private System.Windows.Forms.DateTimePicker dtpNgayBatDau;
        private System.Windows.Forms.ComboBox cmbChonLoai;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraEditors.LookUpEdit lueXemTheo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private System.Windows.Forms.Panel panel9;
        private DevExpress.XtraGrid.GridControl grcDanhSachTheoHangHoa;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewHHTheoCT;
        private System.Windows.Forms.Panel panel8;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraEditors.SimpleButton btnSuaHangHoa;
        private DevExpress.XtraEditors.SimpleButton btnTaoMoihangHoa;
        private System.Windows.Forms.DateTimePicker dtpXemhangHoaKetThuc;
        private System.Windows.Forms.DateTimePicker dtpXemHangHoaTu;
        private System.Windows.Forms.ComboBox cmbLuachonXemTheoHangHoa;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private System.Windows.Forms.Panel panel6;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Windows.Forms.Panel panel7;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit8;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private DevExpress.XtraEditors.SimpleButton simpleButton11;
        private DevExpress.XtraEditors.SimpleButton simpleButton12;
        private DevExpress.XtraEditors.SimpleButton simpleButton13;
        private DevExpress.XtraEditors.SimpleButton simpleButton14;
        private System.Windows.Forms.DateTimePicker dateTimePicker5;
        private System.Windows.Forms.DateTimePicker dateTimePicker6;
        private System.Windows.Forms.ComboBox comboBox2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private System.Windows.Forms.TabPage tabPage4;
        private DevExpress.XtraEditors.SimpleButton btnDonghangHoa;
        private DevExpress.XtraEditors.SimpleButton btnXuatHangHoa;
        private DevExpress.XtraEditors.SimpleButton btnInHangHoa;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraEditors.LookUpEdit lueXemDanhSachHangHoaTheo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private System.Windows.Forms.ComboBox cmbHinhThucThanhToan;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit3;
        private DevExpress.XtraEditors.SimpleButton btnThemMoiPhieuNhapHang;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraEditors.SimpleButton btnNapLaiDanhSach;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private System.Windows.Forms.Panel panel10;
        private DevExpress.XtraLayout.LayoutControl layoutControl5;
        private System.Windows.Forms.TextBox txtThanhToan;
        private DevExpress.XtraEditors.CalcEdit lueChietKhau;
        private DevExpress.XtraEditors.CalcEdit lueVAT;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.SimpleButton btnXemTheoHangHoa;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraGrid.GridControl grcDanhSachHHTheoChungTu;
        private DevExpress.XtraGrid.Views.Grid.GridView griđánhachhangHoaTheoChungTu;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private DevExpress.XtraEditors.SimpleButton btnXoaChungTuTheoCT;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
    }
}