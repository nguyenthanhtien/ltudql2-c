﻿using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmMuaHang : Form
    {
        DACK_ware5Entities1 db = new DACK_ware5Entities1();

        int number;
        public frmMuaHang()
        {
            InitializeComponent();
            Random rnd = new Random();
            number = rnd.Next(10, 10000);
            RandomCode();
            LoadDanhSachXemTheohangHoa();
            LoadDanhSachHHTheoChungTu();
        }

        private void RandomCode()
        {
            txtPhieu.Text = "NH" + number;
        }

        private void btnTheoChungTu_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tabPhieuMuaHang.SelectTab(1);
            tabPhieuMuaHang.Visible = true;
            tabThemDanhMuc.Visible = false;
            lbTenHienThi.Text = "bảng kê tổng hợp";
        }

        private void navBarItem1_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tabPhieuMuaHang.SelectTab(0);
            tabPhieuMuaHang.Visible = true;
            tabThemDanhMuc.Visible = true;
            lbTenHienThi.Text = "Phiếu nhập hàng";
        }

        private void btnTheoHangHoa_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tabPhieuMuaHang.SelectTab(2);
            tabPhieuMuaHang.Visible = true;
            tabThemDanhMuc.Visible = false;
            lbTenHienThi.Text = "bảng kê chi tiết";
        }

        private void LinkKhoHang_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            var form = new frmThemDanhSachKho();
            form.Show();
        }

        private void LinkKhachHang_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            var form = new frmUpdateKH();
            form.Show();
        }

        private void LinkHangHoa_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            var form = new frmThemDanhSachHangHoa();
            form.ShowDialog();
        }

        private void simpleButton17_Click(object sender, EventArgs e)
        {
            tabPhieuMuaHang.SelectTab(0);
            tabPhieuMuaHang.Visible = true;
            tabThemDanhMuc.Visible = true;
            lbTenHienThi.Text = "Phiếu nhập hàng";
        }

        private void frmMuaHang_Load(object sender, EventArgs e)
        {
            lueTenNhaCungCap.Properties.DataSource = db.NhaCungCaps.ToList();
            lueTenNhaCungCap.Properties.DisplayMember = "TenNCC";
            lueTenNhaCungCap.Properties.ValueMember = "id_NCC";

            lueMaNhaCungCap.Properties.DataSource = db.NhaCungCaps.ToList();
            lueMaNhaCungCap.Properties.DisplayMember = "id_NCC";
            lueMaNhaCungCap.Properties.ValueMember = "id_NCC";

            lueKhoNhap.Properties.DataSource = db.Khoes.ToList();
            lueKhoNhap.Properties.DisplayMember = "TenKho";
            lueKhoNhap.Properties.ValueMember = "Id_Kho";

            lueTenNhanVien.Properties.DataSource = db.NhanViens.ToList();
            lueTenNhanVien.Properties.DisplayMember = "TenNV";
            lueTenNhanVien.Properties.ValueMember = "id_NhanVien";



            LoadGridControl();

            lueChietKhau.EditValue = 2;

            lueVAT.EditValue = 0;
        }

        private void LoadGridControl()
        {
            //  grcDanhSachPhieuMuaHang.DataSource = db.NhanViens.ToList();

            // RepositoryItemLookUpEdit lookUpRI = new RepositoryItemLookUpEdit();
            // lookUpRI.DataSource = (from n in db.NhanViens select new { n.id_NhanVien, n.TenNV, n.SDT }).ToList(); /* db.NhanViens.ToList();*/
            // lookUpRI.DisplayMember = "TenNV";
            // lookUpRI.ValueMember = "TenNV";
            // grcDanhSachPhieuMuaHang.RepositoryItems.Add(lookUpRI);
            //(grcDanhSachPhieuMuaHang.MainView as ColumnView).Columns["TenNV"].ColumnEdit = lookUpRI;
            // lookUpRI.ShowHeader = false;


            grcDanhSachPhieuMuaHang.DataSource = (from n in db.NhapHangTamThois from m in db.DonViTinhs where m.Id_DV == n.Id_DonVi
                                                  select new { m.TenDonVi, n.ThanhTien, n.MaHang, n.SoLuong, n.TenHang, n.DonGia }).ToList();
        }

        private void btntaoMoiPhieuNhapHang_Click(object sender, EventArgs e)
        {
            var nh = from o in db.NhapHangTamThois select o;
            foreach (var item in nh)
            {
                db.NhapHangTamThois.Remove(item);
            }
            db.SaveChanges();
            LoadGridControl();
        }

        private void btnDongPhieuNhapHang_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void grcDanhSachPhieuMuaHang_Click(object sender, EventArgs e)
        {

        }

        private void btnThemMoiPhieuNhapHang_Click(object sender, EventArgs e)
        {
            var frm = new frmNhapHangTamThoi();
            frm.Show();
        }

        private void btnNapLaiDanhSach_Click(object sender, EventArgs e)
        {
            LoadGridControl();
            int tongtien = 0;
            int thanhtoan = 0;

            for (int i = 0; i < gridViewacb.RowCount; i++)
            {
                var thanhtien = gridViewacb.GetRowCellValue(i, "ThanhTien");
                tongtien = tongtien + Convert.ToInt32(thanhtien);

            }
            thanhtoan = tongtien - (tongtien * ((Convert.ToInt32(lueChietKhau.EditValue)) / 100));
            txtThanhToan.Text = thanhtoan.ToString();
        }
        int mactnh;
        String idctnh = "CTNH";
        private String RanDomMaCTNH()
        {
            Random rd = new Random();
            mactnh = rd.Next(10, 200000);
            return idctnh + mactnh;
        }

        private void btnLuuVaThem_Click(object sender, EventArgs e)
        {
            RanDomMaCTNH();


            NhapHang nh = new NhapHang();
            ChiTietNhapHang ct = new ChiTietNhapHang();
            nh.id_ChungTuNhap = txtPhieu.Text;
            nh.id_Kho = int.Parse(lueKhoNhap.EditValue.ToString());
            nh.id_NhaCungCap = lueMaNhaCungCap.EditValue.ToString();
            nh.NgayNhap = DateTime.Parse(dtpNgayThangXuat.Text);
            nh.SoPhieuVietTay = int.Parse(txtSoPhieuVietTay.Text);
            nh.SoVat = Convert.ToInt32(txtSoHoaDonVAT.Text);
            nh.SoTien = Convert.ToInt32(txtThanhToan.Text);

            nh.ChietKhau = Convert.ToInt32(lueChietKhau.EditValue);
            nh.Vat = Convert.ToInt32(lueVAT.EditValue);
            nh.SoTien = int.Parse(txtThanhToan.Text) + ((int.Parse(txtThanhToan.Text)) * (Convert.ToInt32(lueChietKhau.EditValue) / 100));
            nh.ThanhToan = int.Parse(txtThanhToan.Text);
            nh.DKThanhToan = cmbHinhThucThanhToan.Text;
            db.NhapHangs.Add(nh);
            db.SaveChanges();

            for (int i = 0; i < gridViewacb.RowCount; i++)
            {
                var tenhang = gridViewacb.GetRowCellValue(i, "TenHang");
                var mahang = gridViewacb.GetRowCellValue(i, "MaHang");
                var donvi = gridViewacb.GetRowCellValue(i, "TenDonVi");
                var soluong = gridViewacb.GetRowCellValue(i, "SoLuong");
                var dongia = gridViewacb.GetRowCellValue(i, "DonGia");
                var thanhtien = gridViewacb.GetRowCellValue(i, "ThanhTien");
                var iddonvi = from n in db.ChiTietNhapHangs
                              from m in db.NhapHangTamThois
                              where n.id_DonVi == m.Id_DonVi
                              select new { n.id_DonVi };

                ct.id_CTNhapHang = RanDomMaCTNH();
                ct.id_HangHoa = Convert.ToString(mahang);
                ct.id_ChungTuNhap = txtPhieu.Text;
                ct.SoLuong = Convert.ToInt32(soluong);
                ct.DonGia = Convert.ToInt32(dongia);
                ct.GhiChu = txtGhiChu.Text;

                //tăng số lượng hàng hóa trong kho lên bằng với số lượng hàng hóa mua thêm
                var tanghang = db.HangHoas.FirstOrDefault(n => n.id_hanghoa.Equals(ct.id_HangHoa));
                //hàm kiểm tra mã hàng hóa mua so sánh với mã hàng trong bảng hàng hóa
                // trùng mã với nhau thì lấy số lượng hàng trong kho công thêm vào.
                tanghang.SoLuong = tanghang.SoLuong + ct.SoLuong;

                foreach (var item in iddonvi)
                {
                    ct.id_DonVi = item.id_DonVi;
                }
                ct.ThanhTien = Convert.ToInt32(thanhtien);
                db.ChiTietNhapHangs.Add(ct);

            }
            db.SaveChanges();
            MessageBox.Show("Mua hàng thành công.");
        }


        private void LoadDanhSachXemTheohangHoa()
        {
            var query = (from n in db.NhapHangs
                         from m in db.ChiTietNhapHangs
                         from p in db.DonViTinhs
                         from q in db.Khoes
                         from t in db.HangHoas
                         where n.id_ChungTuNhap == m.id_ChungTuNhap && m.id_DonVi == p.Id_DV && q.Id_Kho == n.id_Kho && m.id_HangHoa == t.id_hanghoa
                         select new { n.id_ChungTuNhap, n.NgayNhap, n.SoTien, n.ThanhToan,
                             m.SoLuong, m.DonGia, m.id_HangHoa, p.TenDonVi, q.TenKho, t.Tenhang }).ToList();
            grcDanhSachTheoHangHoa.DataSource = query;
        }
        private void btnXemTheoHangHoa_Click(object sender, EventArgs e)
        {

        }

        private void btnDonghangHoa_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadDanhSachHHTheoChungTu()
        {
            var query = (from n in db.NhapHangs
                         from m in db.ChiTietNhapHangs
                         from p in db.DonViTinhs
                         from q in db.Khoes
                         from t in db.HangHoas
                         where n.id_ChungTuNhap == m.id_ChungTuNhap && m.id_DonVi == p.Id_DV && q.Id_Kho == n.id_Kho && m.id_HangHoa == t.id_hanghoa
                         select new
                         {
                             n.id_ChungTuNhap,
                             n.NgayNhap,
                             n.SoTien,
                             n.ThanhToan,
                             m.SoLuong,
                             m.DonGia,
                             m.id_HangHoa,
                             p.TenDonVi,
                             q.TenKho,
                             t.Tenhang
                         }).ToList();
            grcDanhSachHHTheoChungTu.DataSource = query;
        }

        private void btnInPhieuHang_Click(object sender, EventArgs e)
        {
            
        }

        private void btnSuaChungTu_Click(object sender, EventArgs e)
        {
            tabPhieuMuaHang.SelectTab(0);
            tabPhieuMuaHang.Visible = true;
            tabThemDanhMuc.Visible = true;
            lbTenHienThi.Text = "Phiếu nhập hàng";
        }

        private void btnXuatHangHoa_Click(object sender, EventArgs e)
        {
            saveFileDialog1.InitialDirectory = "C:";
            saveFileDialog1.Title = "Save as Excle File";
            saveFileDialog1.FileName = "";
            saveFileDialog1.Filter = "Excle File(2003)|*.xls|Excle File(2007)|*.xlsx";
            if (saveFileDialog1.ShowDialog() != DialogResult.Cancel)
            {
                grcDanhSachTheoHangHoa.ExportToXls(saveFileDialog1.FileName);
            }
        }

        private void btnTaoMoihangHoa_Click(object sender, EventArgs e)
        {
            tabPhieuMuaHang.SelectTab(0);
            tabPhieuMuaHang.Visible = true;
            tabThemDanhMuc.Visible = true;
            lbTenHienThi.Text = "Phiếu nhập hàng";
        }

        private void btnXoaChungTuTheoCT_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.Cancel)
            {       
                string mactnh = gridViewHHTheoCT.GetRowCellValue(gridViewHHTheoCT.FocusedRowHandle, "id_ChungTuNhap").ToString();     
               ChiTietNhapHang ctnh = db.ChiTietNhapHangs.FirstOrDefault(n => n.id_ChungTuNhap == mactnh);
                db.ChiTietNhapHangs.Remove(ctnh);       
                db.SaveChanges();        
                MessageBox.Show("Đã xóa", "Thông báo");
                LoadDanhSachXemTheohangHoa();        
            }
        }

        private void btnXuatChungTu_Click(object sender, EventArgs e)
        {
            saveFileDialog1.InitialDirectory = "C:";
            saveFileDialog1.Title = "Save as Excle File";
            saveFileDialog1.FileName = "";
            saveFileDialog1.Filter = "Excle File(2003)|*.xls|Excle File(2007)|*.xlsx";
            if (saveFileDialog1.ShowDialog() != DialogResult.Cancel)
            {
                griđánhachhangHoaTheoChungTu.ExportToXls(saveFileDialog1.FileName);
            }
        }

        private void btnXoaChungTu_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.Cancel)
            {
                string mactnh = griđánhachhangHoaTheoChungTu.GetRowCellValue(griđánhachhangHoaTheoChungTu.FocusedRowHandle, "id_ChungTuNhap").ToString();
                ChiTietNhapHang ctnh = db.ChiTietNhapHangs.FirstOrDefault(n => n.id_ChungTuNhap == mactnh);
                db.ChiTietNhapHangs.Remove(ctnh);
                db.SaveChanges();
                MessageBox.Show("Đã xóa", "Thông báo");
                LoadDanhSachXemTheohangHoa();
            }
        }

        private void btnNapLaiPhieuHang_Click(object sender, EventArgs e)
        {

        }
    }
}
