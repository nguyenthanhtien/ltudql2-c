﻿namespace QuanLyBanHang
{
    partial class frmBanHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.resourcesTree1 = new DevExpress.XtraScheduler.UI.ResourcesTree();
            ((System.ComponentModel.ISupportInitialize)(this.resourcesTree1)).BeginInit();
            this.SuspendLayout();
            // 
            // resourcesTree1
            // 
            this.resourcesTree1.Location = new System.Drawing.Point(63, 60);
            this.resourcesTree1.Name = "resourcesTree1";
            this.resourcesTree1.OptionsBehavior.Editable = false;
            this.resourcesTree1.RefreshDataOnSchedulerChanges = false;
            this.resourcesTree1.Size = new System.Drawing.Size(654, 282);
            this.resourcesTree1.TabIndex = 0;
            // 
            // frmBanHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(766, 467);
            this.Controls.Add(this.resourcesTree1);
            this.Name = "frmBanHang";
            this.Text = "Bán hàng";
            ((System.ComponentModel.ISupportInitialize)(this.resourcesTree1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraScheduler.UI.ResourcesTree resourcesTree1;
    }
}