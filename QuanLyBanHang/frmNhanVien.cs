﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace QuanLyBanHang
{
    public partial class frmNhanVien : Form
    {
        DACK_ware5Entities1 dbi = new DACK_ware5Entities1();
        public frmNhanVien()
        {
            InitializeComponent();
            LoadDuLieu();
          
        }
        private void LoadDuLieu()
        {
            gc_NhanVien.DataSource = (from n in dbi.NhanViens from m in dbi.BoPhans
                                      where n.id_bophan == m.id_BoPhan
                                      select new {m.TenBoPhan, n.SDT, n.TenNV, n.trangthai, n.email, n.DiaChi,n.id_NhanVien }).ToList();
        }

        private void btn_Dong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Them_Click(object sender, EventArgs e)
        {
            var form = new frmThemNhanVien();
            form.Show();
        }

        private void btn_Xoa_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.Cancel)
            {
                string manhanvien = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "id_NhanVien").ToString();
                int id = int.Parse(manhanvien);
                NhanVien nhanvien = dbi.NhanViens.Single(n => n.id_NhanVien == id);
                dbi.NhanViens.Remove(nhanvien);
                dbi.SaveChanges();
                LoadDuLieu();
            }
            else
            {
                MessageBox.Show("Không xoá");
            }
        }

        private void btn_Sua_Click(object sender, EventArgs e)
        {
            string id = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "id_NhanVien").ToString();
            string ten = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TenNV").ToString();
            string diachi = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "DiaChi").ToString();
            string SDT = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "SDT").ToString();
            string email = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "email").ToString();
            string trangthai = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "trangthai").ToString();
            var form = new frmSuaNhanVien (id, ten, diachi, SDT, email, trangthai);
            form.Show();
           // MessageBox.Show(email);
        }

        private void btn_CapNhat_Click(object sender, EventArgs e)
        {
            LoadDuLieu();
        }

        private void frmNhanVien_Load(object sender, EventArgs e)
        {

        }
    }
    }


