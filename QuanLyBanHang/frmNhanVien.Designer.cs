﻿using System;

namespace QuanLyBanHang
{
    partial class frmNhanVien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Them = new System.Windows.Forms.Button();
            this.btn_Xoa = new System.Windows.Forms.Button();
            this.btn_Sua = new System.Windows.Forms.Button();
            this.btn_CapNhat = new System.Windows.Forms.Button();
            this.btn_Dong = new System.Windows.Forms.Button();
            this.gc_NhanVien = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid_NhanVien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_TenNV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_DiaChi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_SDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_email = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_trangthai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.gc_NhanVien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_Them
            // 
            this.btn_Them.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_Them.Location = new System.Drawing.Point(12, 12);
            this.btn_Them.Name = "btn_Them";
            this.btn_Them.Size = new System.Drawing.Size(75, 23);
            this.btn_Them.TabIndex = 0;
            this.btn_Them.Text = "Thêm";
            this.btn_Them.UseVisualStyleBackColor = false;
            this.btn_Them.Click += new System.EventHandler(this.btn_Them_Click);
            // 
            // btn_Xoa
            // 
            this.btn_Xoa.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_Xoa.Location = new System.Drawing.Point(105, 12);
            this.btn_Xoa.Name = "btn_Xoa";
            this.btn_Xoa.Size = new System.Drawing.Size(75, 23);
            this.btn_Xoa.TabIndex = 1;
            this.btn_Xoa.Text = "Xóa";
            this.btn_Xoa.UseVisualStyleBackColor = false;
            this.btn_Xoa.Click += new System.EventHandler(this.btn_Xoa_Click);
            // 
            // btn_Sua
            // 
            this.btn_Sua.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_Sua.Location = new System.Drawing.Point(200, 12);
            this.btn_Sua.Name = "btn_Sua";
            this.btn_Sua.Size = new System.Drawing.Size(75, 23);
            this.btn_Sua.TabIndex = 2;
            this.btn_Sua.Text = "Sửa";
            this.btn_Sua.UseVisualStyleBackColor = false;
            this.btn_Sua.Click += new System.EventHandler(this.btn_Sua_Click);
            // 
            // btn_CapNhat
            // 
            this.btn_CapNhat.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_CapNhat.Location = new System.Drawing.Point(294, 12);
            this.btn_CapNhat.Name = "btn_CapNhat";
            this.btn_CapNhat.Size = new System.Drawing.Size(75, 23);
            this.btn_CapNhat.TabIndex = 3;
            this.btn_CapNhat.Text = "Cập nhật";
            this.btn_CapNhat.UseVisualStyleBackColor = false;
            this.btn_CapNhat.Click += new System.EventHandler(this.btn_CapNhat_Click);
            // 
            // btn_Dong
            // 
            this.btn_Dong.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_Dong.Location = new System.Drawing.Point(389, 12);
            this.btn_Dong.Name = "btn_Dong";
            this.btn_Dong.Size = new System.Drawing.Size(75, 23);
            this.btn_Dong.TabIndex = 4;
            this.btn_Dong.Text = "Đóng";
            this.btn_Dong.UseVisualStyleBackColor = false;
            this.btn_Dong.Click += new System.EventHandler(this.btn_Dong_Click);
            // 
            // gc_NhanVien
            // 
            this.gc_NhanVien.Location = new System.Drawing.Point(12, 41);
            this.gc_NhanVien.MainView = this.gridView1;
            this.gc_NhanVien.Name = "gc_NhanVien";
            this.gc_NhanVien.Size = new System.Drawing.Size(891, 398);
            this.gc_NhanVien.TabIndex = 5;
            this.gc_NhanVien.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid_NhanVien,
            this.col_TenNV,
            this.col_DiaChi,
            this.col_SDT,
            this.col_email,
            this.col_trangthai,
            this.gridColumn1});
            this.gridView1.GridControl = this.gc_NhanVien;
            this.gridView1.Name = "gridView1";
            // 
            // colid_NhanVien
            // 
            this.colid_NhanVien.Caption = "Bộ Phận";
            this.colid_NhanVien.FieldName = "TenBoPhan";
            this.colid_NhanVien.Name = "colid_NhanVien";
            this.colid_NhanVien.Visible = true;
            this.colid_NhanVien.VisibleIndex = 0;
            // 
            // col_TenNV
            // 
            this.col_TenNV.Caption = "Tên Nhân Viên";
            this.col_TenNV.FieldName = "TenNV";
            this.col_TenNV.Name = "col_TenNV";
            this.col_TenNV.Visible = true;
            this.col_TenNV.VisibleIndex = 1;
            // 
            // col_DiaChi
            // 
            this.col_DiaChi.Caption = "Địa Chỉ";
            this.col_DiaChi.FieldName = "DiaChi";
            this.col_DiaChi.Name = "col_DiaChi";
            this.col_DiaChi.Visible = true;
            this.col_DiaChi.VisibleIndex = 2;
            // 
            // col_SDT
            // 
            this.col_SDT.Caption = "Số Điện Thoại";
            this.col_SDT.FieldName = "SDT";
            this.col_SDT.Name = "col_SDT";
            this.col_SDT.Visible = true;
            this.col_SDT.VisibleIndex = 3;
            // 
            // col_email
            // 
            this.col_email.Caption = "Email";
            this.col_email.FieldName = "email";
            this.col_email.Name = "col_email";
            this.col_email.Visible = true;
            this.col_email.VisibleIndex = 4;
            // 
            // col_trangthai
            // 
            this.col_trangthai.Caption = "Trạng Thái";
            this.col_trangthai.FieldName = "trangthai";
            this.col_trangthai.Name = "col_trangthai";
            this.col_trangthai.Visible = true;
            this.col_trangthai.VisibleIndex = 5;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Mã nhân viên";
            this.gridColumn1.FieldName = "id_NhanVien";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 6;
            // 
            // frmNhanVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 439);
            this.Controls.Add(this.gc_NhanVien);
            this.Controls.Add(this.btn_Dong);
            this.Controls.Add(this.btn_CapNhat);
            this.Controls.Add(this.btn_Sua);
            this.Controls.Add(this.btn_Xoa);
            this.Controls.Add(this.btn_Them);
            this.Name = "frmNhanVien";
            this.Text = "Nhân viên";
            this.Load += new System.EventHandler(this.frmNhanVien_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gc_NhanVien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        private void gc_NhanVien_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.Button btn_Them;
        private System.Windows.Forms.Button btn_Xoa;
        private System.Windows.Forms.Button btn_Sua;
        private System.Windows.Forms.Button btn_CapNhat;
        private System.Windows.Forms.Button btn_Dong;
        private DevExpress.XtraGrid.GridControl gc_NhanVien;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colid_NhanVien;
        private DevExpress.XtraGrid.Columns.GridColumn col_TenNV;
        private DevExpress.XtraGrid.Columns.GridColumn col_DiaChi;
        private DevExpress.XtraGrid.Columns.GridColumn col_SDT;
        private DevExpress.XtraGrid.Columns.GridColumn col_email;
        private DevExpress.XtraGrid.Columns.GridColumn col_trangthai;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
    }
}