﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmThemNhanVien : Form
    {
        DACK_ware5Entities1 db = new DACK_ware5Entities1();
        NhanVien bp = new NhanVien();

        public frmThemNhanVien()
        {
            InitializeComponent();
            LoadData();
        }

        private void LoadData()
        {
            lueBoPhan.Properties.DataSource = db.BoPhans.ToList();
            lueBoPhan.Properties.DisplayMember = "TenBoPhan";
            lueBoPhan.Properties.ValueMember = "id_BoPhan";

            lueQuanLy.Properties.DataSource = db.NhanViens.ToList();
            lueQuanLy.Properties.DisplayMember = "TenNV";
            lueQuanLy.Properties.ValueMember = "id_NhanVien";
        }

        private void btn_Dong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Luu_Click(object sender, EventArgs e)
        {

            bp.TenNV = txt_TenNhanVien.Text;
            bp.DiaChi = txt_DiaChi.Text;
            bp.SDT = txt_SDT.Text;
            bp.email = txt_Email.Text;
            bp.id_bophan = int.Parse(lueBoPhan.EditValue.ToString());
            if (cbQuanLy.Checked == true)
            {
                bp.trangthai = true;
            }
            else
            {
                bp.trangthai = false;
            }
            db.NhanViens.Add(bp);
            db.SaveChanges();
            MessageBox.Show("Thêm thành công !!");
            this.Close();
        }

        private void lueBoPhan_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void groupControl2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
