﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmNhatKyHeThong : Form
    {
        DACK_ware5Entities1 dbe = new DACK_ware5Entities1();
        public frmNhatKyHeThong()
        {
          
          
            InitializeComponent();
        }

        private void frmNhatKyHeThong_Load(object sender, EventArgs e)
        {
            gcNhatKyHeThong.DataSource = dbe.NhatKies.ToList();
        }
    }
}
