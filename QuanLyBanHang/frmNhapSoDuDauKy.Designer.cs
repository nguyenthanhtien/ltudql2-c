﻿namespace QuanLyBanHang
{
    partial class frmNhapSoDuDauKy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNhapSoDuDauKy));
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.btnXemTonKhoDauki = new System.Windows.Forms.Button();
            this.btnXembangKe = new System.Windows.Forms.Button();
            this.btnXemCongNoDauKi = new System.Windows.Forms.Button();
            this.btnThemDanhMuc = new System.Windows.Forms.Button();
            this.nvbTonKhoDauKi = new DevExpress.XtraNavBar.NavBarControl();
            this.nvTenHienThi = new DevExpress.XtraNavBar.NavBarGroup();
            this.nvTonKho = new DevExpress.XtraNavBar.NavBarItem();
            this.nvTheoKi = new DevExpress.XtraNavBar.NavBarItem();
            this.nvTheoHangHoa = new DevExpress.XtraNavBar.NavBarItem();
            this.nvKhachHang = new DevExpress.XtraNavBar.NavBarItem();
            this.nvNhaCungCap = new DevExpress.XtraNavBar.NavBarItem();
            this.nvHangHoa = new DevExpress.XtraNavBar.NavBarItem();
            this.nvKhachHangDanhMuc = new DevExpress.XtraNavBar.NavBarItem();
            this.nvNhaCungCapDanhMuc = new DevExpress.XtraNavBar.NavBarItem();
            this.nvFileExlseMau = new DevExpress.XtraNavBar.NavBarItem();
            this.tabNhapSoDuDauKi = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.grcDanhSachTonKhoDauKi = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.dtpNgayNhapTonKho = new System.Windows.Forms.DateTimePicker();
            this.txtGhiChuTonKho = new DevExpress.XtraEditors.TextEdit();
            this.txtPhieuTonKho = new System.Windows.Forms.TextBox();
            this.lueKhoTonKho = new DevExpress.XtraEditors.LookUpEdit();
            this.txtNguoiNhapKhoDauKi = new System.Windows.Forms.TextBox();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDongTonKho = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuuTonKho = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemMoiTonKho = new DevExpress.XtraEditors.SimpleButton();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.grcHHTheoKi = new DevExpress.XtraGrid.GridControl();
            this.gricHHTheoKi = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.btnXuatHHDauKi = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoaHHTheoKi = new DevExpress.XtraEditors.SimpleButton();
            this.btnSuaHHTheoKi = new DevExpress.XtraEditors.SimpleButton();
            this.btnTaoMoiTheoKi = new DevExpress.XtraEditors.SimpleButton();
            this.btnTimKiemTheoKi = new DevExpress.XtraEditors.SimpleButton();
            this.dtpNgayKetThuc = new System.Windows.Forms.DateTimePicker();
            this.dtpNgayBatDau = new System.Windows.Forms.DateTimePicker();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.grcDanhSachTheoHangHoa = new DevExpress.XtraGrid.GridControl();
            this.gricDanhSachHangHoaTheoDauKi = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel6 = new System.Windows.Forms.Panel();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.dtpNgayBDTheoHH = new System.Windows.Forms.DateTimePicker();
            this.dtpNgayKTTheoHH = new System.Windows.Forms.DateTimePicker();
            this.btnXemHHTheoNgay = new DevExpress.XtraEditors.SimpleButton();
            this.btnSuaHHTheoDauKi = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoaHangHoaTheoCt = new DevExpress.XtraEditors.SimpleButton();
            this.btnTaoMoiHHdauKi = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton13 = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuatDSHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.gricDanhSachKhCongNo = new DevExpress.XtraGrid.GridControl();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel8 = new System.Windows.Forms.Panel();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.btnDongKHSoDuDauKi = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton20 = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuuKhCongNo = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton18 = new DevExpress.XtraEditors.SimpleButton();
            this.lueTimKHheoKhuVuc = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.panel11 = new System.Windows.Forms.Panel();
            this.grcDanhSachNhaCungCapDauKi = new DevExpress.XtraGrid.GridControl();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel10 = new System.Windows.Forms.Panel();
            this.layoutControl5 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton25 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton24 = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuuDanhSachNCCDauKi = new DevExpress.XtraEditors.SimpleButton();
            this.btnXemNCCTheoKHuVuc = new DevExpress.XtraEditors.SimpleButton();
            this.lueTimKhuVucTheoNCC = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnThemMoiHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnnapLai = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nvbTonKhoDauKi)).BeginInit();
            this.tabNhapSoDuDauKi.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachTonKhoDauKi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChuTonKho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKhoTonKho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.panel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcHHTheoKi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gricHHTheoKi)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachTheoHangHoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gricDanhSachHangHoaTheoDauKi)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gricDanhSachKhCongNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueTimKHheoKhuVuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachNhaCungCapDauKi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).BeginInit();
            this.layoutControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueTimKhuVucTheoNCC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane",
            "DevExpress.XtraBars.TabFormControl"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("5a84da5f-703d-44ed-95db-1bf0c6b6ce8c");
            this.dockPanel1.Location = new System.Drawing.Point(0, 0);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.OriginalSize = new System.Drawing.Size(166, 200);
            this.dockPanel1.Size = new System.Drawing.Size(166, 568);
            this.dockPanel1.Text = "Chức năng";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.btnXemTonKhoDauki);
            this.dockPanel1_Container.Controls.Add(this.btnXembangKe);
            this.dockPanel1_Container.Controls.Add(this.btnXemCongNoDauKi);
            this.dockPanel1_Container.Controls.Add(this.btnThemDanhMuc);
            this.dockPanel1_Container.Controls.Add(this.nvbTonKhoDauKi);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(157, 541);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // btnXemTonKhoDauki
            // 
            this.btnXemTonKhoDauki.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXemTonKhoDauki.Location = new System.Drawing.Point(0, 449);
            this.btnXemTonKhoDauki.Name = "btnXemTonKhoDauki";
            this.btnXemTonKhoDauki.Size = new System.Drawing.Size(157, 23);
            this.btnXemTonKhoDauki.TabIndex = 4;
            this.btnXemTonKhoDauki.Text = "Tồn kho đầu kì";
            this.btnXemTonKhoDauki.UseVisualStyleBackColor = true;
            this.btnXemTonKhoDauki.Click += new System.EventHandler(this.btnXemTonKhoDauki_Click);
            // 
            // btnXembangKe
            // 
            this.btnXembangKe.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXembangKe.Location = new System.Drawing.Point(0, 472);
            this.btnXembangKe.Name = "btnXembangKe";
            this.btnXembangKe.Size = new System.Drawing.Size(157, 23);
            this.btnXembangKe.TabIndex = 3;
            this.btnXembangKe.Text = "Bảng kê";
            this.btnXembangKe.UseVisualStyleBackColor = true;
            this.btnXembangKe.Click += new System.EventHandler(this.btnXembangKe_Click);
            // 
            // btnXemCongNoDauKi
            // 
            this.btnXemCongNoDauKi.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXemCongNoDauKi.Location = new System.Drawing.Point(0, 495);
            this.btnXemCongNoDauKi.Name = "btnXemCongNoDauKi";
            this.btnXemCongNoDauKi.Size = new System.Drawing.Size(157, 23);
            this.btnXemCongNoDauKi.TabIndex = 2;
            this.btnXemCongNoDauKi.Text = "Công nợ đầu kì";
            this.btnXemCongNoDauKi.UseVisualStyleBackColor = true;
            this.btnXemCongNoDauKi.Click += new System.EventHandler(this.btnXemCongNoDauKi_Click);
            // 
            // btnThemDanhMuc
            // 
            this.btnThemDanhMuc.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnThemDanhMuc.Location = new System.Drawing.Point(0, 518);
            this.btnThemDanhMuc.Name = "btnThemDanhMuc";
            this.btnThemDanhMuc.Size = new System.Drawing.Size(157, 23);
            this.btnThemDanhMuc.TabIndex = 1;
            this.btnThemDanhMuc.Text = "Thêm danh mục";
            this.btnThemDanhMuc.UseVisualStyleBackColor = true;
            this.btnThemDanhMuc.Click += new System.EventHandler(this.btnThemDanhMuc_Click);
            // 
            // nvbTonKhoDauKi
            // 
            this.nvbTonKhoDauKi.ActiveGroup = this.nvTenHienThi;
            this.nvbTonKhoDauKi.Dock = System.Windows.Forms.DockStyle.Top;
            this.nvbTonKhoDauKi.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.nvTenHienThi});
            this.nvbTonKhoDauKi.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.nvTonKho,
            this.nvTheoKi,
            this.nvTheoHangHoa,
            this.nvKhachHang,
            this.nvNhaCungCap,
            this.nvHangHoa,
            this.nvKhachHangDanhMuc,
            this.nvNhaCungCapDanhMuc,
            this.nvFileExlseMau});
            this.nvbTonKhoDauKi.Location = new System.Drawing.Point(0, 0);
            this.nvbTonKhoDauKi.Name = "nvbTonKhoDauKi";
            this.nvbTonKhoDauKi.OptionsNavPane.ExpandedWidth = 157;
            this.nvbTonKhoDauKi.Size = new System.Drawing.Size(157, 391);
            this.nvbTonKhoDauKi.TabIndex = 0;
            this.nvbTonKhoDauKi.Text = "Tồn kho đầu kì";
            // 
            // nvTenHienThi
            // 
            this.nvTenHienThi.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.nvTenHienThi.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.nvTenHienThi.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.nvTenHienThi.Appearance.Options.UseBackColor = true;
            this.nvTenHienThi.Appearance.Options.UseBorderColor = true;
            this.nvTenHienThi.Caption = "Tồn kho đầu kì";
            this.nvTenHienThi.Expanded = true;
            this.nvTenHienThi.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.nvTonKho),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nvTheoKi),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nvTheoHangHoa),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nvKhachHang),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nvNhaCungCap),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nvHangHoa),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nvKhachHangDanhMuc),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nvNhaCungCapDanhMuc),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nvFileExlseMau)});
            this.nvTenHienThi.Name = "nvTenHienThi";
            // 
            // nvTonKho
            // 
            this.nvTonKho.Caption = "Tồn kho";
            this.nvTonKho.LargeImage = ((System.Drawing.Image)(resources.GetObject("nvTonKho.LargeImage")));
            this.nvTonKho.Name = "nvTonKho";
            this.nvTonKho.SmallImage = ((System.Drawing.Image)(resources.GetObject("nvTonKho.SmallImage")));
            this.nvTonKho.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nvTonKho_LinkClicked);
            // 
            // nvTheoKi
            // 
            this.nvTheoKi.Caption = "Theo kì";
            this.nvTheoKi.LargeImage = ((System.Drawing.Image)(resources.GetObject("nvTheoKi.LargeImage")));
            this.nvTheoKi.Name = "nvTheoKi";
            this.nvTheoKi.SmallImage = ((System.Drawing.Image)(resources.GetObject("nvTheoKi.SmallImage")));
            this.nvTheoKi.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nvTheoKi_LinkClicked);
            // 
            // nvTheoHangHoa
            // 
            this.nvTheoHangHoa.Caption = "Theo hàng hóa";
            this.nvTheoHangHoa.LargeImage = ((System.Drawing.Image)(resources.GetObject("nvTheoHangHoa.LargeImage")));
            this.nvTheoHangHoa.Name = "nvTheoHangHoa";
            this.nvTheoHangHoa.SmallImage = ((System.Drawing.Image)(resources.GetObject("nvTheoHangHoa.SmallImage")));
            this.nvTheoHangHoa.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nvTheoHangHoa_LinkClicked);
            // 
            // nvKhachHang
            // 
            this.nvKhachHang.Caption = "Khách hàng";
            this.nvKhachHang.LargeImage = ((System.Drawing.Image)(resources.GetObject("nvKhachHang.LargeImage")));
            this.nvKhachHang.Name = "nvKhachHang";
            this.nvKhachHang.SmallImage = ((System.Drawing.Image)(resources.GetObject("nvKhachHang.SmallImage")));
            this.nvKhachHang.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nvKhachHang_LinkClicked);
            // 
            // nvNhaCungCap
            // 
            this.nvNhaCungCap.Caption = "Nhà cung cấp";
            this.nvNhaCungCap.LargeImage = ((System.Drawing.Image)(resources.GetObject("nvNhaCungCap.LargeImage")));
            this.nvNhaCungCap.Name = "nvNhaCungCap";
            this.nvNhaCungCap.SmallImage = ((System.Drawing.Image)(resources.GetObject("nvNhaCungCap.SmallImage")));
            this.nvNhaCungCap.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nvNhaCungCap_LinkClicked);
            // 
            // nvHangHoa
            // 
            this.nvHangHoa.Caption = "Hàng hóa";
            this.nvHangHoa.LargeImage = ((System.Drawing.Image)(resources.GetObject("nvHangHoa.LargeImage")));
            this.nvHangHoa.Name = "nvHangHoa";
            this.nvHangHoa.SmallImage = ((System.Drawing.Image)(resources.GetObject("nvHangHoa.SmallImage")));
            this.nvHangHoa.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nvHangHoa_LinkClicked);
            // 
            // nvKhachHangDanhMuc
            // 
            this.nvKhachHangDanhMuc.Caption = "Khách hàng";
            this.nvKhachHangDanhMuc.LargeImage = ((System.Drawing.Image)(resources.GetObject("nvKhachHangDanhMuc.LargeImage")));
            this.nvKhachHangDanhMuc.Name = "nvKhachHangDanhMuc";
            this.nvKhachHangDanhMuc.SmallImage = ((System.Drawing.Image)(resources.GetObject("nvKhachHangDanhMuc.SmallImage")));
            this.nvKhachHangDanhMuc.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nvKhachHangDanhMuc_LinkClicked);
            // 
            // nvNhaCungCapDanhMuc
            // 
            this.nvNhaCungCapDanhMuc.Caption = "Nhà cung cấp";
            this.nvNhaCungCapDanhMuc.LargeImage = ((System.Drawing.Image)(resources.GetObject("nvNhaCungCapDanhMuc.LargeImage")));
            this.nvNhaCungCapDanhMuc.Name = "nvNhaCungCapDanhMuc";
            this.nvNhaCungCapDanhMuc.SmallImage = ((System.Drawing.Image)(resources.GetObject("nvNhaCungCapDanhMuc.SmallImage")));
            this.nvNhaCungCapDanhMuc.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nvNhaCungCapDanhMuc_LinkClicked);
            // 
            // nvFileExlseMau
            // 
            this.nvFileExlseMau.Caption = "File exsle mẫu";
            this.nvFileExlseMau.LargeImage = ((System.Drawing.Image)(resources.GetObject("nvFileExlseMau.LargeImage")));
            this.nvFileExlseMau.Name = "nvFileExlseMau";
            this.nvFileExlseMau.SmallImage = ((System.Drawing.Image)(resources.GetObject("nvFileExlseMau.SmallImage")));
            // 
            // tabNhapSoDuDauKi
            // 
            this.tabNhapSoDuDauKi.Controls.Add(this.tabPage1);
            this.tabNhapSoDuDauKi.Controls.Add(this.tabPage2);
            this.tabNhapSoDuDauKi.Controls.Add(this.tabPage3);
            this.tabNhapSoDuDauKi.Controls.Add(this.tabPage4);
            this.tabNhapSoDuDauKi.Controls.Add(this.tabPage5);
            this.tabNhapSoDuDauKi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabNhapSoDuDauKi.Location = new System.Drawing.Point(166, 0);
            this.tabNhapSoDuDauKi.Name = "tabNhapSoDuDauKi";
            this.tabNhapSoDuDauKi.SelectedIndex = 0;
            this.tabNhapSoDuDauKi.Size = new System.Drawing.Size(887, 568);
            this.tabNhapSoDuDauKi.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(879, 542);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Tồn kho";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnnapLai);
            this.panel3.Controls.Add(this.btnThemMoiHangHoa);
            this.panel3.Controls.Add(this.grcDanhSachTonKhoDauKi);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 119);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(873, 420);
            this.panel3.TabIndex = 2;
            // 
            // grcDanhSachTonKhoDauKi
            // 
            this.grcDanhSachTonKhoDauKi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcDanhSachTonKhoDauKi.Location = new System.Drawing.Point(0, 0);
            this.grcDanhSachTonKhoDauKi.MainView = this.gridView1;
            this.grcDanhSachTonKhoDauKi.Name = "grcDanhSachTonKhoDauKi";
            this.grcDanhSachTonKhoDauKi.Size = new System.Drawing.Size(873, 420);
            this.grcDanhSachTonKhoDauKi.TabIndex = 0;
            this.grcDanhSachTonKhoDauKi.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView1.GridControl = this.grcDanhSachTonKhoDauKi;
            this.gridView1.Name = "gridView1";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.layoutControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 39);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(873, 80);
            this.panel2.TabIndex = 1;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.dtpNgayNhapTonKho);
            this.layoutControl1.Controls.Add(this.txtGhiChuTonKho);
            this.layoutControl1.Controls.Add(this.txtPhieuTonKho);
            this.layoutControl1.Controls.Add(this.lueKhoTonKho);
            this.layoutControl1.Controls.Add(this.txtNguoiNhapKhoDauKi);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(873, 80);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // dtpNgayNhapTonKho
            // 
            this.dtpNgayNhapTonKho.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpNgayNhapTonKho.Location = new System.Drawing.Point(625, 36);
            this.dtpNgayNhapTonKho.Name = "dtpNgayNhapTonKho";
            this.dtpNgayNhapTonKho.Size = new System.Drawing.Size(236, 20);
            this.dtpNgayNhapTonKho.TabIndex = 8;
            // 
            // txtGhiChuTonKho
            // 
            this.txtGhiChuTonKho.Location = new System.Drawing.Point(73, 36);
            this.txtGhiChuTonKho.Name = "txtGhiChuTonKho";
            this.txtGhiChuTonKho.Size = new System.Drawing.Size(487, 20);
            this.txtGhiChuTonKho.StyleController = this.layoutControl1;
            this.txtGhiChuTonKho.TabIndex = 7;
            // 
            // txtPhieuTonKho
            // 
            this.txtPhieuTonKho.Location = new System.Drawing.Point(625, 12);
            this.txtPhieuTonKho.Name = "txtPhieuTonKho";
            this.txtPhieuTonKho.Size = new System.Drawing.Size(236, 20);
            this.txtPhieuTonKho.TabIndex = 6;
            // 
            // lueKhoTonKho
            // 
            this.lueKhoTonKho.Location = new System.Drawing.Point(343, 12);
            this.lueKhoTonKho.Name = "lueKhoTonKho";
            this.lueKhoTonKho.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueKhoTonKho.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenKho", "Tên kho"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id_Kho", "Mã")});
            this.lueKhoTonKho.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.lueKhoTonKho.Size = new System.Drawing.Size(217, 20);
            this.lueKhoTonKho.StyleController = this.layoutControl1;
            this.lueKhoTonKho.TabIndex = 5;
            // 
            // txtNguoiNhapKhoDauKi
            // 
            this.txtNguoiNhapKhoDauKi.Location = new System.Drawing.Point(73, 12);
            this.txtNguoiNhapKhoDauKi.Name = "txtNguoiNhapKhoDauKi";
            this.txtNguoiNhapKhoDauKi.Size = new System.Drawing.Size(205, 20);
            this.txtNguoiNhapKhoDauKi.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(873, 80);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtNguoiNhapKhoDauKi;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(270, 24);
            this.layoutControlItem1.Text = "Người nhập";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(58, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(552, 48);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(301, 12);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lueKhoTonKho;
            this.layoutControlItem2.Location = new System.Drawing.Point(270, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(282, 24);
            this.layoutControlItem2.Text = "Kho nhập";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(58, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtPhieuTonKho;
            this.layoutControlItem3.Location = new System.Drawing.Point(552, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(301, 24);
            this.layoutControlItem3.Text = "Phiếu";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(58, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtGhiChuTonKho;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(552, 36);
            this.layoutControlItem4.Text = "Ghi chú";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(58, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.dtpNgayNhapTonKho;
            this.layoutControlItem5.Location = new System.Drawing.Point(552, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(301, 24);
            this.layoutControlItem5.Text = "Ngày nhập2";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(58, 13);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnDongTonKho);
            this.panel1.Controls.Add(this.btnLuuTonKho);
            this.panel1.Controls.Add(this.simpleButton3);
            this.panel1.Controls.Add(this.simpleButton2);
            this.panel1.Controls.Add(this.btnThemMoiTonKho);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(873, 36);
            this.panel1.TabIndex = 0;
            // 
            // btnDongTonKho
            // 
            this.btnDongTonKho.ImageUri.Uri = "Cancel;Size16x16;Colored";
            this.btnDongTonKho.Location = new System.Drawing.Point(381, 3);
            this.btnDongTonKho.Name = "btnDongTonKho";
            this.btnDongTonKho.Size = new System.Drawing.Size(75, 23);
            this.btnDongTonKho.TabIndex = 4;
            this.btnDongTonKho.Text = "Đóng";
            this.btnDongTonKho.Click += new System.EventHandler(this.btnDongTonKho_Click);
            // 
            // btnLuuTonKho
            // 
            this.btnLuuTonKho.ImageUri.Uri = "SaveAll;Size16x16;Colored";
            this.btnLuuTonKho.Location = new System.Drawing.Point(84, 3);
            this.btnLuuTonKho.Name = "btnLuuTonKho";
            this.btnLuuTonKho.Size = new System.Drawing.Size(96, 23);
            this.btnLuuTonKho.TabIndex = 3;
            this.btnLuuTonKho.Text = "Lưu & Thêm";
            this.btnLuuTonKho.Click += new System.EventHandler(this.btnLuuTonKho_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.ImageUri.Uri = "ExportToXML;Size16x16;Colored";
            this.simpleButton3.Location = new System.Drawing.Point(186, 3);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(108, 23);
            this.simpleButton3.TabIndex = 2;
            this.simpleButton3.Text = "Nhập từ exlce";
            // 
            // simpleButton2
            // 
            this.simpleButton2.ImageUri.Uri = "Refresh;Size16x16;Colored";
            this.simpleButton2.Location = new System.Drawing.Point(300, 3);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Nạp lại";
            // 
            // btnThemMoiTonKho
            // 
            this.btnThemMoiTonKho.ImageUri.Uri = "Edit;Size16x16;Colored";
            this.btnThemMoiTonKho.Location = new System.Drawing.Point(3, 3);
            this.btnThemMoiTonKho.Name = "btnThemMoiTonKho";
            this.btnThemMoiTonKho.Size = new System.Drawing.Size(75, 23);
            this.btnThemMoiTonKho.TabIndex = 0;
            this.btnThemMoiTonKho.Text = "Tạo mới";
            this.btnThemMoiTonKho.Click += new System.EventHandler(this.btnThemMoiTonKho_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel5);
            this.tabPage2.Controls.Add(this.panel4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(879, 542);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "theo kì";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.grcHHTheoKi);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 42);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(873, 497);
            this.panel5.TabIndex = 1;
            // 
            // grcHHTheoKi
            // 
            this.grcHHTheoKi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcHHTheoKi.Location = new System.Drawing.Point(0, 0);
            this.grcHHTheoKi.MainView = this.gricHHTheoKi;
            this.grcHHTheoKi.Name = "grcHHTheoKi";
            this.grcHHTheoKi.Size = new System.Drawing.Size(873, 497);
            this.grcHHTheoKi.TabIndex = 0;
            this.grcHHTheoKi.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gricHHTheoKi});
            // 
            // gricHHTheoKi
            // 
            this.gricHHTheoKi.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29});
            this.gricHHTheoKi.GridControl = this.grcHHTheoKi;
            this.gricHHTheoKi.Name = "gricHHTheoKi";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.layoutControl2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(873, 39);
            this.panel4.TabIndex = 0;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.btnXuatHHDauKi);
            this.layoutControl2.Controls.Add(this.simpleButton11);
            this.layoutControl2.Controls.Add(this.btnXoaHHTheoKi);
            this.layoutControl2.Controls.Add(this.btnSuaHHTheoKi);
            this.layoutControl2.Controls.Add(this.btnTaoMoiTheoKi);
            this.layoutControl2.Controls.Add(this.btnTimKiemTheoKi);
            this.layoutControl2.Controls.Add(this.dtpNgayKetThuc);
            this.layoutControl2.Controls.Add(this.dtpNgayBatDau);
            this.layoutControl2.Controls.Add(this.textBox3);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(519, 177, 450, 400);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(873, 39);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // btnXuatHHDauKi
            // 
            this.btnXuatHHDauKi.ImageUri.Uri = "Paste;Size16x16;Colored";
            this.btnXuatHHDauKi.Location = new System.Drawing.Point(775, 12);
            this.btnXuatHHDauKi.Name = "btnXuatHHDauKi";
            this.btnXuatHHDauKi.Size = new System.Drawing.Size(69, 22);
            this.btnXuatHHDauKi.StyleController = this.layoutControl2;
            this.btnXuatHHDauKi.TabIndex = 13;
            this.btnXuatHHDauKi.Text = "Xuất";
            this.btnXuatHHDauKi.Click += new System.EventHandler(this.btnXuatHHDauKi_Click);
            // 
            // simpleButton11
            // 
            this.simpleButton11.ImageUri.Uri = "Print;Size16x16;Colored";
            this.simpleButton11.Location = new System.Drawing.Point(705, 12);
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.Size = new System.Drawing.Size(66, 22);
            this.simpleButton11.StyleController = this.layoutControl2;
            this.simpleButton11.TabIndex = 12;
            this.simpleButton11.Text = "In";
            // 
            // btnXoaHHTheoKi
            // 
            this.btnXoaHHTheoKi.ImageUri.Uri = "Delete;Size16x16;Colored";
            this.btnXoaHHTheoKi.Location = new System.Drawing.Point(632, 12);
            this.btnXoaHHTheoKi.Name = "btnXoaHHTheoKi";
            this.btnXoaHHTheoKi.Size = new System.Drawing.Size(69, 22);
            this.btnXoaHHTheoKi.StyleController = this.layoutControl2;
            this.btnXoaHHTheoKi.TabIndex = 10;
            this.btnXoaHHTheoKi.Text = "Xóa";
            this.btnXoaHHTheoKi.Click += new System.EventHandler(this.btnXoaHHTheoKi_Click);
            // 
            // btnSuaHHTheoKi
            // 
            this.btnSuaHHTheoKi.ImageUri.Uri = "Replace;Size16x16;Colored";
            this.btnSuaHHTheoKi.Location = new System.Drawing.Point(539, 12);
            this.btnSuaHHTheoKi.Name = "btnSuaHHTheoKi";
            this.btnSuaHHTheoKi.Size = new System.Drawing.Size(89, 22);
            this.btnSuaHHTheoKi.StyleController = this.layoutControl2;
            this.btnSuaHHTheoKi.TabIndex = 9;
            this.btnSuaHHTheoKi.Text = "Sửa chữa";
            this.btnSuaHHTheoKi.Click += new System.EventHandler(this.btnSuaHHTheoKi_Click);
            // 
            // btnTaoMoiTheoKi
            // 
            this.btnTaoMoiTheoKi.ImageUri.Uri = "AddItem;Size16x16;Colored";
            this.btnTaoMoiTheoKi.Location = new System.Drawing.Point(465, 12);
            this.btnTaoMoiTheoKi.Name = "btnTaoMoiTheoKi";
            this.btnTaoMoiTheoKi.Size = new System.Drawing.Size(70, 22);
            this.btnTaoMoiTheoKi.StyleController = this.layoutControl2;
            this.btnTaoMoiTheoKi.TabIndex = 8;
            this.btnTaoMoiTheoKi.Text = "Tạo mới";
            this.btnTaoMoiTheoKi.Click += new System.EventHandler(this.btnTaoMoiTheoKi_Click);
            // 
            // btnTimKiemTheoKi
            // 
            this.btnTimKiemTheoKi.ImageUri.Uri = "Zoom;Size16x16;Colored";
            this.btnTimKiemTheoKi.Location = new System.Drawing.Point(392, 12);
            this.btnTimKiemTheoKi.Name = "btnTimKiemTheoKi";
            this.btnTimKiemTheoKi.Size = new System.Drawing.Size(69, 22);
            this.btnTimKiemTheoKi.StyleController = this.layoutControl2;
            this.btnTimKiemTheoKi.TabIndex = 7;
            this.btnTimKiemTheoKi.Text = "Xem";
            this.btnTimKiemTheoKi.Click += new System.EventHandler(this.btnTimKiemTheoKi_Click);
            // 
            // dtpNgayKetThuc
            // 
            this.dtpNgayKetThuc.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpNgayKetThuc.Location = new System.Drawing.Point(304, 12);
            this.dtpNgayKetThuc.Name = "dtpNgayKetThuc";
            this.dtpNgayKetThuc.Size = new System.Drawing.Size(84, 20);
            this.dtpNgayKetThuc.TabIndex = 6;
            // 
            // dtpNgayBatDau
            // 
            this.dtpNgayBatDau.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpNgayBatDau.Location = new System.Drawing.Point(182, 12);
            this.dtpNgayBatDau.Name = "dtpNgayBatDau";
            this.dtpNgayBatDau.Size = new System.Drawing.Size(71, 20);
            this.dtpNgayBatDau.TabIndex = 5;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(59, 12);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(72, 20);
            this.textBox3.TabIndex = 4;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.emptySpaceItem2,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem14,
            this.layoutControlItem13});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(856, 54);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textBox3;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(123, 24);
            this.layoutControlItem6.Text = "Tùy chọn";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(44, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 24);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(380, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.dtpNgayBatDau;
            this.layoutControlItem7.Location = new System.Drawing.Point(123, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(122, 24);
            this.layoutControlItem7.Text = "Từ";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(44, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.dtpNgayKetThuc;
            this.layoutControlItem8.Location = new System.Drawing.Point(245, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(135, 24);
            this.layoutControlItem8.Text = "Đến";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(44, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.btnTimKiemTheoKi;
            this.layoutControlItem9.Location = new System.Drawing.Point(380, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(73, 34);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.btnTaoMoiTheoKi;
            this.layoutControlItem10.Location = new System.Drawing.Point(453, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(74, 34);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.btnSuaHHTheoKi;
            this.layoutControlItem11.Location = new System.Drawing.Point(527, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(93, 34);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.btnXoaHHTheoKi;
            this.layoutControlItem12.Location = new System.Drawing.Point(620, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(73, 34);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.simpleButton11;
            this.layoutControlItem14.Location = new System.Drawing.Point(693, 0);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(70, 34);
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.btnXuatHHDauKi;
            this.layoutControlItem13.Location = new System.Drawing.Point(763, 0);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(73, 34);
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel7);
            this.tabPage3.Controls.Add(this.panel6);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(879, 542);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Theo hàng hóa";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.grcDanhSachTheoHangHoa);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 54);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(873, 485);
            this.panel7.TabIndex = 1;
            // 
            // grcDanhSachTheoHangHoa
            // 
            this.grcDanhSachTheoHangHoa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcDanhSachTheoHangHoa.Location = new System.Drawing.Point(0, 0);
            this.grcDanhSachTheoHangHoa.MainView = this.gricDanhSachHangHoaTheoDauKi;
            this.grcDanhSachTheoHangHoa.Name = "grcDanhSachTheoHangHoa";
            this.grcDanhSachTheoHangHoa.Size = new System.Drawing.Size(873, 485);
            this.grcDanhSachTheoHangHoa.TabIndex = 0;
            this.grcDanhSachTheoHangHoa.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gricDanhSachHangHoaTheoDauKi});
            // 
            // gricDanhSachHangHoaTheoDauKi
            // 
            this.gricDanhSachHangHoaTheoDauKi.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24});
            this.gricDanhSachHangHoaTheoDauKi.GridControl = this.grcDanhSachTheoHangHoa;
            this.gricDanhSachHangHoaTheoDauKi.Name = "gricDanhSachHangHoaTheoDauKi";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.layoutControl3);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(3, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(873, 51);
            this.panel6.TabIndex = 0;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.textBox4);
            this.layoutControl3.Controls.Add(this.dtpNgayBDTheoHH);
            this.layoutControl3.Controls.Add(this.dtpNgayKTTheoHH);
            this.layoutControl3.Controls.Add(this.btnXemHHTheoNgay);
            this.layoutControl3.Controls.Add(this.btnSuaHHTheoDauKi);
            this.layoutControl3.Controls.Add(this.btnXoaHangHoaTheoCt);
            this.layoutControl3.Controls.Add(this.btnTaoMoiHHdauKi);
            this.layoutControl3.Controls.Add(this.simpleButton13);
            this.layoutControl3.Controls.Add(this.btnXuatDSHangHoa);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 0);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(873, 51);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(56, 12);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(42, 20);
            this.textBox4.TabIndex = 12;
            // 
            // dtpNgayBDTheoHH
            // 
            this.dtpNgayBDTheoHH.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpNgayBDTheoHH.Location = new System.Drawing.Point(146, 12);
            this.dtpNgayBDTheoHH.Name = "dtpNgayBDTheoHH";
            this.dtpNgayBDTheoHH.Size = new System.Drawing.Size(71, 20);
            this.dtpNgayBDTheoHH.TabIndex = 11;
            // 
            // dtpNgayKTTheoHH
            // 
            this.dtpNgayKTTheoHH.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpNgayKTTheoHH.Location = new System.Drawing.Point(265, 12);
            this.dtpNgayKTTheoHH.Name = "dtpNgayKTTheoHH";
            this.dtpNgayKTTheoHH.Size = new System.Drawing.Size(92, 20);
            this.dtpNgayKTTheoHH.TabIndex = 10;
            // 
            // btnXemHHTheoNgay
            // 
            this.btnXemHHTheoNgay.ImageUri.Uri = "Zoom;Size16x16;Colored";
            this.btnXemHHTheoNgay.Location = new System.Drawing.Point(361, 12);
            this.btnXemHHTheoNgay.Name = "btnXemHHTheoNgay";
            this.btnXemHHTheoNgay.Size = new System.Drawing.Size(72, 22);
            this.btnXemHHTheoNgay.StyleController = this.layoutControl3;
            this.btnXemHHTheoNgay.TabIndex = 9;
            this.btnXemHHTheoNgay.Text = "Xem";
            this.btnXemHHTheoNgay.Click += new System.EventHandler(this.simpleButton17_Click);
            // 
            // btnSuaHHTheoDauKi
            // 
            this.btnSuaHHTheoDauKi.ImageUri.Uri = "Replace;Size16x16;Colored";
            this.btnSuaHHTheoDauKi.Location = new System.Drawing.Point(437, 12);
            this.btnSuaHHTheoDauKi.Name = "btnSuaHHTheoDauKi";
            this.btnSuaHHTheoDauKi.Size = new System.Drawing.Size(78, 22);
            this.btnSuaHHTheoDauKi.StyleController = this.layoutControl3;
            this.btnSuaHHTheoDauKi.TabIndex = 8;
            this.btnSuaHHTheoDauKi.Text = "Sửa";
            this.btnSuaHHTheoDauKi.Click += new System.EventHandler(this.btnSuaHHTheoDauKi_Click);
            // 
            // btnXoaHangHoaTheoCt
            // 
            this.btnXoaHangHoaTheoCt.ImageUri.Uri = "Delete;Size16x16;Colored";
            this.btnXoaHangHoaTheoCt.Location = new System.Drawing.Point(519, 12);
            this.btnXoaHangHoaTheoCt.Name = "btnXoaHangHoaTheoCt";
            this.btnXoaHangHoaTheoCt.Size = new System.Drawing.Size(78, 22);
            this.btnXoaHangHoaTheoCt.StyleController = this.layoutControl3;
            this.btnXoaHangHoaTheoCt.TabIndex = 7;
            this.btnXoaHangHoaTheoCt.Text = "Xóa";
            this.btnXoaHangHoaTheoCt.Click += new System.EventHandler(this.btnXoaHangHoaTheoCt_Click);
            // 
            // btnTaoMoiHHdauKi
            // 
            this.btnTaoMoiHHdauKi.ImageUri.Uri = "Edit;Size16x16;Colored";
            this.btnTaoMoiHHdauKi.Location = new System.Drawing.Point(601, 12);
            this.btnTaoMoiHHdauKi.Name = "btnTaoMoiHHdauKi";
            this.btnTaoMoiHHdauKi.Size = new System.Drawing.Size(78, 22);
            this.btnTaoMoiHHdauKi.StyleController = this.layoutControl3;
            this.btnTaoMoiHHdauKi.TabIndex = 6;
            this.btnTaoMoiHHdauKi.Text = "Tạo mới";
            this.btnTaoMoiHHdauKi.Click += new System.EventHandler(this.btnTaoMoiHHdauKi_Click);
            // 
            // simpleButton13
            // 
            this.simpleButton13.ImageUri.Uri = "Print;Size16x16;Colored";
            this.simpleButton13.Location = new System.Drawing.Point(683, 12);
            this.simpleButton13.Name = "simpleButton13";
            this.simpleButton13.Size = new System.Drawing.Size(78, 22);
            this.simpleButton13.StyleController = this.layoutControl3;
            this.simpleButton13.TabIndex = 5;
            this.simpleButton13.Text = "In";
            // 
            // btnXuatDSHangHoa
            // 
            this.btnXuatDSHangHoa.ImageUri.Uri = "Copy;Size16x16;Colored";
            this.btnXuatDSHangHoa.Location = new System.Drawing.Point(765, 12);
            this.btnXuatDSHangHoa.Name = "btnXuatDSHangHoa";
            this.btnXuatDSHangHoa.Size = new System.Drawing.Size(79, 22);
            this.btnXuatDSHangHoa.StyleController = this.layoutControl3;
            this.btnXuatDSHangHoa.TabIndex = 4;
            this.btnXuatDSHangHoa.Text = "Xuất";
            this.btnXuatDSHangHoa.Click += new System.EventHandler(this.btnXuatDSHangHoa_Click);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem15,
            this.emptySpaceItem3,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(856, 56);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.btnXuatDSHangHoa;
            this.layoutControlItem15.Location = new System.Drawing.Point(753, 0);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(83, 26);
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(753, 26);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(83, 10);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.simpleButton13;
            this.layoutControlItem16.Location = new System.Drawing.Point(671, 0);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(82, 36);
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.btnTaoMoiHHdauKi;
            this.layoutControlItem17.Location = new System.Drawing.Point(589, 0);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(82, 36);
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.btnXoaHangHoaTheoCt;
            this.layoutControlItem18.Location = new System.Drawing.Point(507, 0);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(82, 36);
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.btnSuaHHTheoDauKi;
            this.layoutControlItem19.Location = new System.Drawing.Point(425, 0);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(82, 36);
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.btnXemHHTheoNgay;
            this.layoutControlItem20.Location = new System.Drawing.Point(349, 0);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(76, 36);
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextVisible = false;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.dtpNgayKTTheoHH;
            this.layoutControlItem21.Location = new System.Drawing.Point(209, 0);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(140, 36);
            this.layoutControlItem21.Text = "Đến";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(41, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.dtpNgayBDTheoHH;
            this.layoutControlItem22.Location = new System.Drawing.Point(90, 0);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(119, 36);
            this.layoutControlItem22.Text = "Từ";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(41, 13);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.textBox4;
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(90, 36);
            this.layoutControlItem23.Text = "lựa chọn";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(41, 13);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.panel9);
            this.tabPage4.Controls.Add(this.panel8);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(879, 542);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Khách hàng";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.gricDanhSachKhCongNo);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(3, 40);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(873, 499);
            this.panel9.TabIndex = 1;
            // 
            // gricDanhSachKhCongNo
            // 
            this.gricDanhSachKhCongNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gricDanhSachKhCongNo.Location = new System.Drawing.Point(0, 0);
            this.gricDanhSachKhCongNo.MainView = this.gridView4;
            this.gricDanhSachKhCongNo.Name = "gricDanhSachKhCongNo";
            this.gricDanhSachKhCongNo.Size = new System.Drawing.Size(873, 499);
            this.gricDanhSachKhCongNo.TabIndex = 0;
            this.gricDanhSachKhCongNo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11});
            this.gridView4.GridControl = this.gricDanhSachKhCongNo;
            this.gridView4.Name = "gridView4";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.layoutControl4);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(873, 37);
            this.panel8.TabIndex = 0;
            // 
            // layoutControl4
            // 
            this.layoutControl4.Controls.Add(this.btnDongKHSoDuDauKi);
            this.layoutControl4.Controls.Add(this.simpleButton20);
            this.layoutControl4.Controls.Add(this.btnLuuKhCongNo);
            this.layoutControl4.Controls.Add(this.simpleButton18);
            this.layoutControl4.Controls.Add(this.lueTimKHheoKhuVuc);
            this.layoutControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl4.Location = new System.Drawing.Point(0, 0);
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.Root = this.layoutControlGroup4;
            this.layoutControl4.Size = new System.Drawing.Size(873, 37);
            this.layoutControl4.TabIndex = 0;
            this.layoutControl4.Text = "layoutControl4";
            // 
            // btnDongKHSoDuDauKi
            // 
            this.btnDongKHSoDuDauKi.ImageUri.Uri = "Cancel;Size16x16;Colored";
            this.btnDongKHSoDuDauKi.Location = new System.Drawing.Point(754, 12);
            this.btnDongKHSoDuDauKi.Name = "btnDongKHSoDuDauKi";
            this.btnDongKHSoDuDauKi.Size = new System.Drawing.Size(90, 22);
            this.btnDongKHSoDuDauKi.StyleController = this.layoutControl4;
            this.btnDongKHSoDuDauKi.TabIndex = 8;
            this.btnDongKHSoDuDauKi.Text = "Đóng";
            this.btnDongKHSoDuDauKi.Click += new System.EventHandler(this.btnDongKHSoDuDauKi_Click);
            // 
            // simpleButton20
            // 
            this.simpleButton20.ImageUri.Uri = "ExportToXLS;Size16x16;Colored";
            this.simpleButton20.Location = new System.Drawing.Point(652, 12);
            this.simpleButton20.Name = "simpleButton20";
            this.simpleButton20.Size = new System.Drawing.Size(98, 22);
            this.simpleButton20.StyleController = this.layoutControl4;
            this.simpleButton20.TabIndex = 7;
            this.simpleButton20.Text = "Nhập từ exlce";
            // 
            // btnLuuKhCongNo
            // 
            this.btnLuuKhCongNo.ImageUri.Uri = "Save;Size16x16;Colored";
            this.btnLuuKhCongNo.Location = new System.Drawing.Point(557, 12);
            this.btnLuuKhCongNo.Name = "btnLuuKhCongNo";
            this.btnLuuKhCongNo.Size = new System.Drawing.Size(91, 22);
            this.btnLuuKhCongNo.StyleController = this.layoutControl4;
            this.btnLuuKhCongNo.TabIndex = 6;
            this.btnLuuKhCongNo.Text = "Lưu";
            this.btnLuuKhCongNo.Click += new System.EventHandler(this.btnLuuKhCongNo_Click);
            // 
            // simpleButton18
            // 
            this.simpleButton18.ImageUri.Uri = "Zoom;Size16x16;Colored";
            this.simpleButton18.Location = new System.Drawing.Point(472, 12);
            this.simpleButton18.Name = "simpleButton18";
            this.simpleButton18.Size = new System.Drawing.Size(81, 22);
            this.simpleButton18.StyleController = this.layoutControl4;
            this.simpleButton18.TabIndex = 5;
            this.simpleButton18.Text = "Xem";
            this.simpleButton18.Click += new System.EventHandler(this.simpleButton18_Click);
            // 
            // lueTimKHheoKhuVuc
            // 
            this.lueTimKHheoKhuVuc.Location = new System.Drawing.Point(54, 12);
            this.lueTimKHheoKhuVuc.Name = "lueTimKHheoKhuVuc";
            this.lueTimKHheoKhuVuc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTimKHheoKhuVuc.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenKhuVuc", "Khu vực"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id_KhuVuc", "Mã")});
            this.lueTimKHheoKhuVuc.Size = new System.Drawing.Size(414, 20);
            this.lueTimKHheoKhuVuc.StyleController = this.layoutControl4;
            this.lueTimKHheoKhuVuc.TabIndex = 4;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem24,
            this.emptySpaceItem4,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.layoutControlItem28});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(856, 54);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.lueTimKHheoKhuVuc;
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(460, 24);
            this.layoutControlItem24.Text = "Khu vực";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(39, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 24);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(460, 10);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.simpleButton18;
            this.layoutControlItem25.Location = new System.Drawing.Point(460, 0);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(85, 34);
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextVisible = false;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.btnLuuKhCongNo;
            this.layoutControlItem26.Location = new System.Drawing.Point(545, 0);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(95, 34);
            this.layoutControlItem26.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem26.TextVisible = false;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.simpleButton20;
            this.layoutControlItem27.Location = new System.Drawing.Point(640, 0);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(102, 34);
            this.layoutControlItem27.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem27.TextVisible = false;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.btnDongKHSoDuDauKi;
            this.layoutControlItem28.Location = new System.Drawing.Point(742, 0);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(94, 34);
            this.layoutControlItem28.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem28.TextVisible = false;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.panel11);
            this.tabPage5.Controls.Add(this.panel10);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(879, 542);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Nhà cung cấp";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.grcDanhSachNhaCungCapDauKi);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(3, 54);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(873, 485);
            this.panel11.TabIndex = 1;
            // 
            // grcDanhSachNhaCungCapDauKi
            // 
            this.grcDanhSachNhaCungCapDauKi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcDanhSachNhaCungCapDauKi.Location = new System.Drawing.Point(0, 0);
            this.grcDanhSachNhaCungCapDauKi.MainView = this.gridView5;
            this.grcDanhSachNhaCungCapDauKi.Name = "grcDanhSachNhaCungCapDauKi";
            this.grcDanhSachNhaCungCapDauKi.Size = new System.Drawing.Size(873, 485);
            this.grcDanhSachNhaCungCapDauKi.TabIndex = 0;
            this.grcDanhSachNhaCungCapDauKi.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16});
            this.gridView5.GridControl = this.grcDanhSachNhaCungCapDauKi;
            this.gridView5.Name = "gridView5";
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.layoutControl5);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(3, 3);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(873, 51);
            this.panel10.TabIndex = 0;
            // 
            // layoutControl5
            // 
            this.layoutControl5.Controls.Add(this.simpleButton25);
            this.layoutControl5.Controls.Add(this.simpleButton24);
            this.layoutControl5.Controls.Add(this.btnLuuDanhSachNCCDauKi);
            this.layoutControl5.Controls.Add(this.btnXemNCCTheoKHuVuc);
            this.layoutControl5.Controls.Add(this.lueTimKhuVucTheoNCC);
            this.layoutControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl5.Location = new System.Drawing.Point(0, 0);
            this.layoutControl5.Name = "layoutControl5";
            this.layoutControl5.Root = this.layoutControlGroup5;
            this.layoutControl5.Size = new System.Drawing.Size(873, 51);
            this.layoutControl5.TabIndex = 0;
            this.layoutControl5.Text = "layoutControl5";
            // 
            // simpleButton25
            // 
            this.simpleButton25.ImageUri.Uri = "Close;Size16x16;Colored";
            this.simpleButton25.Location = new System.Drawing.Point(758, 12);
            this.simpleButton25.Name = "simpleButton25";
            this.simpleButton25.Size = new System.Drawing.Size(86, 22);
            this.simpleButton25.StyleController = this.layoutControl5;
            this.simpleButton25.TabIndex = 8;
            this.simpleButton25.Text = "Đóng";
            this.simpleButton25.Click += new System.EventHandler(this.simpleButton25_Click);
            // 
            // simpleButton24
            // 
            this.simpleButton24.ImageUri.Uri = "ExportToXLS;Size16x16;Colored";
            this.simpleButton24.Location = new System.Drawing.Point(656, 12);
            this.simpleButton24.Name = "simpleButton24";
            this.simpleButton24.Size = new System.Drawing.Size(98, 22);
            this.simpleButton24.StyleController = this.layoutControl5;
            this.simpleButton24.TabIndex = 7;
            this.simpleButton24.Text = "Nhập từ exlce ";
            // 
            // btnLuuDanhSachNCCDauKi
            // 
            this.btnLuuDanhSachNCCDauKi.ImageUri.Uri = "Save;Size16x16;Colored";
            this.btnLuuDanhSachNCCDauKi.Location = new System.Drawing.Point(570, 12);
            this.btnLuuDanhSachNCCDauKi.Name = "btnLuuDanhSachNCCDauKi";
            this.btnLuuDanhSachNCCDauKi.Size = new System.Drawing.Size(82, 22);
            this.btnLuuDanhSachNCCDauKi.StyleController = this.layoutControl5;
            this.btnLuuDanhSachNCCDauKi.TabIndex = 6;
            this.btnLuuDanhSachNCCDauKi.Text = "Lưu";
            this.btnLuuDanhSachNCCDauKi.Click += new System.EventHandler(this.btnLuuDanhSachNCCDauKi_Click);
            // 
            // btnXemNCCTheoKHuVuc
            // 
            this.btnXemNCCTheoKHuVuc.ImageUri.Uri = "Zoom;Size16x16;Colored";
            this.btnXemNCCTheoKHuVuc.Location = new System.Drawing.Point(472, 12);
            this.btnXemNCCTheoKHuVuc.Name = "btnXemNCCTheoKHuVuc";
            this.btnXemNCCTheoKHuVuc.Size = new System.Drawing.Size(94, 22);
            this.btnXemNCCTheoKHuVuc.StyleController = this.layoutControl5;
            this.btnXemNCCTheoKHuVuc.TabIndex = 5;
            this.btnXemNCCTheoKHuVuc.Text = "Xem";
            this.btnXemNCCTheoKHuVuc.Click += new System.EventHandler(this.btnXemNCCTheoKHuVuc_Click);
            // 
            // lueTimKhuVucTheoNCC
            // 
            this.lueTimKhuVucTheoNCC.Location = new System.Drawing.Point(54, 12);
            this.lueTimKhuVucTheoNCC.Name = "lueTimKhuVucTheoNCC";
            this.lueTimKhuVucTheoNCC.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTimKhuVucTheoNCC.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenKhuVuc", "Khu vực"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id_KhuVuc", "Mã")});
            this.lueTimKhuVucTheoNCC.Size = new System.Drawing.Size(414, 20);
            this.lueTimKhuVucTheoNCC.StyleController = this.layoutControl5;
            this.lueTimKhuVucTheoNCC.TabIndex = 4;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem29,
            this.emptySpaceItem5,
            this.layoutControlItem30,
            this.layoutControlItem31,
            this.layoutControlItem32,
            this.layoutControlItem33});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(856, 54);
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.lueTimKhuVucTheoNCC;
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(460, 24);
            this.layoutControlItem29.Text = "Khu vực";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(39, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 24);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(460, 10);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.btnXemNCCTheoKHuVuc;
            this.layoutControlItem30.Location = new System.Drawing.Point(460, 0);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(98, 34);
            this.layoutControlItem30.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem30.TextVisible = false;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.btnLuuDanhSachNCCDauKi;
            this.layoutControlItem31.Location = new System.Drawing.Point(558, 0);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(86, 34);
            this.layoutControlItem31.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem31.TextVisible = false;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.simpleButton24;
            this.layoutControlItem32.Location = new System.Drawing.Point(644, 0);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(102, 34);
            this.layoutControlItem32.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem32.TextVisible = false;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.simpleButton25;
            this.layoutControlItem33.Location = new System.Drawing.Point(746, 0);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(90, 34);
            this.layoutControlItem33.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem33.TextVisible = false;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.label1);
            this.panel12.Location = new System.Drawing.Point(499, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(549, 22);
            this.panel12.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 0;
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "gridBand2";
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = -1;
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "gridBand1";
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = -1;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Mã hàng";
            this.gridColumn1.FieldName = "MaHang";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Tên hàng";
            this.gridColumn2.FieldName = "TenHang";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Đơn vị";
            this.gridColumn3.FieldName = "TenDonVi";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Số lượng";
            this.gridColumn4.FieldName = "SoLuong";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Đơn giá";
            this.gridColumn5.FieldName = "DonGia";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Thành tiền";
            this.gridColumn6.FieldName = "ThanhTien";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Khu vực";
            this.gridColumn7.FieldName = "TenKhuVuc";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 0;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Mã khách hàng";
            this.gridColumn8.FieldName = "Id_KH";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 1;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Tên khách hàng";
            this.gridColumn9.FieldName = "TenKhachHang";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 2;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Tiền nợ";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 3;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Diễn giải";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 4;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Khu vực";
            this.gridColumn12.FieldName = "TenKhuVuc";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 0;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Mã nhà cung cấp";
            this.gridColumn13.FieldName = "id_NCC";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 1;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Tên nhà cung cấp";
            this.gridColumn14.FieldName = "TenNCC";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 2;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 3;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 4;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Chứng từ";
            this.gridColumn17.FieldName = "id_ChungTuNhap";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 0;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Ngày";
            this.gridColumn18.FieldName = "NgayNhap";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 1;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Mã hàng";
            this.gridColumn19.FieldName = "id_HangHoa";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 2;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Kho hàng";
            this.gridColumn20.FieldName = "TenKho";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 3;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Đơn vị";
            this.gridColumn21.FieldName = "TenDonVi";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 4;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Số lượng";
            this.gridColumn22.FieldName = "SoLuong";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 5;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Tên hàng";
            this.gridColumn23.FieldName = "Tenhang";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 6;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Đơn giá";
            this.gridColumn24.FieldName = "DonGia";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 7;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Chứng từ";
            this.gridColumn25.FieldName = "id_ChungTuNhap";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 0;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Ngày";
            this.gridColumn26.FieldName = "NgayNhap";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 1;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Kho hàng";
            this.gridColumn27.FieldName = "TenKho";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 2;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Số tiền";
            this.gridColumn28.FieldName = "ThanhToan";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 3;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Diễn giải";
            this.gridColumn29.FieldName = "GhiChu";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 4;
            // 
            // btnThemMoiHangHoa
            // 
            this.btnThemMoiHangHoa.ImageUri.Uri = "Edit;Size16x16;Colored";
            this.btnThemMoiHangHoa.Location = new System.Drawing.Point(614, 0);
            this.btnThemMoiHangHoa.Name = "btnThemMoiHangHoa";
            this.btnThemMoiHangHoa.Size = new System.Drawing.Size(97, 34);
            this.btnThemMoiHangHoa.TabIndex = 1;
            this.btnThemMoiHangHoa.Text = "Thêm mới";
            this.btnThemMoiHangHoa.Click += new System.EventHandler(this.btnThemMoiHangHoa_Click);
            // 
            // btnnapLai
            // 
            this.btnnapLai.ImageUri.Uri = "Refresh;Size16x16;Colored";
            this.btnnapLai.Location = new System.Drawing.Point(717, 1);
            this.btnnapLai.Name = "btnnapLai";
            this.btnnapLai.Size = new System.Drawing.Size(113, 33);
            this.btnnapLai.TabIndex = 2;
            this.btnnapLai.Text = "Nạp lại";
            this.btnnapLai.Click += new System.EventHandler(this.btnnapLai_Click);
            // 
            // frmNhapSoDuDauKy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1053, 568);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.tabNhapSoDuDauKi);
            this.Controls.Add(this.dockPanel1);
            this.Name = "frmNhapSoDuDauKy";
            this.Text = "Nhập số dư đầu kỳ";
            this.Load += new System.EventHandler(this.frmNhapSoDuDauKy_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nvbTonKhoDauKi)).EndInit();
            this.tabNhapSoDuDauKi.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachTonKhoDauKi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChuTonKho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKhoTonKho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.panel1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcHHTheoKi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gricHHTheoKi)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachTheoHangHoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gricDanhSachHangHoaTheoDauKi)).EndInit();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gricDanhSachKhCongNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lueTimKHheoKhuVuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcDanhSachNhaCungCapDauKi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            this.panel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).EndInit();
            this.layoutControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lueTimKhuVucTheoNCC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraNavBar.NavBarControl nvbTonKhoDauKi;
        private DevExpress.XtraNavBar.NavBarGroup nvTenHienThi;
        private System.Windows.Forms.Button btnXemTonKhoDauki;
        private System.Windows.Forms.Button btnXembangKe;
        private System.Windows.Forms.Button btnXemCongNoDauKi;
        private System.Windows.Forms.Button btnThemDanhMuc;
        private DevExpress.XtraNavBar.NavBarItem nvTonKho;
        private DevExpress.XtraNavBar.NavBarItem nvTheoKi;
        private DevExpress.XtraNavBar.NavBarItem nvTheoHangHoa;
        private DevExpress.XtraNavBar.NavBarItem nvKhachHang;
        private DevExpress.XtraNavBar.NavBarItem nvNhaCungCap;
        private DevExpress.XtraNavBar.NavBarItem nvHangHoa;
        private DevExpress.XtraNavBar.NavBarItem nvKhachHangDanhMuc;
        private DevExpress.XtraNavBar.NavBarItem nvNhaCungCapDanhMuc;
        private DevExpress.XtraNavBar.NavBarItem nvFileExlseMau;
        private System.Windows.Forms.TabControl tabNhapSoDuDauKi;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private System.Windows.Forms.DateTimePicker dtpNgayNhapTonKho;
        private DevExpress.XtraEditors.TextEdit txtGhiChuTonKho;
        private System.Windows.Forms.TextBox txtPhieuTonKho;
        private DevExpress.XtraEditors.LookUpEdit lueKhoTonKho;
        private System.Windows.Forms.TextBox txtNguoiNhapKhoDauKi;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SimpleButton btnDongTonKho;
        private DevExpress.XtraEditors.SimpleButton btnLuuTonKho;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton btnThemMoiTonKho;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraGrid.GridControl grcHHTheoKi;
        private DevExpress.XtraGrid.Views.Grid.GridView gricHHTheoKi;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.SimpleButton btnXuatHHDauKi;
        private DevExpress.XtraEditors.SimpleButton simpleButton11;
        private DevExpress.XtraEditors.SimpleButton btnXoaHHTheoKi;
        private DevExpress.XtraEditors.SimpleButton btnSuaHHTheoKi;
        private DevExpress.XtraEditors.SimpleButton btnTaoMoiTheoKi;
        private DevExpress.XtraEditors.SimpleButton btnTimKiemTheoKi;
        private System.Windows.Forms.DateTimePicker dtpNgayKetThuc;
        private System.Windows.Forms.DateTimePicker dtpNgayBatDau;
        private System.Windows.Forms.TextBox textBox3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private System.Windows.Forms.Panel panel6;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.DateTimePicker dtpNgayBDTheoHH;
        private System.Windows.Forms.DateTimePicker dtpNgayKTTheoHH;
        private DevExpress.XtraEditors.SimpleButton btnXemHHTheoNgay;
        private DevExpress.XtraEditors.SimpleButton btnSuaHHTheoDauKi;
        private DevExpress.XtraEditors.SimpleButton btnXoaHangHoaTheoCt;
        private DevExpress.XtraEditors.SimpleButton btnTaoMoiHHdauKi;
        private DevExpress.XtraEditors.SimpleButton simpleButton13;
        private DevExpress.XtraEditors.SimpleButton btnXuatDSHangHoa;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private System.Windows.Forms.Panel panel7;
        private DevExpress.XtraGrid.GridControl grcDanhSachTheoHangHoa;
        private DevExpress.XtraGrid.Views.Grid.GridView gricDanhSachHangHoaTheoDauKi;
        private System.Windows.Forms.Panel panel9;
        private DevExpress.XtraGrid.GridControl gricDanhSachKhCongNo;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private System.Windows.Forms.Panel panel8;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraEditors.SimpleButton btnDongKHSoDuDauKi;
        private DevExpress.XtraEditors.SimpleButton simpleButton20;
        private DevExpress.XtraEditors.SimpleButton btnLuuKhCongNo;
        private DevExpress.XtraEditors.SimpleButton simpleButton18;
        private DevExpress.XtraEditors.LookUpEdit lueTimKHheoKhuVuc;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private System.Windows.Forms.Panel panel11;
        private DevExpress.XtraGrid.GridControl grcDanhSachNhaCungCapDauKi;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private System.Windows.Forms.Panel panel10;
        private DevExpress.XtraLayout.LayoutControl layoutControl5;
        private DevExpress.XtraEditors.SimpleButton simpleButton25;
        private DevExpress.XtraEditors.SimpleButton simpleButton24;
        private DevExpress.XtraEditors.SimpleButton btnLuuDanhSachNCCDauKi;
        private DevExpress.XtraEditors.SimpleButton btnXemNCCTheoKHuVuc;
        private DevExpress.XtraEditors.LookUpEdit lueTimKhuVucTheoNCC;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label1;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.GridControl grcDanhSachTonKhoDauKi;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraEditors.SimpleButton btnnapLai;
        private DevExpress.XtraEditors.SimpleButton btnThemMoiHangHoa;
    }
}