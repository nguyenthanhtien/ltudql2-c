﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Columns;

namespace QuanLyBanHang
{
    public partial class frmPhanQuyen : Form
    {
        DACK_ware5Entities1 db = new DACK_ware5Entities1();
      
        public frmPhanQuyen(string user)
        {
            InitializeComponent();
            LoadDanhSachvaiTro(user);
            LoadHeThong(user);
        }

        private void LoadHeThong(string user)
        {
            var username = db.NguoiDungs.FirstOrDefault(a => a.Username.Equals(user));
            var query = from n in db.VaiTroes
                        where username.id_VaiTro == n.idVaiTro
                        select new {n.idVaiTro,n.MoTa, n.TenVaiTro, username.TrangThai};
           
                grcVaiTroVaNguoiDung.DataSource = query.ToList();
            
        }

        private void LoadDanhSachvaiTro(string user)
        {
            var username = db.NguoiDungs.FirstOrDefault(a => a.Username.Equals(user));
            var query = from n in db.VaiTroes where n.idVaiTro == username.id_VaiTro select new { n.TenVaiTro };
            foreach (var item in query)
            {
                trlVaitro.Columns.Clear();
                TreeListColumn newColumn = trlVaitro.Columns.Add();
                newColumn.Caption = "Vai trò và Người dùng";
                newColumn.Visible = true;
                trlVaitro.Nodes.Clear();
                TreeListNode rootNode = trlVaitro.Nodes.Add(item.TenVaiTro, "Root Node");
                TreeListNode child1 = rootNode.Nodes.Add(user, "Child 1");
                trlVaitro.RefreshNode(rootNode);
            }
        }




        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnThemNguoiDung_Click(object sender, EventArgs e)
        {
            var form = new frmThemNguoiDung();
            form.ShowDialog();
        }

        private void btnThemVaiTro_Click(object sender, EventArgs e)
        {
            var form = new frmThemVaiTro();
            form.ShowDialog();
        }

        private void frmPhanQuyen_Load(object sender, EventArgs e)
        {


            var cn1 = from n in db.ChucNangs
                      from m in db.VaiTro_ChucNang
                      where n.id_ChucNang == m.id_ChucNang
                      select new { n.TenChucNang, m.Them, m.Xoa, m.Sua, m.Nhap, m.Xuat, m.in_ra };
            grdDanhSachPhanQuyen.DataSource = cn1.ToList();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            
            var form = new frmSuaVaiTroNguoiDung();
            form.ShowDialog();
        }
    }
}
