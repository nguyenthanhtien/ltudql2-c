﻿namespace QuanLyBanHang
{
    partial class frmPhanQuyen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnThemVaiTro = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemNguoiDung = new DevExpress.XtraEditors.SimpleButton();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.trlVaitro = new DevExpress.XtraTreeList.TreeList();
            this.grcVaiTroVaNguoiDung = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.idVaiTro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TenVaiTro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MoTa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TrangThai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grdDanhSachPhanQuyen = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.trlVaitro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcVaiTroVaNguoiDung)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdDanhSachPhanQuyen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // btnThemVaiTro
            // 
            this.btnThemVaiTro.ImageUri.Uri = "Add;Size16x16;Colored";
            this.btnThemVaiTro.Location = new System.Drawing.Point(12, 12);
            this.btnThemVaiTro.Name = "btnThemVaiTro";
            this.btnThemVaiTro.Size = new System.Drawing.Size(113, 23);
            this.btnThemVaiTro.TabIndex = 0;
            this.btnThemVaiTro.Text = "Thêm vai trò";
            this.btnThemVaiTro.Click += new System.EventHandler(this.btnThemVaiTro_Click);
            // 
            // btnThemNguoiDung
            // 
            this.btnThemNguoiDung.ImageUri.Uri = "Add;Size16x16;Colored";
            this.btnThemNguoiDung.Location = new System.Drawing.Point(131, 12);
            this.btnThemNguoiDung.Name = "btnThemNguoiDung";
            this.btnThemNguoiDung.Size = new System.Drawing.Size(114, 23);
            this.btnThemNguoiDung.TabIndex = 1;
            this.btnThemNguoiDung.Text = "Thêm người dùng";
            this.btnThemNguoiDung.Click += new System.EventHandler(this.btnThemNguoiDung_Click);
            // 
            // btnDong
            // 
            this.btnDong.ImageUri.Uri = "Delete;Size16x16;Colored";
            this.btnDong.Location = new System.Drawing.Point(413, 12);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(75, 23);
            this.btnDong.TabIndex = 3;
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.ImageUri.Uri = "Cancel;Size16x16;Colored";
            this.btnXoa.Location = new System.Drawing.Point(332, 12);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 4;
            this.btnXoa.Text = "Xóa";
            // 
            // btnSua
            // 
            this.btnSua.ImageUri.Uri = "Replace;Size16x16;Colored";
            this.btnSua.Location = new System.Drawing.Point(251, 12);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(75, 23);
            this.btnSua.TabIndex = 5;
            this.btnSua.Text = "Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // trlVaitro
            // 
            this.trlVaitro.Location = new System.Drawing.Point(14, 41);
            this.trlVaitro.Name = "trlVaitro";
            this.trlVaitro.Size = new System.Drawing.Size(204, 383);
            this.trlVaitro.TabIndex = 7;
            // 
            // grcVaiTroVaNguoiDung
            // 
            this.grcVaiTroVaNguoiDung.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grcVaiTroVaNguoiDung.Location = new System.Drawing.Point(223, 43);
            this.grcVaiTroVaNguoiDung.MainView = this.gridView1;
            this.grcVaiTroVaNguoiDung.Name = "grcVaiTroVaNguoiDung";
            this.grcVaiTroVaNguoiDung.Size = new System.Drawing.Size(774, 204);
            this.grcVaiTroVaNguoiDung.TabIndex = 8;
            this.grcVaiTroVaNguoiDung.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.idVaiTro,
            this.TenVaiTro,
            this.MoTa,
            this.TrangThai});
            this.gridView1.GridControl = this.grcVaiTroVaNguoiDung;
            this.gridView1.Name = "gridView1";
            // 
            // idVaiTro
            // 
            this.idVaiTro.Caption = "Mã nhóm";
            this.idVaiTro.FieldName = "idVaiTro";
            this.idVaiTro.Name = "idVaiTro";
            this.idVaiTro.Visible = true;
            this.idVaiTro.VisibleIndex = 0;
            // 
            // TenVaiTro
            // 
            this.TenVaiTro.Caption = "Vai trò";
            this.TenVaiTro.FieldName = "TenVaiTro";
            this.TenVaiTro.Name = "TenVaiTro";
            this.TenVaiTro.Visible = true;
            this.TenVaiTro.VisibleIndex = 1;
            // 
            // MoTa
            // 
            this.MoTa.Caption = "Diễn giải";
            this.MoTa.FieldName = "MoTa";
            this.MoTa.Name = "MoTa";
            this.MoTa.Visible = true;
            this.MoTa.VisibleIndex = 2;
            // 
            // TrangThai
            // 
            this.TrangThai.Caption = "Kích hoạt";
            this.TrangThai.FieldName = "TrangThai";
            this.TrangThai.Name = "TrangThai";
            this.TrangThai.Visible = true;
            this.TrangThai.VisibleIndex = 3;
            // 
            // grdDanhSachPhanQuyen
            // 
            this.grdDanhSachPhanQuyen.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdDanhSachPhanQuyen.Location = new System.Drawing.Point(224, 252);
            this.grdDanhSachPhanQuyen.MainView = this.gridView2;
            this.grdDanhSachPhanQuyen.Name = "grdDanhSachPhanQuyen";
            this.grdDanhSachPhanQuyen.Size = new System.Drawing.Size(773, 200);
            this.grdDanhSachPhanQuyen.TabIndex = 9;
            this.grdDanhSachPhanQuyen.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7});
            this.gridView2.GridControl = this.grdDanhSachPhanQuyen;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Chức năng";
            this.gridColumn1.FieldName = "TenChucNang";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Thêm";
            this.gridColumn2.FieldName = "Them";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Xóa";
            this.gridColumn3.FieldName = "Xoa";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Sửa";
            this.gridColumn4.FieldName = "Sua";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Nhập";
            this.gridColumn5.FieldName = "Nhap";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 3;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Xuất";
            this.gridColumn6.FieldName = "Xuat";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 4;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "In ra";
            this.gridColumn7.FieldName = "in_ra";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 5;
            // 
            // frmPhanQuyen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(999, 464);
            this.Controls.Add(this.grdDanhSachPhanQuyen);
            this.Controls.Add(this.grcVaiTroVaNguoiDung);
            this.Controls.Add(this.trlVaitro);
            this.Controls.Add(this.btnSua);
            this.Controls.Add(this.btnXoa);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.btnThemNguoiDung);
            this.Controls.Add(this.btnThemVaiTro);
            this.Name = "frmPhanQuyen";
            this.Text = "Phân quyền người dùng";
            this.Load += new System.EventHandler(this.frmPhanQuyen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trlVaitro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcVaiTroVaNguoiDung)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdDanhSachPhanQuyen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnThemVaiTro;
        private DevExpress.XtraEditors.SimpleButton btnThemNguoiDung;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraTreeList.TreeList trlVaitro;
        private DevExpress.XtraGrid.GridControl grcVaiTroVaNguoiDung;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn idVaiTro;
        private DevExpress.XtraGrid.Columns.GridColumn TenVaiTro;
        private DevExpress.XtraGrid.Columns.GridColumn MoTa;
        private DevExpress.XtraGrid.Columns.GridColumn TrangThai;
        private DevExpress.XtraGrid.GridControl grdDanhSachPhanQuyen;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
    }
}