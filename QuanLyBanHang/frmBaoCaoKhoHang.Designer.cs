﻿namespace QuanLyBanHang
{
    partial class frmBaoCaoKhoHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBaoCaoKhoHang));
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockManager2 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel2 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel2_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.navBarControl1 = new DevExpress.XtraNavBar.NavBarControl();
            this.navBarGroup1 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItem1 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem2 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem3 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem4 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem5 = new DevExpress.XtraNavBar.NavBarItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.tlMaHang = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlTenHang = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlDonVi = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlSoLuong = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlDonGia = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlThanhTien = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlNhomHang = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlKhoHang = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlConQuanLy = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.advBandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Mã = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn13 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lookUpEdit2 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit3 = new DevExpress.XtraEditors.FontEdit();
            this.lookUpEdit5 = new DevExpress.XtraEditors.DateEdit();
            this.lookUpEdit4 = new DevExpress.XtraEditors.DateEdit();
            this.tpTheKho = new System.Windows.Forms.TabPage();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.advBandedGridView2 = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn14 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn15 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn16 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn17 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn22 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn18 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn19 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn20 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn21 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.simpleButton13 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton12 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lookUpEdit7 = new DevExpress.XtraEditors.LookUpEdit();
            this.lkeTheKho = new DevExpress.XtraEditors.LookUpEdit();
            this.lkeMaHang = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit8 = new DevExpress.XtraEditors.DateEdit();
            this.lookUpEdit9 = new DevExpress.XtraEditors.DateEdit();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.advBandedGridView3 = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn25 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn26 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn27 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn28 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn29 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn30 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn31 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn32 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn33 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn34 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand16 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn35 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn36 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand17 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn37 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn38 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn40 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn41 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn24 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.simpleButton18 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton17 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton16 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton14 = new DevExpress.XtraEditors.SimpleButton();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lookUpEdit13 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit10 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit12 = new DevExpress.XtraEditors.DateEdit();
            this.lookUpEdit11 = new DevExpress.XtraEditors.DateEdit();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.advBandedGridView4 = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand19 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn39 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn42 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn43 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn44 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn45 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn46 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn47 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn48 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn49 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn50 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand20 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn51 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn52 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand21 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn53 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn54 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand22 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn55 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.simpleButton19 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton15 = new DevExpress.XtraEditors.SimpleButton();
            this.lookUpEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit15 = new DevExpress.XtraEditors.TextEdit();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            this.bandedGridColumn23 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.gridBand18 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager2)).BeginInit();
            this.dockPanel2.SuspendLayout();
            this.dockPanel2_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).BeginInit();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit5.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).BeginInit();
            this.tpTheKho.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkeTheKho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkeMaHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit8.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit9.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit9.Properties)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit12.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit11.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit11.Properties)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane",
            "DevExpress.XtraBars.TabFormControl"});
            // 
            // dockManager2
            // 
            this.dockManager2.Form = this;
            this.dockManager2.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel2});
            this.dockManager2.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane",
            "DevExpress.XtraBars.TabFormControl"});
            // 
            // dockPanel2
            // 
            this.dockPanel2.Controls.Add(this.dockPanel2_Container);
            this.dockPanel2.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel2.ID = new System.Guid("fb2b336d-470f-4420-89b6-ae2bd32d35b9");
            this.dockPanel2.ImageIndex = 1;
            this.dockPanel2.Location = new System.Drawing.Point(0, 0);
            this.dockPanel2.Name = "dockPanel2";
            this.dockPanel2.OriginalSize = new System.Drawing.Size(166, 200);
            this.dockPanel2.Size = new System.Drawing.Size(166, 419);
            this.dockPanel2.Text = "Danh Sách Báo Cáo";
            // 
            // dockPanel2_Container
            // 
            this.dockPanel2_Container.Controls.Add(this.navBarControl1);
            this.dockPanel2_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel2_Container.Name = "dockPanel2_Container";
            this.dockPanel2_Container.Size = new System.Drawing.Size(157, 392);
            this.dockPanel2_Container.TabIndex = 0;
            // 
            // navBarControl1
            // 
            this.navBarControl1.ActiveGroup = this.navBarGroup1;
            this.navBarControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navBarControl1.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.navBarControl1.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarGroup1});
            this.navBarControl1.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.navBarItem1,
            this.navBarItem2,
            this.navBarItem3,
            this.navBarItem4,
            this.navBarItem5});
            this.navBarControl1.LinkInterval = 1;
            this.navBarControl1.Location = new System.Drawing.Point(0, 0);
            this.navBarControl1.Name = "navBarControl1";
            this.navBarControl1.OptionsNavPane.ExpandedWidth = 157;
            this.navBarControl1.Size = new System.Drawing.Size(157, 392);
            this.navBarControl1.TabIndex = 0;
            this.navBarControl1.Text = "navBarControl1";
            this.navBarControl1.Click += new System.EventHandler(this.navBarControl1_Click);
            // 
            // navBarGroup1
            // 
            this.navBarGroup1.Caption = "Tồn Kho ";
            this.navBarGroup1.Expanded = true;
            this.navBarGroup1.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem1),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem2),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem3),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem4),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem5)});
            this.navBarGroup1.LargeImageIndex = 1;
            this.navBarGroup1.Name = "navBarGroup1";
            this.navBarGroup1.SmallImageIndex = 1;
            // 
            // navBarItem1
            // 
            this.navBarItem1.Caption = "Tồn Kho Tổng Hợp";
            this.navBarItem1.LargeImageIndex = 1;
            this.navBarItem1.Name = "navBarItem1";
            this.navBarItem1.SmallImage = ((System.Drawing.Image)(resources.GetObject("navBarItem1.SmallImage")));
            this.navBarItem1.SmallImageIndex = 1;
            this.navBarItem1.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem1_LinkClicked);
            // 
            // navBarItem2
            // 
            this.navBarItem2.Caption = "Nhập - Xuất - Tồn ";
            this.navBarItem2.LargeImageIndex = 1;
            this.navBarItem2.Name = "navBarItem2";
            this.navBarItem2.SmallImage = ((System.Drawing.Image)(resources.GetObject("navBarItem2.SmallImage")));
            this.navBarItem2.SmallImageIndex = 1;
            this.navBarItem2.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem2_LinkClicked);
            // 
            // navBarItem3
            // 
            this.navBarItem3.Caption = "Thẻ Kho";
            this.navBarItem3.LargeImageIndex = 1;
            this.navBarItem3.Name = "navBarItem3";
            this.navBarItem3.SmallImage = ((System.Drawing.Image)(resources.GetObject("navBarItem3.SmallImage")));
            this.navBarItem3.SmallImageIndex = 1;
            this.navBarItem3.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem3_LinkClicked);
            // 
            // navBarItem4
            // 
            this.navBarItem4.Caption = "Sổ Chi Tiết Hàng Hóa";
            this.navBarItem4.LargeImageIndex = 1;
            this.navBarItem4.Name = "navBarItem4";
            this.navBarItem4.SmallImage = ((System.Drawing.Image)(resources.GetObject("navBarItem4.SmallImage")));
            this.navBarItem4.SmallImageIndex = 1;
            this.navBarItem4.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem4_LinkClicked);
            // 
            // navBarItem5
            // 
            this.navBarItem5.Caption = "Lịch Sử Hàng Hóa";
            this.navBarItem5.LargeImageIndex = 1;
            this.navBarItem5.Name = "navBarItem5";
            this.navBarItem5.SmallImage = ((System.Drawing.Image)(resources.GetObject("navBarItem5.SmallImage")));
            this.navBarItem5.SmallImageIndex = 1;
            this.navBarItem5.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem5_LinkClicked);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tabControl1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(166, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1065, 419);
            this.panel1.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tpTheKho);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1065, 419);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.treeList1);
            this.tabPage1.Controls.Add(this.simpleButton5);
            this.tabPage1.Controls.Add(this.simpleButton1);
            this.tabPage1.Controls.Add(this.simpleButton4);
            this.tabPage1.Controls.Add(this.simpleButton3);
            this.tabPage1.Controls.Add(this.simpleButton2);
            this.tabPage1.Controls.Add(this.lookUpEdit1);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1057, 393);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Tồn Kho Tổng Hợp ";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // treeList1
            // 
            this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.tlMaHang,
            this.tlTenHang,
            this.tlDonVi,
            this.tlSoLuong,
            this.tlDonGia,
            this.tlThanhTien,
            this.tlNhomHang,
            this.tlKhoHang,
            this.tlConQuanLy});
            this.treeList1.Cursor = System.Windows.Forms.Cursors.Default;
            this.treeList1.Location = new System.Drawing.Point(2, 89);
            this.treeList1.Name = "treeList1";
            this.treeList1.Size = new System.Drawing.Size(863, 300);
            this.treeList1.TabIndex = 8;
            // 
            // tlMaHang
            // 
            this.tlMaHang.Caption = "Mã Hàng ";
            this.tlMaHang.FieldName = "Mã Hàng ";
            this.tlMaHang.Name = "tlMaHang";
            this.tlMaHang.Visible = true;
            this.tlMaHang.VisibleIndex = 0;
            // 
            // tlTenHang
            // 
            this.tlTenHang.Caption = "Tên Hàng ";
            this.tlTenHang.FieldName = "Tên Hàng ";
            this.tlTenHang.Name = "tlTenHang";
            this.tlTenHang.Visible = true;
            this.tlTenHang.VisibleIndex = 1;
            // 
            // tlDonVi
            // 
            this.tlDonVi.Caption = "Đơn Vị ";
            this.tlDonVi.FieldName = "Đơn Vị ";
            this.tlDonVi.Name = "tlDonVi";
            this.tlDonVi.Visible = true;
            this.tlDonVi.VisibleIndex = 2;
            // 
            // tlSoLuong
            // 
            this.tlSoLuong.Caption = "Số Lượng ";
            this.tlSoLuong.FieldName = "Số Lượng ";
            this.tlSoLuong.Name = "tlSoLuong";
            this.tlSoLuong.Visible = true;
            this.tlSoLuong.VisibleIndex = 3;
            // 
            // tlDonGia
            // 
            this.tlDonGia.Caption = "Đơn Giá";
            this.tlDonGia.FieldName = "Đơn Giá";
            this.tlDonGia.Name = "tlDonGia";
            this.tlDonGia.Visible = true;
            this.tlDonGia.VisibleIndex = 4;
            // 
            // tlThanhTien
            // 
            this.tlThanhTien.Caption = "Thành Tiền ";
            this.tlThanhTien.FieldName = "Thành Tiền ";
            this.tlThanhTien.Name = "tlThanhTien";
            this.tlThanhTien.Visible = true;
            this.tlThanhTien.VisibleIndex = 5;
            // 
            // tlNhomHang
            // 
            this.tlNhomHang.Caption = "Nhóm Hàng ";
            this.tlNhomHang.FieldName = "Nhóm Hàng ";
            this.tlNhomHang.Name = "tlNhomHang";
            this.tlNhomHang.Visible = true;
            this.tlNhomHang.VisibleIndex = 6;
            // 
            // tlKhoHang
            // 
            this.tlKhoHang.Caption = "Kho Hàng ";
            this.tlKhoHang.FieldName = "Kho Hàng ";
            this.tlKhoHang.Name = "tlKhoHang";
            this.tlKhoHang.Visible = true;
            this.tlKhoHang.VisibleIndex = 7;
            // 
            // tlConQuanLy
            // 
            this.tlConQuanLy.Caption = "Còn Quản Lý ";
            this.tlConQuanLy.FieldName = "Còn Quản Lý ";
            this.tlConQuanLy.Name = "tlConQuanLy";
            this.tlConQuanLy.Visible = true;
            this.tlConQuanLy.VisibleIndex = 8;
            // 
            // simpleButton5
            // 
           // this.simpleButton5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton5.ImageOptions.Image")));
            this.simpleButton5.Location = new System.Drawing.Point(6, 46);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(75, 23);
            this.simpleButton5.TabIndex = 7;
            this.simpleButton5.Text = "Xem";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(198, 553);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 6;
            this.simpleButton1.Text = "simpleButton1";
            // 
            // simpleButton4
            // 
          //  this.simpleButton4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton4.ImageOptions.Image")));
            this.simpleButton4.Location = new System.Drawing.Point(125, 46);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(75, 23);
            this.simpleButton4.TabIndex = 5;
            this.simpleButton4.Text = "Xuất";
            // 
            // simpleButton3
            // 
            //this.simpleButton3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton3.ImageOptions.Image")));
            this.simpleButton3.Location = new System.Drawing.Point(274, 46);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(75, 23);
            this.simpleButton3.TabIndex = 4;
            this.simpleButton3.Text = "In";
            // 
            // simpleButton2
            // 
           // this.simpleButton2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.ImageOptions.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(417, 46);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 3;
            this.simpleButton2.Text = "Đóng";
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.Location = new System.Drawing.Point(6, 6);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit1.Properties.NullText = "( Tất cả )";
            this.lookUpEdit1.Size = new System.Drawing.Size(132, 20);
            this.lookUpEdit1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(284, 156);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.simpleButton9);
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Controls.Add(this.simpleButton8);
            this.tabPage2.Controls.Add(this.simpleButton7);
            this.tabPage2.Controls.Add(this.simpleButton6);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.lookUpEdit2);
            this.tabPage2.Controls.Add(this.lookUpEdit3);
            this.tabPage2.Controls.Add(this.lookUpEdit5);
            this.tabPage2.Controls.Add(this.lookUpEdit4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1057, 393);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Nhập - Xuất - Tồn ";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // simpleButton9
            // 
           // this.simpleButton9.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton9.ImageOptions.Image")));
            this.simpleButton9.Location = new System.Drawing.Point(109, 44);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(75, 23);
            this.simpleButton9.TabIndex = 10;
            this.simpleButton9.Text = "Xuất";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.gridControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(3, 74);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1051, 316);
            this.panel2.TabIndex = 9;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.advBandedGridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1051, 316);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridView1});
            // 
            // advBandedGridView1
            // 
            this.advBandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand2,
            this.gridBand3,
            this.gridBand4,
            this.gridBand5});
            this.advBandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.Mã,
            this.bandedGridColumn2,
            this.bandedGridColumn3,
            this.bandedGridColumn4,
            this.bandedGridColumn5,
            this.bandedGridColumn6,
            this.bandedGridColumn7,
            this.bandedGridColumn8,
            this.bandedGridColumn9,
            this.bandedGridColumn10,
            this.bandedGridColumn11,
            this.bandedGridColumn12,
            this.bandedGridColumn13});
            this.advBandedGridView1.GridControl = this.gridControl1;
            this.advBandedGridView1.Name = "advBandedGridView1";
            // 
            // gridBand1
            // 
            this.gridBand1.Columns.Add(this.Mã);
            this.gridBand1.Columns.Add(this.bandedGridColumn2);
            this.gridBand1.Columns.Add(this.bandedGridColumn3);
            this.gridBand1.Columns.Add(this.bandedGridColumn4);
            this.gridBand1.Columns.Add(this.bandedGridColumn5);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 462;
            // 
            // Mã
            // 
            this.Mã.Caption = "Mã";
            this.Mã.Name = "Mã";
            this.Mã.Visible = true;
            this.Mã.Width = 57;
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.Caption = "Hàng Hóa";
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            this.bandedGridColumn2.Visible = true;
            this.bandedGridColumn2.Width = 165;
            // 
            // bandedGridColumn3
            // 
            this.bandedGridColumn3.Caption = "Đơn Vị ";
            this.bandedGridColumn3.Name = "bandedGridColumn3";
            this.bandedGridColumn3.Visible = true;
            this.bandedGridColumn3.Width = 70;
            // 
            // bandedGridColumn4
            // 
            this.bandedGridColumn4.Caption = "Kho Hàng";
            this.bandedGridColumn4.Name = "bandedGridColumn4";
            this.bandedGridColumn4.Visible = true;
            this.bandedGridColumn4.Width = 95;
            // 
            // bandedGridColumn5
            // 
            this.bandedGridColumn5.Caption = "Còn Quản Lý";
            this.bandedGridColumn5.Name = "bandedGridColumn5";
            this.bandedGridColumn5.Visible = true;
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "                Đầu Kỳ";
            this.gridBand2.Columns.Add(this.bandedGridColumn6);
            this.gridBand2.Columns.Add(this.bandedGridColumn7);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 1;
            this.gridBand2.Width = 158;
            // 
            // bandedGridColumn6
            // 
            this.bandedGridColumn6.Caption = "Số Lượng ";
            this.bandedGridColumn6.Name = "bandedGridColumn6";
            this.bandedGridColumn6.Visible = true;
            this.bandedGridColumn6.Width = 79;
            // 
            // bandedGridColumn7
            // 
            this.bandedGridColumn7.Caption = "Thành Tiền ";
            this.bandedGridColumn7.Name = "bandedGridColumn7";
            this.bandedGridColumn7.Visible = true;
            this.bandedGridColumn7.Width = 79;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "              Nhập Kho ";
            this.gridBand3.Columns.Add(this.bandedGridColumn8);
            this.gridBand3.Columns.Add(this.bandedGridColumn9);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 2;
            this.gridBand3.Width = 159;
            // 
            // bandedGridColumn8
            // 
            this.bandedGridColumn8.Caption = "Số Lượng ";
            this.bandedGridColumn8.Name = "bandedGridColumn8";
            this.bandedGridColumn8.Visible = true;
            this.bandedGridColumn8.Width = 78;
            // 
            // bandedGridColumn9
            // 
            this.bandedGridColumn9.Caption = "Thành Tiền ";
            this.bandedGridColumn9.Name = "bandedGridColumn9";
            this.bandedGridColumn9.Visible = true;
            this.bandedGridColumn9.Width = 81;
            // 
            // gridBand4
            // 
            this.gridBand4.Caption = "            Xuất Kho ";
            this.gridBand4.Columns.Add(this.bandedGridColumn10);
            this.gridBand4.Columns.Add(this.bandedGridColumn11);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 3;
            this.gridBand4.Width = 157;
            // 
            // bandedGridColumn10
            // 
            this.bandedGridColumn10.Caption = "Số Lượng ";
            this.bandedGridColumn10.Name = "bandedGridColumn10";
            this.bandedGridColumn10.Visible = true;
            this.bandedGridColumn10.Width = 79;
            // 
            // bandedGridColumn11
            // 
            this.bandedGridColumn11.Caption = "Thành Tiền ";
            this.bandedGridColumn11.Name = "bandedGridColumn11";
            this.bandedGridColumn11.Visible = true;
            this.bandedGridColumn11.Width = 78;
            // 
            // gridBand5
            // 
            this.gridBand5.Caption = "             Cuối Kỳ ";
            this.gridBand5.Columns.Add(this.bandedGridColumn12);
            this.gridBand5.Columns.Add(this.bandedGridColumn13);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 4;
            this.gridBand5.Width = 163;
            // 
            // bandedGridColumn12
            // 
            this.bandedGridColumn12.Caption = "Số Lượng ";
            this.bandedGridColumn12.Name = "bandedGridColumn12";
            this.bandedGridColumn12.Visible = true;
            this.bandedGridColumn12.Width = 81;
            // 
            // bandedGridColumn13
            // 
            this.bandedGridColumn13.Caption = "Thành Tiền ";
            this.bandedGridColumn13.Name = "bandedGridColumn13";
            this.bandedGridColumn13.Visible = true;
            this.bandedGridColumn13.Width = 82;
            // 
            // simpleButton8
            // 
           // this.simpleButton8.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton8.ImageOptions.Image")));
            this.simpleButton8.Location = new System.Drawing.Point(226, 44);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(75, 23);
            this.simpleButton8.TabIndex = 8;
            this.simpleButton8.Text = "In";
            // 
            // simpleButton7
            // 
           // this.simpleButton7.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton7.ImageOptions.Image")));
            this.simpleButton7.Location = new System.Drawing.Point(339, 44);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(75, 23);
            this.simpleButton7.TabIndex = 7;
            this.simpleButton7.Text = "Đóng";
            // 
            // simpleButton6
            // 
           // this.simpleButton6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton6.ImageOptions.Image")));
            this.simpleButton6.Location = new System.Drawing.Point(3, 44);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(75, 23);
            this.simpleButton6.TabIndex = 6;
            this.simpleButton6.Text = "Xem";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(387, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Đến";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(255, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Từ";
            // 
            // lookUpEdit2
            // 
            this.lookUpEdit2.Location = new System.Drawing.Point(2, 6);
            this.lookUpEdit2.Name = "lookUpEdit2";
            this.lookUpEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit2.Properties.NullText = "( Tất cả )";
            this.lookUpEdit2.Size = new System.Drawing.Size(122, 20);
            this.lookUpEdit2.TabIndex = 0;
            // 
            // lookUpEdit3
            // 
            this.lookUpEdit3.Location = new System.Drawing.Point(141, 6);
            this.lookUpEdit3.Name = "lookUpEdit3";
            this.lookUpEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit3.Properties.NullText = "Tháng này";
            this.lookUpEdit3.Size = new System.Drawing.Size(100, 20);
            this.lookUpEdit3.TabIndex = 1;
            // 
            // lookUpEdit5
            // 
            this.lookUpEdit5.EditValue = null;
            this.lookUpEdit5.Location = new System.Drawing.Point(281, 6);
            this.lookUpEdit5.Name = "lookUpEdit5";
            this.lookUpEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit5.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit5.Properties.DisplayFormat.FormatString = "";
            this.lookUpEdit5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.lookUpEdit5.Properties.EditFormat.FormatString = "";
            this.lookUpEdit5.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.lookUpEdit5.Properties.Mask.EditMask = "";
            this.lookUpEdit5.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.lookUpEdit5.Properties.NullText = "01/06/2017";
            this.lookUpEdit5.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.lookUpEdit5.Size = new System.Drawing.Size(100, 20);
            this.lookUpEdit5.TabIndex = 3;
            // 
            // lookUpEdit4
            // 
            this.lookUpEdit4.EditValue = null;
            this.lookUpEdit4.Location = new System.Drawing.Point(420, 6);
            this.lookUpEdit4.Name = "lookUpEdit4";
            this.lookUpEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit4.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit4.Properties.DisplayFormat.FormatString = "";
            this.lookUpEdit4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.lookUpEdit4.Properties.EditFormat.FormatString = "";
            this.lookUpEdit4.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.lookUpEdit4.Properties.Mask.EditMask = "";
            this.lookUpEdit4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.lookUpEdit4.Properties.NullText = "30/06/2017";
            this.lookUpEdit4.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.lookUpEdit4.Size = new System.Drawing.Size(100, 20);
            this.lookUpEdit4.TabIndex = 2;
            // 
            // tpTheKho
            // 
            this.tpTheKho.Controls.Add(this.gridControl2);
            this.tpTheKho.Controls.Add(this.simpleButton13);
            this.tpTheKho.Controls.Add(this.simpleButton12);
            this.tpTheKho.Controls.Add(this.simpleButton11);
            this.tpTheKho.Controls.Add(this.simpleButton10);
            this.tpTheKho.Controls.Add(this.label5);
            this.tpTheKho.Controls.Add(this.label4);
            this.tpTheKho.Controls.Add(this.lookUpEdit7);
            this.tpTheKho.Controls.Add(this.lkeTheKho);
            this.tpTheKho.Controls.Add(this.lkeMaHang);
            this.tpTheKho.Controls.Add(this.lookUpEdit6);
            this.tpTheKho.Controls.Add(this.lookUpEdit8);
            this.tpTheKho.Controls.Add(this.lookUpEdit9);
            this.tpTheKho.Location = new System.Drawing.Point(4, 22);
            this.tpTheKho.Name = "tpTheKho";
            this.tpTheKho.Padding = new System.Windows.Forms.Padding(3);
            this.tpTheKho.Size = new System.Drawing.Size(1057, 393);
            this.tpTheKho.TabIndex = 2;
            this.tpTheKho.Text = "Thẻ Kho";
            this.tpTheKho.UseVisualStyleBackColor = true;
            // 
            // gridControl2
            // 
            this.gridControl2.Location = new System.Drawing.Point(6, 92);
            this.gridControl2.MainView = this.advBandedGridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(909, 297);
            this.gridControl2.TabIndex = 13;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridView2,
            this.gridView1});
            // 
            // advBandedGridView2
            // 
            this.advBandedGridView2.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand8,
            this.gridBand10,
            this.gridBand11,
            this.gridBand12});
            this.advBandedGridView2.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn1,
            this.bandedGridColumn14,
            this.bandedGridColumn15,
            this.bandedGridColumn16,
            this.bandedGridColumn17,
            this.bandedGridColumn18,
            this.bandedGridColumn19,
            this.bandedGridColumn20,
            this.bandedGridColumn21,
            this.bandedGridColumn22});
            this.advBandedGridView2.GridControl = this.gridControl2;
            this.advBandedGridView2.Name = "advBandedGridView2";
            // 
            // gridBand8
            // 
            this.gridBand8.Columns.Add(this.bandedGridColumn1);
            this.gridBand8.Columns.Add(this.bandedGridColumn14);
            this.gridBand8.Columns.Add(this.bandedGridColumn15);
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 0;
            this.gridBand8.Width = 267;
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.Caption = "   Mã Hàng ";
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            this.bandedGridColumn1.Visible = true;
            this.bandedGridColumn1.Width = 69;
            // 
            // bandedGridColumn14
            // 
            this.bandedGridColumn14.Caption = "   Hàng Hóa";
            this.bandedGridColumn14.Name = "bandedGridColumn14";
            this.bandedGridColumn14.Visible = true;
            this.bandedGridColumn14.Width = 105;
            // 
            // bandedGridColumn15
            // 
            this.bandedGridColumn15.Caption = "  Ngày Tháng ";
            this.bandedGridColumn15.Name = "bandedGridColumn15";
            this.bandedGridColumn15.Visible = true;
            this.bandedGridColumn15.Width = 93;
            // 
            // gridBand10
            // 
            this.gridBand10.Caption = "                        Chứng Từ";
            this.gridBand10.Columns.Add(this.bandedGridColumn16);
            this.gridBand10.Columns.Add(this.bandedGridColumn17);
            this.gridBand10.Columns.Add(this.bandedGridColumn22);
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.VisibleIndex = 1;
            this.gridBand10.Width = 257;
            // 
            // bandedGridColumn16
            // 
            this.bandedGridColumn16.Caption = "      Nhập ";
            this.bandedGridColumn16.Name = "bandedGridColumn16";
            this.bandedGridColumn16.Visible = true;
            this.bandedGridColumn16.Width = 90;
            // 
            // bandedGridColumn17
            // 
            this.bandedGridColumn17.Caption = "      Xuất";
            this.bandedGridColumn17.Name = "bandedGridColumn17";
            this.bandedGridColumn17.Visible = true;
            this.bandedGridColumn17.Width = 92;
            // 
            // bandedGridColumn22
            // 
            this.bandedGridColumn22.Caption = "      Loại";
            this.bandedGridColumn22.Name = "bandedGridColumn22";
            this.bandedGridColumn22.Visible = true;
            // 
            // gridBand11
            // 
            this.gridBand11.Columns.Add(this.bandedGridColumn18);
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.VisibleIndex = 2;
            this.gridBand11.Width = 134;
            // 
            // bandedGridColumn18
            // 
            this.bandedGridColumn18.Caption = "                Diễn Giải";
            this.bandedGridColumn18.Name = "bandedGridColumn18";
            this.bandedGridColumn18.Visible = true;
            this.bandedGridColumn18.Width = 134;
            // 
            // gridBand12
            // 
            this.gridBand12.Caption = "                         Số Lượng ";
            this.gridBand12.Columns.Add(this.bandedGridColumn19);
            this.gridBand12.Columns.Add(this.bandedGridColumn20);
            this.gridBand12.Columns.Add(this.bandedGridColumn21);
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.VisibleIndex = 3;
            this.gridBand12.Width = 232;
            // 
            // bandedGridColumn19
            // 
            this.bandedGridColumn19.Caption = "      Nhập ";
            this.bandedGridColumn19.Name = "bandedGridColumn19";
            this.bandedGridColumn19.Visible = true;
            this.bandedGridColumn19.Width = 77;
            // 
            // bandedGridColumn20
            // 
            this.bandedGridColumn20.Caption = "     Xuất";
            this.bandedGridColumn20.Name = "bandedGridColumn20";
            this.bandedGridColumn20.Visible = true;
            this.bandedGridColumn20.Width = 77;
            // 
            // bandedGridColumn21
            // 
            this.bandedGridColumn21.Caption = "     Tồn";
            this.bandedGridColumn21.Name = "bandedGridColumn21";
            this.bandedGridColumn21.Visible = true;
            this.bandedGridColumn21.Width = 78;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl2;
            this.gridView1.Name = "gridView1";
            // 
            // simpleButton13
            // 
           // this.simpleButton13.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton13.ImageOptions.Image")));
            this.simpleButton13.Location = new System.Drawing.Point(112, 46);
            this.simpleButton13.Name = "simpleButton13";
            this.simpleButton13.Size = new System.Drawing.Size(75, 23);
            this.simpleButton13.TabIndex = 12;
            this.simpleButton13.Text = "Xuất";
            // 
            // simpleButton12
            // 
           // this.simpleButton12.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton12.ImageOptions.Image")));
            this.simpleButton12.Location = new System.Drawing.Point(218, 46);
            this.simpleButton12.Name = "simpleButton12";
            this.simpleButton12.Size = new System.Drawing.Size(75, 23);
            this.simpleButton12.TabIndex = 11;
            this.simpleButton12.Text = "In";
            // 
            // simpleButton11
            // 
          //  this.simpleButton11.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton11.ImageOptions.Image")));
            this.simpleButton11.Location = new System.Drawing.Point(324, 46);
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.Size = new System.Drawing.Size(75, 23);
            this.simpleButton11.TabIndex = 10;
            this.simpleButton11.Text = "Đóng";
            // 
            // simpleButton10
            // 
          //  this.simpleButton10.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton10.ImageOptions.Image")));
            this.simpleButton10.Location = new System.Drawing.Point(2, 46);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(75, 23);
            this.simpleButton10.TabIndex = 9;
            this.simpleButton10.Text = "Xem";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(562, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Đến";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(430, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Từ";
            // 
            // lookUpEdit7
            // 
            this.lookUpEdit7.Location = new System.Drawing.Point(324, 6);
            this.lookUpEdit7.Name = "lookUpEdit7";
            this.lookUpEdit7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit7.Properties.NullText = "Tháng này";
            this.lookUpEdit7.Size = new System.Drawing.Size(100, 20);
            this.lookUpEdit7.TabIndex = 3;
            // 
            // lkeTheKho
            // 
            this.lkeTheKho.Location = new System.Drawing.Point(6, 6);
            this.lkeTheKho.Name = "lkeTheKho";
            this.lkeTheKho.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkeTheKho.Properties.NullText = "( Tất cả )";
            this.lkeTheKho.Size = new System.Drawing.Size(100, 20);
            this.lkeTheKho.TabIndex = 0;
            // 
            // lkeMaHang
            // 
            this.lkeMaHang.EditValue = "Mã Hàng ";
            this.lkeMaHang.Location = new System.Drawing.Point(112, 6);
            this.lkeMaHang.Name = "lkeMaHang";
            this.lkeMaHang.Properties.NullText = "[EditValue is null]";
            this.lkeMaHang.Size = new System.Drawing.Size(100, 20);
            this.lkeMaHang.TabIndex = 1;
            // 
            // lookUpEdit6
            // 
            this.lookUpEdit6.EditValue = "Tên Hàng ";
            this.lookUpEdit6.Location = new System.Drawing.Point(218, 6);
            this.lookUpEdit6.Name = "lookUpEdit6";
            this.lookUpEdit6.Properties.NullText = "[EditValue is null]";
            this.lookUpEdit6.Size = new System.Drawing.Size(103, 20);
            this.lookUpEdit6.TabIndex = 6;
            // 
            // lookUpEdit8
            // 
            this.lookUpEdit8.EditValue = null;
            this.lookUpEdit8.Location = new System.Drawing.Point(456, 6);
            this.lookUpEdit8.Name = "lookUpEdit8";
            this.lookUpEdit8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit8.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit8.Properties.DisplayFormat.FormatString = "";
            this.lookUpEdit8.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.lookUpEdit8.Properties.EditFormat.FormatString = "";
            this.lookUpEdit8.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.lookUpEdit8.Properties.Mask.EditMask = "";
            this.lookUpEdit8.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.lookUpEdit8.Properties.NullText = "01/06/2017";
            this.lookUpEdit8.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.lookUpEdit8.Size = new System.Drawing.Size(100, 20);
            this.lookUpEdit8.TabIndex = 7;
            // 
            // lookUpEdit9
            // 
            this.lookUpEdit9.EditValue = null;
            this.lookUpEdit9.Location = new System.Drawing.Point(595, 6);
            this.lookUpEdit9.Name = "lookUpEdit9";
            this.lookUpEdit9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit9.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit9.Properties.DisplayFormat.FormatString = "";
            this.lookUpEdit9.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.lookUpEdit9.Properties.EditFormat.FormatString = "";
            this.lookUpEdit9.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.lookUpEdit9.Properties.Mask.EditMask = "";
            this.lookUpEdit9.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.lookUpEdit9.Properties.NullText = "30/06/2017";
            this.lookUpEdit9.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.lookUpEdit9.Size = new System.Drawing.Size(100, 20);
            this.lookUpEdit9.TabIndex = 8;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.gridControl3);
            this.tabPage4.Controls.Add(this.simpleButton18);
            this.tabPage4.Controls.Add(this.simpleButton17);
            this.tabPage4.Controls.Add(this.simpleButton16);
            this.tabPage4.Controls.Add(this.simpleButton14);
            this.tabPage4.Controls.Add(this.label7);
            this.tabPage4.Controls.Add(this.label6);
            this.tabPage4.Controls.Add(this.lookUpEdit13);
            this.tabPage4.Controls.Add(this.lookUpEdit10);
            this.tabPage4.Controls.Add(this.lookUpEdit12);
            this.tabPage4.Controls.Add(this.lookUpEdit11);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1057, 393);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Sổ Chi Tiết Hàng Hóa";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // gridControl3
            // 
            this.gridControl3.Location = new System.Drawing.Point(3, 91);
            this.gridControl3.MainView = this.advBandedGridView3;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(769, 298);
            this.gridControl3.TabIndex = 11;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridView3,
            this.gridView2});
            // 
            // advBandedGridView3
            // 
            this.advBandedGridView3.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand14,
            this.gridBand15,
            this.gridBand16,
            this.gridBand17,
            this.gridBand13});
            this.advBandedGridView3.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn24,
            this.bandedGridColumn25,
            this.bandedGridColumn26,
            this.bandedGridColumn27,
            this.bandedGridColumn28,
            this.bandedGridColumn29,
            this.bandedGridColumn30,
            this.bandedGridColumn31,
            this.bandedGridColumn32,
            this.bandedGridColumn33,
            this.bandedGridColumn34,
            this.bandedGridColumn35,
            this.bandedGridColumn36,
            this.bandedGridColumn37,
            this.bandedGridColumn38,
            this.bandedGridColumn40,
            this.bandedGridColumn41});
            this.advBandedGridView3.GridControl = this.gridControl3;
            this.advBandedGridView3.Name = "advBandedGridView3";
            // 
            // gridBand14
            // 
            this.gridBand14.Columns.Add(this.bandedGridColumn25);
            this.gridBand14.Columns.Add(this.bandedGridColumn26);
            this.gridBand14.Columns.Add(this.bandedGridColumn27);
            this.gridBand14.Columns.Add(this.bandedGridColumn28);
            this.gridBand14.Columns.Add(this.bandedGridColumn29);
            this.gridBand14.Columns.Add(this.bandedGridColumn30);
            this.gridBand14.Columns.Add(this.bandedGridColumn31);
            this.gridBand14.Columns.Add(this.bandedGridColumn32);
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.VisibleIndex = 0;
            this.gridBand14.Width = 590;
            // 
            // bandedGridColumn25
            // 
            this.bandedGridColumn25.Caption = "   Thứ Tự ";
            this.bandedGridColumn25.Name = "bandedGridColumn25";
            this.bandedGridColumn25.Visible = true;
            this.bandedGridColumn25.Width = 70;
            // 
            // bandedGridColumn26
            // 
            this.bandedGridColumn26.Caption = "   Chứng Từ ";
            this.bandedGridColumn26.Name = "bandedGridColumn26";
            this.bandedGridColumn26.Visible = true;
            this.bandedGridColumn26.Width = 70;
            // 
            // bandedGridColumn27
            // 
            this.bandedGridColumn27.Caption = "      Ngày ";
            this.bandedGridColumn27.Name = "bandedGridColumn27";
            this.bandedGridColumn27.Visible = true;
            this.bandedGridColumn27.Width = 70;
            // 
            // bandedGridColumn28
            // 
            this.bandedGridColumn28.Caption = "   Diễn Giải";
            this.bandedGridColumn28.Name = "bandedGridColumn28";
            this.bandedGridColumn28.Visible = true;
            this.bandedGridColumn28.Width = 70;
            // 
            // bandedGridColumn29
            // 
            this.bandedGridColumn29.Caption = "   Mã Hàng ";
            this.bandedGridColumn29.Name = "bandedGridColumn29";
            this.bandedGridColumn29.Visible = true;
            this.bandedGridColumn29.Width = 70;
            // 
            // bandedGridColumn30
            // 
            this.bandedGridColumn30.Caption = "   Hàng Hóa ";
            this.bandedGridColumn30.Name = "bandedGridColumn30";
            this.bandedGridColumn30.Visible = true;
            this.bandedGridColumn30.Width = 70;
            // 
            // bandedGridColumn31
            // 
            this.bandedGridColumn31.Caption = "   Đơn Vị ";
            this.bandedGridColumn31.Name = "bandedGridColumn31";
            this.bandedGridColumn31.Visible = true;
            this.bandedGridColumn31.Width = 70;
            // 
            // bandedGridColumn32
            // 
            this.bandedGridColumn32.Caption = "    Đơn Giá ";
            this.bandedGridColumn32.Name = "bandedGridColumn32";
            this.bandedGridColumn32.Visible = true;
            this.bandedGridColumn32.Width = 100;
            // 
            // gridBand15
            // 
            this.gridBand15.Caption = "          Tồn Đầu ";
            this.gridBand15.Columns.Add(this.bandedGridColumn33);
            this.gridBand15.Columns.Add(this.bandedGridColumn34);
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.VisibleIndex = 1;
            this.gridBand15.Width = 150;
            // 
            // bandedGridColumn33
            // 
            this.bandedGridColumn33.Caption = "   Số Lượng ";
            this.bandedGridColumn33.Name = "bandedGridColumn33";
            this.bandedGridColumn33.Visible = true;
            // 
            // bandedGridColumn34
            // 
            this.bandedGridColumn34.Caption = "   Thành Tiền ";
            this.bandedGridColumn34.Name = "bandedGridColumn34";
            this.bandedGridColumn34.Visible = true;
            // 
            // gridBand16
            // 
            this.gridBand16.Caption = "      Nhập Hàng ";
            this.gridBand16.Columns.Add(this.bandedGridColumn35);
            this.gridBand16.Columns.Add(this.bandedGridColumn36);
            this.gridBand16.Name = "gridBand16";
            this.gridBand16.VisibleIndex = 2;
            this.gridBand16.Width = 160;
            // 
            // bandedGridColumn35
            // 
            this.bandedGridColumn35.Caption = "   Số Lượng ";
            this.bandedGridColumn35.Name = "bandedGridColumn35";
            this.bandedGridColumn35.Visible = true;
            this.bandedGridColumn35.Width = 77;
            // 
            // bandedGridColumn36
            // 
            this.bandedGridColumn36.Caption = "   Thành Tiền ";
            this.bandedGridColumn36.Name = "bandedGridColumn36";
            this.bandedGridColumn36.Visible = true;
            this.bandedGridColumn36.Width = 83;
            // 
            // gridBand17
            // 
            this.gridBand17.Caption = "        Xuất Hàng ";
            this.gridBand17.Columns.Add(this.bandedGridColumn37);
            this.gridBand17.Columns.Add(this.bandedGridColumn38);
            this.gridBand17.Name = "gridBand17";
            this.gridBand17.VisibleIndex = 3;
            this.gridBand17.Width = 168;
            // 
            // bandedGridColumn37
            // 
            this.bandedGridColumn37.Caption = "   Số Lượng ";
            this.bandedGridColumn37.Name = "bandedGridColumn37";
            this.bandedGridColumn37.Visible = true;
            this.bandedGridColumn37.Width = 83;
            // 
            // bandedGridColumn38
            // 
            this.bandedGridColumn38.Caption = "   Thành Tiền ";
            this.bandedGridColumn38.Name = "bandedGridColumn38";
            this.bandedGridColumn38.Visible = true;
            this.bandedGridColumn38.Width = 85;
            // 
            // gridBand13
            // 
            this.gridBand13.Caption = "           Tồn Cuối";
            this.gridBand13.Columns.Add(this.bandedGridColumn40);
            this.gridBand13.Columns.Add(this.bandedGridColumn41);
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 4;
            this.gridBand13.Width = 150;
            // 
            // bandedGridColumn40
            // 
            this.bandedGridColumn40.Caption = "   Số Lượng ";
            this.bandedGridColumn40.Name = "bandedGridColumn40";
            this.bandedGridColumn40.Visible = true;
            // 
            // bandedGridColumn41
            // 
            this.bandedGridColumn41.Caption = "   Thành Tiền ";
            this.bandedGridColumn41.Name = "bandedGridColumn41";
            this.bandedGridColumn41.Visible = true;
            // 
            // bandedGridColumn24
            // 
            this.bandedGridColumn24.Name = "bandedGridColumn24";
            this.bandedGridColumn24.Visible = true;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridControl3;
            this.gridView2.Name = "gridView2";
            // 
            // simpleButton18
            // 
           // this.simpleButton18.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton18.ImageOptions.Image")));
            this.simpleButton18.Location = new System.Drawing.Point(134, 44);
            this.simpleButton18.Name = "simpleButton18";
            this.simpleButton18.Size = new System.Drawing.Size(89, 28);
            this.simpleButton18.TabIndex = 10;
            this.simpleButton18.Text = "Xuất";
            // 
            // simpleButton17
            // 
           // this.simpleButton17.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton17.ImageOptions.Image")));
            this.simpleButton17.Location = new System.Drawing.Point(276, 44);
            this.simpleButton17.Name = "simpleButton17";
            this.simpleButton17.Size = new System.Drawing.Size(89, 28);
            this.simpleButton17.TabIndex = 9;
            this.simpleButton17.Text = "In";
            // 
            // simpleButton16
            // 
           // this.simpleButton16.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton16.ImageOptions.Image")));
            this.simpleButton16.Location = new System.Drawing.Point(395, 44);
            this.simpleButton16.Name = "simpleButton16";
            this.simpleButton16.Size = new System.Drawing.Size(89, 28);
            this.simpleButton16.TabIndex = 8;
            this.simpleButton16.Text = "Đóng";
            // 
            // simpleButton14
            // 
           // this.simpleButton14.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton14.ImageOptions.Image")));
            this.simpleButton14.Location = new System.Drawing.Point(6, 44);
            this.simpleButton14.Name = "simpleButton14";
            this.simpleButton14.Size = new System.Drawing.Size(89, 28);
            this.simpleButton14.TabIndex = 6;
            this.simpleButton14.Text = "Xem";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(392, 13);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Đến ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(250, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Từ";
            // 
            // lookUpEdit13
            // 
            this.lookUpEdit13.Location = new System.Drawing.Point(134, 6);
            this.lookUpEdit13.Name = "lookUpEdit13";
            this.lookUpEdit13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit13.Properties.NullText = "Tháng này";
            this.lookUpEdit13.Size = new System.Drawing.Size(100, 20);
            this.lookUpEdit13.TabIndex = 3;
            // 
            // lookUpEdit10
            // 
            this.lookUpEdit10.Location = new System.Drawing.Point(6, 6);
            this.lookUpEdit10.Name = "lookUpEdit10";
            this.lookUpEdit10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit10.Properties.NullText = "Kho công ty ";
            this.lookUpEdit10.Size = new System.Drawing.Size(111, 20);
            this.lookUpEdit10.TabIndex = 0;
            // 
            // lookUpEdit12
            // 
            this.lookUpEdit12.EditValue = null;
            this.lookUpEdit12.Location = new System.Drawing.Point(276, 6);
            this.lookUpEdit12.Name = "lookUpEdit12";
            this.lookUpEdit12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit12.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit12.Properties.DisplayFormat.FormatString = "";
            this.lookUpEdit12.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.lookUpEdit12.Properties.EditFormat.FormatString = "";
            this.lookUpEdit12.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.lookUpEdit12.Properties.Mask.EditMask = "";
            this.lookUpEdit12.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.lookUpEdit12.Properties.NullText = "01/06/2017";
            this.lookUpEdit12.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.lookUpEdit12.Size = new System.Drawing.Size(100, 20);
            this.lookUpEdit12.TabIndex = 2;
            // 
            // lookUpEdit11
            // 
            this.lookUpEdit11.EditValue = null;
            this.lookUpEdit11.Location = new System.Drawing.Point(428, 6);
            this.lookUpEdit11.Name = "lookUpEdit11";
            this.lookUpEdit11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit11.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit11.Properties.DisplayFormat.FormatString = "";
            this.lookUpEdit11.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.lookUpEdit11.Properties.EditFormat.FormatString = "";
            this.lookUpEdit11.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.lookUpEdit11.Properties.Mask.EditMask = "";
            this.lookUpEdit11.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.lookUpEdit11.Properties.NullText = "30/06/2017";
            this.lookUpEdit11.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.lookUpEdit11.Size = new System.Drawing.Size(100, 20);
            this.lookUpEdit11.TabIndex = 1;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.gridControl4);
            this.tabPage5.Controls.Add(this.simpleButton19);
            this.tabPage5.Controls.Add(this.simpleButton15);
            this.tabPage5.Controls.Add(this.lookUpEdit14);
            this.tabPage5.Controls.Add(this.lookUpEdit15);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1057, 393);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Lịch Sử Hàng Hóa";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // gridControl4
            // 
            this.gridControl4.Location = new System.Drawing.Point(6, 42);
            this.gridControl4.MainView = this.advBandedGridView4;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.Size = new System.Drawing.Size(1048, 345);
            this.gridControl4.TabIndex = 4;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridView4,
            this.gridView3});
            // 
            // advBandedGridView4
            // 
            this.advBandedGridView4.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand19,
            this.gridBand20,
            this.gridBand21,
            this.gridBand22});
            this.advBandedGridView4.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn39,
            this.bandedGridColumn42,
            this.bandedGridColumn43,
            this.bandedGridColumn44,
            this.bandedGridColumn45,
            this.bandedGridColumn46,
            this.bandedGridColumn47,
            this.bandedGridColumn48,
            this.bandedGridColumn49,
            this.bandedGridColumn50,
            this.bandedGridColumn51,
            this.bandedGridColumn52,
            this.bandedGridColumn53,
            this.bandedGridColumn54,
            this.bandedGridColumn55});
            this.advBandedGridView4.GridControl = this.gridControl4;
            this.advBandedGridView4.Name = "advBandedGridView4";
            // 
            // gridBand19
            // 
            this.gridBand19.Columns.Add(this.bandedGridColumn39);
            this.gridBand19.Columns.Add(this.bandedGridColumn42);
            this.gridBand19.Columns.Add(this.bandedGridColumn43);
            this.gridBand19.Columns.Add(this.bandedGridColumn44);
            this.gridBand19.Columns.Add(this.bandedGridColumn45);
            this.gridBand19.Columns.Add(this.bandedGridColumn46);
            this.gridBand19.Columns.Add(this.bandedGridColumn47);
            this.gridBand19.Columns.Add(this.bandedGridColumn48);
            this.gridBand19.Columns.Add(this.bandedGridColumn49);
            this.gridBand19.Columns.Add(this.bandedGridColumn50);
            this.gridBand19.Name = "gridBand19";
            this.gridBand19.VisibleIndex = 0;
            this.gridBand19.Width = 976;
            // 
            // bandedGridColumn39
            // 
            this.bandedGridColumn39.Caption = "      Ngày";
            this.bandedGridColumn39.Name = "bandedGridColumn39";
            this.bandedGridColumn39.Visible = true;
            this.bandedGridColumn39.Width = 84;
            // 
            // bandedGridColumn42
            // 
            this.bandedGridColumn42.Caption = "   Chứng từ ";
            this.bandedGridColumn42.Name = "bandedGridColumn42";
            this.bandedGridColumn42.Visible = true;
            this.bandedGridColumn42.Width = 84;
            // 
            // bandedGridColumn43
            // 
            this.bandedGridColumn43.Caption = "       Loại ";
            this.bandedGridColumn43.Name = "bandedGridColumn43";
            this.bandedGridColumn43.Visible = true;
            this.bandedGridColumn43.Width = 101;
            // 
            // bandedGridColumn44
            // 
            this.bandedGridColumn44.Caption = "      Kho hàng ";
            this.bandedGridColumn44.Name = "bandedGridColumn44";
            this.bandedGridColumn44.Visible = true;
            this.bandedGridColumn44.Width = 162;
            // 
            // bandedGridColumn45
            // 
            this.bandedGridColumn45.Caption = "     Mã hàng";
            this.bandedGridColumn45.Name = "bandedGridColumn45";
            this.bandedGridColumn45.Visible = true;
            this.bandedGridColumn45.Width = 93;
            // 
            // bandedGridColumn46
            // 
            this.bandedGridColumn46.Caption = "     Tên hàng";
            this.bandedGridColumn46.Name = "bandedGridColumn46";
            this.bandedGridColumn46.Visible = true;
            this.bandedGridColumn46.Width = 146;
            // 
            // bandedGridColumn47
            // 
            this.bandedGridColumn47.Caption = "    Đơn vị";
            this.bandedGridColumn47.Name = "bandedGridColumn47";
            this.bandedGridColumn47.Visible = true;
            this.bandedGridColumn47.Width = 63;
            // 
            // bandedGridColumn48
            // 
            this.bandedGridColumn48.Caption = "     Số lượng";
            this.bandedGridColumn48.Name = "bandedGridColumn48";
            this.bandedGridColumn48.Visible = true;
            this.bandedGridColumn48.Width = 77;
            // 
            // bandedGridColumn49
            // 
            this.bandedGridColumn49.Caption = "     Đơn giá";
            this.bandedGridColumn49.Name = "bandedGridColumn49";
            this.bandedGridColumn49.Visible = true;
            this.bandedGridColumn49.Width = 71;
            // 
            // bandedGridColumn50
            // 
            this.bandedGridColumn50.Caption = "     Thành tiền ";
            this.bandedGridColumn50.Name = "bandedGridColumn50";
            this.bandedGridColumn50.Visible = true;
            this.bandedGridColumn50.Width = 95;
            // 
            // gridBand20
            // 
            this.gridBand20.Caption = "                 Bình quân ";
            this.gridBand20.Columns.Add(this.bandedGridColumn51);
            this.gridBand20.Columns.Add(this.bandedGridColumn52);
            this.gridBand20.Name = "gridBand20";
            this.gridBand20.VisibleIndex = 1;
            this.gridBand20.Width = 172;
            // 
            // bandedGridColumn51
            // 
            this.bandedGridColumn51.Caption = "        Đơn giá";
            this.bandedGridColumn51.Name = "bandedGridColumn51";
            this.bandedGridColumn51.Visible = true;
            this.bandedGridColumn51.Width = 84;
            // 
            // bandedGridColumn52
            // 
            this.bandedGridColumn52.Caption = "      Thành tiền ";
            this.bandedGridColumn52.Name = "bandedGridColumn52";
            this.bandedGridColumn52.Visible = true;
            this.bandedGridColumn52.Width = 88;
            // 
            // gridBand21
            // 
            this.gridBand21.Caption = "                                       Tồn cuối ";
            this.gridBand21.Columns.Add(this.bandedGridColumn53);
            this.gridBand21.Columns.Add(this.bandedGridColumn54);
            this.gridBand21.Name = "gridBand21";
            this.gridBand21.VisibleIndex = 2;
            this.gridBand21.Width = 216;
            // 
            // bandedGridColumn53
            // 
            this.bandedGridColumn53.Caption = "        Số lượng ";
            this.bandedGridColumn53.Name = "bandedGridColumn53";
            this.bandedGridColumn53.Visible = true;
            this.bandedGridColumn53.Width = 116;
            // 
            // bandedGridColumn54
            // 
            this.bandedGridColumn54.Caption = "           Thành tiền ";
            this.bandedGridColumn54.Name = "bandedGridColumn54";
            this.bandedGridColumn54.Visible = true;
            this.bandedGridColumn54.Width = 100;
            // 
            // gridBand22
            // 
            this.gridBand22.Columns.Add(this.bandedGridColumn55);
            this.gridBand22.Name = "gridBand22";
            this.gridBand22.VisibleIndex = 3;
            this.gridBand22.Width = 104;
            // 
            // bandedGridColumn55
            // 
            this.bandedGridColumn55.Caption = "    Ghi chú";
            this.bandedGridColumn55.Name = "bandedGridColumn55";
            this.bandedGridColumn55.Visible = true;
            this.bandedGridColumn55.Width = 104;
            // 
            // gridView3
            // 
            this.gridView3.GridControl = this.gridControl4;
            this.gridView3.Name = "gridView3";
            // 
            // simpleButton19
            // 
           // this.simpleButton19.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton19.ImageOptions.Image")));
            this.simpleButton19.Location = new System.Drawing.Point(403, 6);
            this.simpleButton19.Name = "simpleButton19";
            this.simpleButton19.Size = new System.Drawing.Size(97, 20);
            this.simpleButton19.TabIndex = 3;
            this.simpleButton19.Text = "Thoát";
            this.simpleButton19.Click += new System.EventHandler(this.simpleButton19_Click);
            // 
            // simpleButton15
            // 
           // this.simpleButton15.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton15.ImageOptions.Image")));
            this.simpleButton15.Location = new System.Drawing.Point(257, 6);
            this.simpleButton15.Name = "simpleButton15";
            this.simpleButton15.Size = new System.Drawing.Size(95, 20);
            this.simpleButton15.TabIndex = 2;
            this.simpleButton15.Text = "Tìm";
            // 
            // lookUpEdit14
            // 
            this.lookUpEdit14.Location = new System.Drawing.Point(6, 6);
            this.lookUpEdit14.Name = "lookUpEdit14";
            this.lookUpEdit14.Properties.NullText = "Mã Hàng";
            this.lookUpEdit14.Size = new System.Drawing.Size(100, 20);
            this.lookUpEdit14.TabIndex = 0;
            // 
            // lookUpEdit15
            // 
            this.lookUpEdit15.Location = new System.Drawing.Point(121, 6);
            this.lookUpEdit15.Name = "lookUpEdit15";
            this.lookUpEdit15.Properties.NullText = "Hàng Hóa";
            this.lookUpEdit15.Size = new System.Drawing.Size(100, 20);
            this.lookUpEdit15.TabIndex = 1;
            // 
            // bandedGridColumn23
            // 
            this.bandedGridColumn23.Name = "bandedGridColumn23";
            this.bandedGridColumn23.Visible = true;
            // 
            // gridBand6
            // 
            this.gridBand6.Caption = "gridBand6";
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = -1;
            this.gridBand6.Width = 225;
            // 
            // gridBand9
            // 
            this.gridBand9.Caption = "gridBand9";
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.VisibleIndex = -1;
            this.gridBand9.Width = 78;
            // 
            // gridBand7
            // 
            this.gridBand7.Caption = "gridBand7";
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = -1;
            this.gridBand7.Width = 225;
            // 
            // gridBand18
            // 
            this.gridBand18.Caption = "gridBand18";
            this.gridBand18.Name = "gridBand18";
            this.gridBand18.VisibleIndex = -1;
            this.gridBand18.Width = 574;
            // 
            // frmBaoCaoKhoHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1231, 419);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dockPanel2);
            this.Name = "frmBaoCaoKhoHang";
            this.Text = "Báo Cáo Kho Hàng";
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager2)).EndInit();
            this.dockPanel2.ResumeLayout(false);
            this.dockPanel2_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit5.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).EndInit();
            this.tpTheKho.ResumeLayout(false);
            this.tpTheKho.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkeTheKho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkeMaHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit8.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit9.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit9.Properties)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit12.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit11.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit11.Properties)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel2;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel2_Container;
        private DevExpress.XtraNavBar.NavBarControl navBarControl1;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup1;
        private DevExpress.XtraNavBar.NavBarItem navBarItem1;
        private DevExpress.XtraNavBar.NavBarItem navBarItem2;
        private DevExpress.XtraNavBar.NavBarItem navBarItem3;
        private DevExpress.XtraNavBar.NavBarItem navBarItem4;
        private DevExpress.XtraNavBar.NavBarItem navBarItem5;
        private DevExpress.XtraBars.Docking.DockManager dockManager2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private DevExpress.XtraTreeList.TreeList treeList1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tlMaHang;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tlTenHang;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tlDonVi;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tlSoLuong;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tlDonGia;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tlThanhTien;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tlNhomHang;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tlKhoHang;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tlConQuanLy;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advBandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Mã;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn13;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit2;
        private DevExpress.XtraEditors.FontEdit lookUpEdit3;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraEditors.DateEdit lookUpEdit5;
        private DevExpress.XtraEditors.DateEdit lookUpEdit4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn23;
        private System.Windows.Forms.TabPage tpTheKho;
        private DevExpress.XtraEditors.SimpleButton simpleButton12;
        private DevExpress.XtraEditors.SimpleButton simpleButton11;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit7;
        private DevExpress.XtraEditors.LookUpEdit lkeTheKho;
        private DevExpress.XtraEditors.TextEdit lkeMaHang;
        private DevExpress.XtraEditors.TextEdit lookUpEdit6;
        private DevExpress.XtraEditors.DateEdit lookUpEdit8;
        private DevExpress.XtraEditors.DateEdit lookUpEdit9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraEditors.SimpleButton simpleButton13;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advBandedGridView2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn14;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn15;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn16;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn22;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn18;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn19;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn20;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn21;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advBandedGridView3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn25;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn26;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn27;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn28;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn29;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn30;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn31;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn32;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn33;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn34;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand16;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn35;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn36;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn37;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn38;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn40;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn41;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn24;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.SimpleButton simpleButton18;
        private DevExpress.XtraEditors.SimpleButton simpleButton17;
        private DevExpress.XtraEditors.SimpleButton simpleButton16;
        private DevExpress.XtraEditors.SimpleButton simpleButton14;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit13;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit10;
        private DevExpress.XtraEditors.DateEdit lookUpEdit12;
        private DevExpress.XtraEditors.DateEdit lookUpEdit11;
        private DevExpress.XtraEditors.SimpleButton simpleButton19;
        private DevExpress.XtraEditors.SimpleButton simpleButton15;
        private DevExpress.XtraEditors.TextEdit lookUpEdit14;
        private DevExpress.XtraEditors.TextEdit lookUpEdit15;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand18;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advBandedGridView4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand19;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn39;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn42;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn43;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn44;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn45;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn46;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn47;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn48;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn49;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn50;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand20;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn51;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn52;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand21;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn53;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn54;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand22;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn55;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
    }
}