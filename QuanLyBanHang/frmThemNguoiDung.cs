﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmThemNguoiDung : Form
    {
        DACK_ware5Entities1 db = new DACK_ware5Entities1();
        public frmThemNguoiDung()
        {
            InitializeComponent();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmThemNguoiDung_Load(object sender, EventArgs e)
        {
           //var query = from n in db.NhanViens from k in db.BoPhans where k.id_BoPhan == n.id_bophan  select new {k.TenBoPhan, n.id_NhanVien, n.TenNV };
            lueNhanVien.Properties.DataSource = db.NhanViens.ToList();
            lueNhanVien.Properties.DisplayMember = "TenNV";
            lueNhanVien.Properties.ValueMember = "TenNV";

            lueVaiTro.Properties.DataSource = db.VaiTroes.ToList();
            lueVaiTro.Properties.DisplayMember = "TenVaiTro";
            lueVaiTro.Properties.ValueMember = "idVaiTro";
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (txtTaiKhoan.Text != "" && txtMatKhau.Text != "") {
                var user = db.NguoiDungs.FirstOrDefault(a => a.Username.Equals(txtTaiKhoan.Text));
                if (user == null)
                {

                    NguoiDung nd = new NguoiDung();
                    nd.id_VaiTro = int.Parse(lueVaiTro.EditValue.ToString());
                    nd.TenNguoiDung = lueNhanVien.EditValue.ToString();
                    nd.Username = txtTaiKhoan.Text;
                    nd.Password = txtMatKhau.Text;
                    nd.TrangThai = true;
                    if (txtXacNhanMK.Text != txtMatKhau.Text)
                    {
                        MessageBox.Show("Nhập lại mật khẩu");
                    }
                    else
                    {
                        db.NguoiDungs.Add(nd);
                        db.SaveChangesAsync();
                        MessageBox.Show("Đăng ký thành công.");
                    }
                }
                else
                {
                    MessageBox.Show("Tên tài khoản đã được đăng ký.");
                }
            }
        }
    }
}
