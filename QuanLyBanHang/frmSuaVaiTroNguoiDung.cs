﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmSuaVaiTroNguoiDung : Form
    {
        DACK_ware5Entities1 db = new DACK_ware5Entities1();

        public frmSuaVaiTroNguoiDung()
        {
            InitializeComponent();
        }

        private void frmSuaVaiTroNguoiDung_Load(object sender, EventArgs e)
        {
            var cn1 = from n in db.ChucNangs
                      from m in db.VaiTro_ChucNang
                      where n.id_ChucNang == m.id_ChucNang
                      select new { n.TenChucNang, m.Them, m.Xoa, m.Sua, m.Nhap, m.Xuat, m.in_ra };
            trlDanhSachQuyenHan.DataSource = cn1.ToList();
        }
    }
}
