﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmPhucHoi : Form
    {
        private SqlConnection conn;
        private SqlDataReader datareader;
        private SqlCommand command;
        string sql;
        string connectionstring = "";
        public frmPhucHoi()
        {
            InitializeComponent();
        }

        private void btnKetNoi_Click(object sender, EventArgs e)
        {
            try
            {
                connectionstring = "Data Source =" + txtServerName.Text + "; User Id=" + txtID.Text + "; Password=" + txtPW.Text + "";
                conn = new SqlConnection(connectionstring);
                conn.Open();
                //sql = "EXEC QuanLyBanHang";
                sql = "SELECT * FROM sys.databases d WHERE d.database_id>4";
                command = new SqlCommand(sql, conn);
                datareader = command.ExecuteReader();
                cbDatabase.Items.Clear();
                while (datareader.Read())
                {
                    cbDatabase.Items.Add(datareader[0].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            txtServerName.Enabled = false;
            txtID.Enabled = false;
            txtPW.Enabled = false;
        }

        private void btnPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog fdl = new OpenFileDialog();
            fdl.Filter = "Backup files (*.bak)|*.bak| All files(*.*) | *.*";
            if(fdl.ShowDialog() == DialogResult.OK)
            {
                txtPath.Text = fdl.FileName;
            }
        }

        private void btnKhoiPhuc_Click(object sender, EventArgs e)
        {
            try
            {
                if(cbDatabase.Text.CompareTo("") == 0)
                {
                    MessageBox.Show("Hãy chọn cơ sở dữ liệu !");
                    return;
                }
                else
                {
                    conn = new SqlConnection(connectionstring);
                    conn.Open();
                    sql = "ALTER DATABASE " + cbDatabase.Text + " SET SINGLE_USER WITH ROLLBACK IMMEDIATE;";
                    sql += "RESTORE DATABASE " +cbDatabase.Text+ " FROM DISK = '" +txtPath.Text+ "' WITH REPLACE;";
                    command = new SqlCommand(sql, conn);
                    command.ExecuteNonQuery();
                    conn.Close();
                    conn.Dispose();
                    MessageBox.Show("Phục hồi thành công");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void frmPhucHoi_Load(object sender, EventArgs e)
        {

        }
    }
}
