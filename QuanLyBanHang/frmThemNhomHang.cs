﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmThemNhomHang : Form
    {
        DACK_ware5Entities1 db = new DACK_ware5Entities1();

        public frmThemNhomHang()
        {
            InitializeComponent();
        }

        private void frmThemNhomHang_Load(object sender, EventArgs e)
        {
           
        }

        private void btn_Them_Click(object sender, EventArgs e)
        {
            NhomHang nh = new NhomHang();
            nh.TenNhomHang = txt_TenNhomHang.Text;
            if (cbQuanLy.Checked == true)
            {
                nh.TrangThai = true;
            }
            else
            {
                nh.TrangThai = false;
            }
    
            db.NhomHangs.Add(nh);
            db.SaveChanges();
            MessageBox.Show("Thêm thành công!");
            this.Close();

        }
    }
}
