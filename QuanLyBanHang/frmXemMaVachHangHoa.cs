﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmXemMaVachHangHoa : Form
    {

        public frmXemMaVachHangHoa(string idhang, string idmv, string namepro, string namebar, string pric, string donvi)
        {
            InitializeComponent();
            LoadData(idhang, idmv, namepro, namebar, pric, donvi);

        }

        private void LoadData(string idhang, string idmv, string namepro, string namebar, string pric, string donvi)
        {
            txtIDHangHoa.Text = idhang;
            txtMaVach.Text = idmv;
            txtTenSanPham.Text = namepro;
            txtTenMV.Text = namebar;
            txtGia.Text = pric;
            txtDonVi.Text = donvi;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmXemMaVachHangHoa_Load(object sender, EventArgs e)
        {

        }
    }
}
