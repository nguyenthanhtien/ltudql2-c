﻿using DevExpress.XtraEditors.Repository;
using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmNhapSoDuDauKy : Form
    {
        int number;
        DACK_ware5Entities1 db = new DACK_ware5Entities1();
        public frmNhapSoDuDauKy()
        {
            InitializeComponent();
            DanhMuc(false);
            CongNoDauKi(false);
            BangKe(false);
            LoaddataKhachHang();
            LoadDanhSachNCCDauKi();
            LoadDShangHoa();
            LoadDSHHtheoDauKi();
            LoadDanhSachTonKhoDauKi();
            Random rnd = new Random();
            number = rnd.Next(10, 10000);
            RandomCode();
            LoadDSHangHoaKhoNhap();
        }

        private void LoadDSHangHoaKhoNhap()
        {
            grcDanhSachTonKhoDauKi.DataSource = (from n in db.NhapHangTamThois
                                                  from m in db.DonViTinhs
                                                  where m.Id_DV == n.Id_DonVi
                                                  select new { m.TenDonVi, n.ThanhTien, n.MaHang, n.SoLuong, n.TenHang, n.DonGia }).ToList();
        }

        private void RandomCode()
        {
            txtPhieuTonKho.Text = "NSDDK" + number;
        }
        private void LoadDanhSachTonKhoDauKi()
        {
           
        }

        private void LoadDSHHtheoDauKi()
        {
            var query = (from n in db.NhapHangs
                         from m in db.ChiTietNhapHangs
                         from p in db.DonViTinhs
                         from q in db.Khoes
                         from t in db.HangHoas
                         where n.id_ChungTuNhap == m.id_ChungTuNhap && m.id_DonVi == p.Id_DV && q.Id_Kho == n.id_Kho && m.id_HangHoa == t.id_hanghoa
                         select new
                         {
                             n.id_ChungTuNhap,
                             n.NgayNhap,                         
                             n.ThanhToan,                          
                             m.id_HangHoa,                    
                             q.TenKho                       
                        }).ToList();
            grcHHTheoKi.DataSource = query;
        }

        private void LoadDShangHoa()
        {
            var query = (from n in db.NhapHangs
                         from m in db.ChiTietNhapHangs
                         from p in db.DonViTinhs
                         from q in db.Khoes
                         from t in db.HangHoas
                         where n.id_ChungTuNhap == m.id_ChungTuNhap && m.id_DonVi == p.Id_DV && q.Id_Kho == n.id_Kho && m.id_HangHoa == t.id_hanghoa
                         select new
                         {
                             n.id_ChungTuNhap,
                             n.NgayNhap,
                             n.SoTien,
                             n.ThanhToan,
                             m.SoLuong,
                             m.DonGia,
                             m.id_HangHoa,
                             p.TenDonVi,
                             q.TenKho,
                             t.Tenhang
                         }).ToList();
            grcDanhSachTheoHangHoa.DataSource = query;
        }

        private void btnXemTonKhoDauki_Click(object sender, EventArgs e)
        {
            TonKhoDauKi(true);
            DanhMuc(false);
            CongNoDauKi(false);
            BangKe(false);
        }

        private void btnXembangKe_Click(object sender, EventArgs e)
        {
            TonKhoDauKi(false);
            DanhMuc(false);
            CongNoDauKi(false);
            BangKe(true);
        }
        private void DanhMuc(Boolean cohieu)
        {
            if (cohieu == true)
            {
                nvHangHoa.Visible = cohieu;
                nvKhachHangDanhMuc.Visible = cohieu;
                nvNhaCungCapDanhMuc.Visible = cohieu;
                nvFileExlseMau.Visible = cohieu;
                nvTenHienThi.Caption = "Thêm danh mục";

            }
            else {
                nvHangHoa.Visible = cohieu;
                nvKhachHangDanhMuc.Visible = cohieu;
                nvNhaCungCapDanhMuc.Visible = cohieu;
                nvFileExlseMau.Visible = cohieu;

            }
        }
        private void CongNoDauKi(Boolean cohieu)
        { if (cohieu == true)
            {
                nvTenHienThi.Caption = "Công nợ đầu kì";
                nvKhachHang.Visible = cohieu;
                nvNhaCungCap.Visible = cohieu;
            }
            else
            {
                nvKhachHang.Visible = cohieu;
                nvNhaCungCap.Visible = cohieu;
            }
        }
        private void BangKe(Boolean cohieu)
        {
            if (cohieu == true)
            {
                nvTenHienThi.Caption = "Bảng kê";
                nvTheoKi.Visible = cohieu;
                nvTheoHangHoa.Visible = cohieu;
            } else
            {
                nvTheoKi.Visible = cohieu;
                nvTheoHangHoa.Visible = cohieu;
            }
        }
        private void TonKhoDauKi(Boolean cohieu)
        {
            if (cohieu == true)
            {
                nvTenHienThi.Caption = "Tồn kho đầu kì";
                nvTonKho.Visible = cohieu;
            } else
            {
                nvTonKho.Visible = cohieu;
            }
        }

        private void btnXemCongNoDauKi_Click(object sender, EventArgs e)
        {
            TonKhoDauKi(false);
            DanhMuc(false);
            CongNoDauKi(true);
            BangKe(false);
        }

        private void btnThemDanhMuc_Click(object sender, EventArgs e)
        {
            TonKhoDauKi(false);
            DanhMuc(true);
            CongNoDauKi(false);
            BangKe(false);
        }

        private void nvTonKho_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tabNhapSoDuDauKi.SelectTab(0);
            label1.Text = "Nhập kho đầu kì";
        }

        private void nvTheoKi_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tabNhapSoDuDauKi.SelectTab(1);
            label1.Text = "Bảng kê tổng hợp";
        }

        private void nvTheoHangHoa_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tabNhapSoDuDauKi.SelectTab(2);
            label1.Text = "Bảng kê chi tiết";
        }

        private void nvKhachHang_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tabNhapSoDuDauKi.SelectTab(3);
            label1.Text = "Công nợ khách hàng";
        }

        private void nvNhaCungCap_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tabNhapSoDuDauKi.SelectTab(4);
            label1.Text = "Công nợ nhà cung cấp";
        }

        private void nvHangHoa_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            var form = new frmThemDanhSachHangHoa();
            form.ShowDialog();
        }

        private void nvKhachHangDanhMuc_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            var form = new frmUpdateKH();
            form.ShowDialog();
        }

        private void nvNhaCungCapDanhMuc_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            var form = new frmUpdateNCC();
            form.ShowDialog();
        }

        private void btnDongTonKho_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmNhapSoDuDauKy_Load(object sender, EventArgs e)
        {
            var query = from n in db.HangHoas select n.Tenhang;
            RepositoryItemLookUpEdit riLookUp = new RepositoryItemLookUpEdit();
            riLookUp.DataSource = query.ToList();



            lueKhoTonKho.Properties.DataSource = db.Khoes.ToList();
            lueKhoTonKho.Properties.DisplayMember = "TenKho";
            lueKhoTonKho.Properties.ValueMember = "Id_Kho";



        }

        private void LoaddataKhachHang()
        {
            lueTimKHheoKhuVuc.Properties.DataSource = db.KhuVucs.ToList();

            lueTimKHheoKhuVuc.Properties.DisplayMember = "TenKhuVuc";
            lueTimKHheoKhuVuc.Properties.ValueMember = "Id_KhuVuc";

            var query = (from n in db.KhachHangs from m in db.KhuVucs
                         where n.Id_KhuVuc == m.Id_KhuVuc
                         select new { n.TenKhachHang, n.Id_KH, m.TenKhuVuc }).ToList();
            gricDanhSachKhCongNo.DataSource = query;
        }



        private void btnThemMoiTonKho_Click(object sender, EventArgs e)
        {
            var nh = from o in db.NhapHangTamThois select o;
            foreach (var item in nh)
            {
                db.NhapHangTamThois.Remove(item);
            }
            db.SaveChanges();
            LoadDSHangHoaKhoNhap();
        }

        private void btnLuuKhCongNo_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Dữ liệu đã được lưu.");
        }

        private void simpleButton18_Click(object sender, EventArgs e)
        {
            int ma = int.Parse(lueTimKHheoKhuVuc.EditValue.ToString());
            gricDanhSachKhCongNo.DataSource = null;
            var query = (from n in db.KhachHangs
                         from m in db.KhuVucs
                         where n.Id_KhuVuc == m.Id_KhuVuc && m.Id_KhuVuc == ma
                         select new { n.TenKhachHang, n.Id_KH, m.TenKhuVuc }).ToList();
            gricDanhSachKhCongNo.DataSource = query;
        }

        private void btnDongKHSoDuDauKi_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void LoadDanhSachNCCDauKi()
        {
            lueTimKhuVucTheoNCC.Properties.DataSource = db.KhuVucs.ToList();

            lueTimKhuVucTheoNCC.Properties.DisplayMember = "TenKhuVuc";
            lueTimKhuVucTheoNCC.Properties.ValueMember = "Id_KhuVuc";
            var query = (from n in db.NhaCungCaps
                         from m in db.KhuVucs
                         where n.id_KV == m.Id_KhuVuc
                         select new {m.TenKhuVuc, n.id_NCC, n.TenNCC }).ToList();
            grcDanhSachNhaCungCapDauKi.DataSource = query;
        }

        private void btnLuuDanhSachNCCDauKi_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Dữ liệu đã được lưu.");
        }

        private void btnXemNCCTheoKHuVuc_Click(object sender, EventArgs e)
        {
            grcDanhSachNhaCungCapDauKi.DataSource = null;
            int ma = int.Parse(lueTimKhuVucTheoNCC.EditValue.ToString());
            var query = (from n in db.NhaCungCaps
                         from m in db.KhuVucs
                         where n.id_KV == m.Id_KhuVuc && m.Id_KhuVuc == ma
                         select new { m.TenKhuVuc, n.id_NCC, n.TenNCC }).ToList();
            grcDanhSachNhaCungCapDauKi.DataSource = query;
        }


        private void simpleButton25_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSuaHHTheoDauKi_Click(object sender, EventArgs e)
        {
            tabNhapSoDuDauKi.SelectTab(0);
            label1.Text = "Nhập kho đầu kì";
        }

        private void btnXoaHangHoaTheoCt_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.Cancel)
            {
                string mactnh = gricDanhSachHangHoaTheoDauKi.GetRowCellValue(gricDanhSachHangHoaTheoDauKi.FocusedRowHandle, "id_ChungTuNhap").ToString();
                ChiTietNhapHang ctnh = db.ChiTietNhapHangs.FirstOrDefault(n => n.id_ChungTuNhap == mactnh);
                db.ChiTietNhapHangs.Remove(ctnh);
                db.SaveChanges();
                MessageBox.Show("Đã xóa", "Thông báo");
                LoadDShangHoa(); 
            }
        }

        private void simpleButton17_Click(object sender, EventArgs e)
        {
            grcDanhSachTheoHangHoa.DataSource = null;
            var query = (from n in db.NhapHangs
                         from m in db.ChiTietNhapHangs
                         from p in db.DonViTinhs
                         from q in db.Khoes
                         from t in db.HangHoas
                         where n.id_ChungTuNhap == m.id_ChungTuNhap && m.id_DonVi == p.Id_DV && q.Id_Kho == n.id_Kho && m.id_HangHoa == t.id_hanghoa

                         select new
                         {
                             n.id_ChungTuNhap,
                             n.NgayNhap,
                             n.SoTien,
                             n.ThanhToan,
                             m.SoLuong,
                             m.DonGia,
                             m.id_HangHoa,
                             p.TenDonVi,
                             q.TenKho,
                             t.Tenhang
                         }).ToList();
            grcDanhSachTheoHangHoa.DataSource = query;
        }

        private void btnXuatDSHangHoa_Click(object sender, EventArgs e)
        {
            saveFileDialog1.InitialDirectory = "C:";
            saveFileDialog1.Title = "Save as Excle File";
            saveFileDialog1.FileName = "";
            saveFileDialog1.Filter = "Excle File(2003)|*.xls|Excle File(2007)|*.xlsx";
            if (saveFileDialog1.ShowDialog() != DialogResult.Cancel)
            {
                gricDanhSachHangHoaTheoDauKi.ExportToXls(saveFileDialog1.FileName);
            }
        }

        private void btnTaoMoiHHdauKi_Click(object sender, EventArgs e)
        {
            tabNhapSoDuDauKi.SelectTab(0);
            label1.Text = "Nhập kho đầu kì";
        }

        private void btnXuatHHDauKi_Click(object sender, EventArgs e)
        {
            saveFileDialog1.InitialDirectory = "C:";
            saveFileDialog1.Title = "Save as Excle File";
            saveFileDialog1.FileName = "";
            saveFileDialog1.Filter = "Excle File(2003)|*.xls|Excle File(2007)|*.xlsx";
            if (saveFileDialog1.ShowDialog() != DialogResult.Cancel)
            {
                gricHHTheoKi.ExportToXls(saveFileDialog1.FileName);
            }
            
        }

        private void btnTaoMoiTheoKi_Click(object sender, EventArgs e)
        {
            tabNhapSoDuDauKi.SelectTab(0);
            label1.Text = "Nhập kho đầu kì";
        }

        private void btnSuaHHTheoKi_Click(object sender, EventArgs e)
        {
            tabNhapSoDuDauKi.SelectTab(0);
            label1.Text = "Nhập kho đầu kì";
        }

        private void btnXoaHHTheoKi_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.Cancel)
            {
                string mactnh = gricHHTheoKi.GetRowCellValue(gricHHTheoKi.FocusedRowHandle, "id_ChungTuNhap").ToString();
                ChiTietNhapHang ctnh = db.ChiTietNhapHangs.FirstOrDefault(n => n.id_ChungTuNhap == mactnh);
                db.ChiTietNhapHangs.Remove(ctnh);
                db.SaveChanges();
                MessageBox.Show("Đã xóa", "Thông báo");
                LoadDSHHtheoDauKi();
            }
        }

        private void btnTimKiemTheoKi_Click(object sender, EventArgs e)
        {
            grcHHTheoKi.DataSource = null;
            var query = (from n in db.NhapHangs
                         from m in db.ChiTietNhapHangs                    
                         from q in db.Khoes
                         from t in db.HangHoas
                         where n.id_ChungTuNhap == m.id_ChungTuNhap && q.Id_Kho == n.id_Kho && m.id_HangHoa == t.id_hanghoa
                      
                         select new
                         {
                             n.id_ChungTuNhap,
                             n.NgayNhap,
                             n.ThanhToan,
                             m.id_HangHoa,
                             q.TenKho
                         }).ToList();
            grcHHTheoKi.DataSource = query;
        }

        private void btnThemMoiHangHoa_Click(object sender, EventArgs e)
        {
            var frm = new frmNhapHangTamThoi();
            frm.Show();
        }

        private void btnnapLai_Click(object sender, EventArgs e)
        {
            LoadDSHangHoaKhoNhap();
        }

        private void btnLuuTonKho_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Chưa xử lý lưu.");
        }
    }
}
