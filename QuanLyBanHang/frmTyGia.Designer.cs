﻿namespace QuanLyBanHang
{
    partial class frmTyGia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gcTiGia = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid_TyGia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenTyGia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTyGiaQuyDoi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrangThai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tyGiaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnThemTiGia = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapNhatTiGia = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoaTiGia = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gcTiGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tyGiaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // gcTiGia
            // 
            this.gcTiGia.Location = new System.Drawing.Point(1, 34);
            this.gcTiGia.MainView = this.gridView1;
            this.gcTiGia.Name = "gcTiGia";
            this.gcTiGia.Size = new System.Drawing.Size(740, 377);
            this.gcTiGia.TabIndex = 0;
            this.gcTiGia.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid_TyGia,
            this.colTenTyGia,
            this.colTyGiaQuyDoi,
            this.colTrangThai});
            this.gridView1.GridControl = this.gcTiGia;
            this.gridView1.Name = "gridView1";
            // 
            // colid_TyGia
            // 
            this.colid_TyGia.Caption = "Mã tỉ giá";
            this.colid_TyGia.FieldName = "id_TyGia";
            this.colid_TyGia.Name = "colid_TyGia";
            this.colid_TyGia.Visible = true;
            this.colid_TyGia.VisibleIndex = 0;
            // 
            // colTenTyGia
            // 
            this.colTenTyGia.Caption = "Tên tỉ  giá";
            this.colTenTyGia.FieldName = "TenTyGia";
            this.colTenTyGia.Name = "colTenTyGia";
            this.colTenTyGia.Visible = true;
            this.colTenTyGia.VisibleIndex = 1;
            // 
            // colTyGiaQuyDoi
            // 
            this.colTyGiaQuyDoi.Caption = "Giá trị quy đổi";
            this.colTyGiaQuyDoi.FieldName = "TyGiaQuyDoi";
            this.colTyGiaQuyDoi.Name = "colTyGiaQuyDoi";
            this.colTyGiaQuyDoi.Visible = true;
            this.colTyGiaQuyDoi.VisibleIndex = 2;
            // 
            // colTrangThai
            // 
            this.colTrangThai.Caption = "Trạng thái";
            this.colTrangThai.FieldName = "TrangThai";
            this.colTrangThai.Name = "colTrangThai";
            this.colTrangThai.Visible = true;
            this.colTrangThai.VisibleIndex = 3;
            // 
            // btnThemTiGia
            // 
            this.btnThemTiGia.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btnThemTiGia.ImageUri.Uri = "Add;Size16x16;Colored";
            this.btnThemTiGia.Location = new System.Drawing.Point(10, 3);
            this.btnThemTiGia.Name = "btnThemTiGia";
            this.btnThemTiGia.Size = new System.Drawing.Size(69, 26);
            this.btnThemTiGia.TabIndex = 1;
            this.btnThemTiGia.Text = "Thêm";
            this.btnThemTiGia.Click += new System.EventHandler(this.btnThemTiGia_Click);
            // 
            // btnCapNhatTiGia
            // 
            this.btnCapNhatTiGia.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btnCapNhatTiGia.ImageUri.Uri = "Edit;Size16x16;Colored";
            this.btnCapNhatTiGia.Location = new System.Drawing.Point(85, 2);
            this.btnCapNhatTiGia.Name = "btnCapNhatTiGia";
            this.btnCapNhatTiGia.Size = new System.Drawing.Size(81, 26);
            this.btnCapNhatTiGia.TabIndex = 1;
            this.btnCapNhatTiGia.Text = "Cập nhật";
            this.btnCapNhatTiGia.Click += new System.EventHandler(this.btnCapNhatTiGia_Click);
            // 
            // btnXoaTiGia
            // 
            this.btnXoaTiGia.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btnXoaTiGia.ImageUri.Uri = "Cancel;Size16x16;Colored";
            this.btnXoaTiGia.Location = new System.Drawing.Point(172, 2);
            this.btnXoaTiGia.Name = "btnXoaTiGia";
            this.btnXoaTiGia.Size = new System.Drawing.Size(81, 26);
            this.btnXoaTiGia.TabIndex = 1;
            this.btnXoaTiGia.Text = "Xóa";
            this.btnXoaTiGia.Click += new System.EventHandler(this.btnXoaTiGia_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(615, 5);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(108, 23);
            this.simpleButton1.TabIndex = 2;
            this.simpleButton1.Text = "simpleButton1";
            // 
            // frmTyGia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(743, 410);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.btnXoaTiGia);
            this.Controls.Add(this.btnCapNhatTiGia);
            this.Controls.Add(this.btnThemTiGia);
            this.Controls.Add(this.gcTiGia);
            this.Name = "frmTyGia";
            this.Text = "Tiền tệ";
            this.Load += new System.EventHandler(this.frmTyGia_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.gcTiGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tyGiaBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcTiGia;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource tyGiaBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colid_TyGia;
        private DevExpress.XtraGrid.Columns.GridColumn colTenTyGia;
        private DevExpress.XtraGrid.Columns.GridColumn colTyGiaQuyDoi;
        private DevExpress.XtraGrid.Columns.GridColumn colTrangThai;
        private DevExpress.XtraEditors.SimpleButton btnThemTiGia;
        private DevExpress.XtraEditors.SimpleButton btnCapNhatTiGia;
        private DevExpress.XtraEditors.SimpleButton btnXoaTiGia;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}