﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmSuaNhomHang : Form
    {
        DACK_ware5Entities1 db = new DACK_ware5Entities1();
        NhomHang nh = new NhomHang();

        public frmSuaNhomHang(string id, string ten, string trangthai)
        {
            InitializeComponent();
            LoadTextbox(id, ten, trangthai);
        }

        private void LoadTextbox(string id, string ten, string trangthai)
        {
            txt_MaNhomHang.Text = id;
            txt_TenNhonHang.Text = ten;
            if (trangthai == "True") { cbQuanLy.Checked = true; }
        }

        private void frmSuaNhomHang_Load(object sender, EventArgs e)
        {

        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Luu_Click(object sender, EventArgs e)
        {
            int id = int.Parse(txt_MaNhomHang.Text);
            NhomHang nhomhang = db.NhomHangs.Single(n => n.Id_NhomHang == id);
            nhomhang.TenNhomHang = txt_TenNhonHang.Text;
            if (cbQuanLy.Checked == true)
            {
                nhomhang.TrangThai = true;
            }
            else {
                nhomhang.TrangThai = false;
            }
          
            db.SaveChangesAsync();
            MessageBox.Show("Sửa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.None);
            this.Close();
        }
    }
}
